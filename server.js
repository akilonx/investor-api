﻿require('rootpath')()
const express = require('express')
const app = express()
const cors = require('cors')
const {
  ApolloServer,
  gql,
  AuthenticationError,
} = require('apollo-server-express')
var fs = require('fs')
var https = require('https')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const jwt = require('_helpers/jwt')
const jwt2 = require('jsonwebtoken')
const errorHandler = require('_helpers/error-handler')
const permission = require('./_helpers/permission')

const corsOptions = {
  origin: 'http://localhost:3001',
  credentials: true,
}
const SECRET_KEY =
  'eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTYzODU4MTIxNywiaWF0IjoxNjM4NTgxMjE3fQ.rLbFyBuCcEK9BEAeyzEv46sHukKmsss4uClCKRCYS8A'

const originlist = [
  'http://localhost:3000',
  'http://localhost:3001',
  'http://localhost:3002',
  'http://localhost:3003',
  'http://localhost:3004',
]
app.use(
  cors({
    origin: function (origin, callback) {
      if (origin && originlist.find((a) => a == origin)) {
        return callback(null, true)
      }
      return callback(null, false)
    },
    credentials: true,
  })
)

//app.use(cors(corsOptions))
app.use(cookieParser())

const extractSubDomain = (url) => {
  let hostname

  if (url.indexOf('//') > -1) {
    hostname = url.split('/')[2]
  } else {
    hostname = url.split('/')[0]
  }

  hostname = hostname.split(':')[0]
  hostname = hostname.split('?')[0]

  return hostname.split('.')[0]
}

const context = ({ req }) => {
  const token = req.cookies['jwt'] || ''
  try {
    return ({ id, email } = jwt2.verify(token, SECRET_KEY))
    // return { dbhost: extractSubDomain(req.headers.origin), id, email };
  } catch (e) {
    //return { client: extractSubDomain(req.headers.origin) }
    return { client: 'spainvestor' }
  }
}

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
)
app.use(bodyParser.json())

// use JWT auth to secure the api

const apolloserver = new ApolloServer({
  modules: [
    require('./GraphQL/person'),
    require('./GraphQL/staff'),
    require('./GraphQL/country'),
    require('./GraphQL/order'),
    require('./GraphQL/product'),
    require('./GraphQL/cart'),
    require('./GraphQL/buyer'),
    require('./GraphQL/orderdetail'),
    require('./GraphQL/postcode'),
    require('./GraphQL/config'),
    require('./GraphQL/category'),
    require('./GraphQL/leave'),
    require('./GraphQL/member'),
    require('./GraphQL/officestaff'),
    require('./GraphQL/punchcard'),
    require('./GraphQL/booking'),
    require('./GraphQL/stock'),
    require('./GraphQL/healthreport'),
    require('./GraphQL/payroll'),
    require('./GraphQL/transaction'),
    require('./GraphQL/branch'),
  ],
  context,
  cors: false,
})

apolloserver.applyMiddleware({
  app,
  cors: false,
})
app.use(
  express.urlencoded({
    extended: true,
  })
)

app.use(jwt())

app.use('/media', express.static('../uploads'))

// api routes
app.use('/users', require('./users/users.controller'))

// global error handler
// global error handler
app.use(errorHandler)

// start server
/*
var credentials = {
    key: fs.readFileSync('../../certs/frapp.key'),
    cert: fs.readFileSync('../../certs/frapp_cloud.crt'),
    ca: fs.readFileSync('../../certs/frapp_cloud.ca-bundle')

};

var httpsServer = https.createServer(credentials, app);


const port = process.env.NODE_ENV === 'production' ? 80 : 4000;
const server = httpsServer.listen(port, function () {
    console.log('Server listening on port ' + port);
}); */

const port = process.env.NODE_ENV === 'production' ? 80 : 3020
const server = app.listen(port, function () {
  console.log('Server listening on port ' + port)
})
