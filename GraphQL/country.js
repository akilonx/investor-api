const { gql } = require('apollo-server-express')
const db = require('../database')

const typeDefs = gql`
  type Country {
    id: ID!
    CountryCode: String
    CountryName: String
  }

  extend type Query {
    countries: [Country]
  }
`

const resolvers = {
  Query: {
    countries: () =>
      db.country.findAll({
        order: [
          ['Ordering', 'DESC'],
          ['CountryName', 'ASC']
        ]
      })
  }
}

module.exports = {
  typeDefs,
  resolvers
}
