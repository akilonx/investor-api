const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Cart {
    id: ID
    OrderID: ID
    UserID: ID
    ShipperID: ID
    ProductID: ID
    ProductNo: String
    OrderDate: Date
    RequiredDate: Date
    Freight: Float
    SalesTax: Float
    TransactStatus: String
    InvoiceAmount: Float
    PaymentDate: Date
    Qty: Int
    ProductName: String
    UnitPrice: Float
    ProductImage: String
    PriceID: ID
    ProductPrices: String
    ProductDesc: String
    Category: ID
    SalesPerson: String
    SalesClient: String
  }

  type RemoveCart {
    id: ID
    PriceID: ID
  }

  extend type Query {
    carts(UserID: ID): [Cart]
  }

  extend type Mutation {
    insertcart(
      UserID: ID
      ProductID: ID
      Qty: Int
      PriceID: ID
      SalesPerson: String
    ): Cart
    updatecart(UserID: ID, id: ID, ProductID: ID, Qty: Int, PriceID: ID): Cart
    removecart(id: ID, PriceID: ID): Int
  }
`

const resolvers = {
  Query: {
    /* carts: async (root, {}, { username, role, client, sessionid, iat }) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return []

      const query = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.ProductDesc,
          b.UnitPrice,
          (select FileName from ${client}.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage,
          c.ProductPrices
        FROM ${client}.ao_cartlist a
        LEFT JOIN
          ${client}.ao_productlist b ON a.ProductID = b.id
        LEFT JOIN
          (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", SingleItem, "|", Uom  order by UnitPrice asc) as ProductPrices from ${client}.ao_pricelist group by ProductID) c ON a.ProductID = c.ProductID
        WHERE a.UserID=? ORDER by b.Ordering asc
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    }, */
    carts: async (
      root,
      { UserID },
      { username, role, client, sessionid, iat }
    ) => {
      return db.sequelize.query(
        `
        SELECT 
          a.*,
          b.ProductName,
          b.ProductDesc,
          b.Category,
          (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage,
          c.ProductPrices
        FROM spa1.ao_cartlist a
        LEFT JOIN
          spa1.ao_productlist b ON a.ProductID = b.id
        LEFT JOIN
          (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.ProductID = c.ProductID
        WHERE a.UserID=? ORDER by b.Category, b.Ordering asc`,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )
    },
  },
  Mutation: {
    insertcart: async (
      root,
      { UserID, ProductID, Qty, PriceID, SalesPerson },
      { username, role, client, sessionid, iat }
    ) => {
      const price = await db.sequelize.query(
        `SELECT 
          UnitPrice 
        FROM spa1.ao_pricelist
        WHERE id=?
        `,
        {
          replacements: [PriceID],
          type: QueryTypes.SELECT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          spa1.ao_cartlist 
         SET UserID=?, ProductID=?, PriceID=?, UnitPrice=?, InvoiceAmount=?, Qty=?, SalesPerson=?, SalesClient=?, CreatedDate=NOW()`,
        {
          replacements: [
            UserID,
            ProductID,
            PriceID,
            price[0] && price[0].UnitPrice,
            price[0] && price[0].UnitPrice * Qty,
            Qty,
            SalesPerson,
            client,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.ProductDesc,
          b.Category,
          (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage,
          c.ProductPrices
        FROM spa1.ao_cartlist a
        LEFT JOIN
          spa1.ao_productlist b ON a.ProductID = b.id
        LEFT JOIN
          (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.ProductID = c.ProductID
        WHERE a.id=? ORDER by b.Ordering asc
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removecart: async (
      parent,
      { id, PriceID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `DELETE FROM
          spa1.ao_cartlist 
        WHERE id=?`,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return 1
    },
    updatecart: async (
      root,
      { UserID, id, ProductID, PriceID, Qty },
      { username, role, client, sessionid, iat }
    ) => {
      const price = await db.sequelize.query(
        `SELECT 
          UnitPrice 
        FROM spa1.ao_pricelist
        WHERE id=?
        `,
        {
          replacements: [PriceID],
          type: QueryTypes.SELECT,
        }
      )

      /* const product = await db.sequelize.query(
        `SELECT 
          Category 
        FROM spa1.ao_productlist
        WHERE id=?
        `,
        {
          replacements: [ProductID],
          type: QueryTypes.SELECT,
        }
      ) */

      const update = await db.sequelize.query(
        `UPDATE
          spa1.ao_cartlist 
        SET Qty=?, UnitPrice=?, InvoiceAmount=?, ProductID=?, PriceID=? WHERE id=?`,
        {
          replacements: [
            Qty,
            price[0] && price[0].UnitPrice,
            price[0] && price[0].UnitPrice * Qty,
            ProductID,
            PriceID,
            id,
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.ProductDesc,
          b.Category,
          (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage,
          c.ProductPrices
        FROM spa1.ao_cartlist a
        LEFT JOIN
          spa1.ao_productlist b ON a.ProductID = b.id
        LEFT JOIN
         (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.ProductID = c.ProductID
        WHERE a.id=? ORDER by b.Ordering asc
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
