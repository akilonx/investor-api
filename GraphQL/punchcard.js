const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')
const crypto = require('crypto')

//id, UserID, intime, outtime, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, Username, PunchType, PunchDate, Token

const typeDefs = gql`
  type Punchcard {
    id: ID
    UserID: String
    Intime: String
    Outtime: String
    CreatedBy: String
    CreatedOn: Date
    UpdatedBy: String
    UpdatedOn: Date
    Username: String
    PunchType: String
    PunchDate: Date
    Token: String
    FirstName: String
    LastName: String
  }

  type PunchCardQR {
    token: String
  }

  extend type Query {
    punchcard: Punchcard
    punchcardhistory: [Punchcard]
  }

  extend type Mutation {
    punchin(Token: String, PunchType: String): Int
    updatepunchcard(id: ID, Intime: String, Outtime: String): Punchcard
    insertpunchcard(id: ID, Intime: String, Outtime: String): Punchcard
    removepunchcard(id: ID): Int
  }
`

const resolvers = {
  Query: {
    punchcard: async (root, {}, { username, role, client, sessionid, iat }) => {
      const check = await db.sequelize.query(
        `SELECT 
        *
        FROM ${client}.ao_puchcarddetail 
        WHERE Token is not null AND Username is null
        ORDER BY id DESC
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      if (!check[0]) {
        const token = crypto.randomBytes(64).toString('hex')

        const insert = await db.sequelize.query(
          `INSERT INTO ${client}.ao_puchcarddetail 
          SET
            Token=?
          `,
          {
            replacements: [token],
            type: QueryTypes.INSERT,
          }
        )

        const query = await db.sequelize.query(
          `SELECT 
          *
          FROM ${client}.ao_puchcarddetail 
          WHERE Token is not null AND Username is null
          ORDER BY id DESC
          `,
          {
            replacements: [],
            type: QueryTypes.SELECT,
          }
        )

        return query[0]
      } else {
        return check[0]
      }
    },
    punchcardhistory: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
        a.*,
        c.FirstName,
        c.LastName
        FROM ${client}.ao_puchcarddetail a
        LEFT JOIN ${client}.ao_stafflist b ON b.StaffID = a.Username
        LEFT JOIN ${client}.ao_staffdetail c ON c.UserID = b.id
        WHERE a.Username is not null
        ORDER BY a.id DESC
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },

  Mutation: {
    punchin: async (
      root,
      { Token, PunchType },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      const check = await db.sequelize.query(
        `SELECT 
        *
        FROM ${client}.ao_puchcarddetail 
        WHERE Token=? AND Username is not null
        `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (check[0]) return 0

      const query = await db.sequelize.query(
        `UPDATE 
          ${client}.ao_puchcarddetail 
        SET
          PunchType=?, Username=?, PunchDate=NOW() 
        WHERE Token=? `,
        {
          replacements: [PunchType, username, Token],
          type: QueryTypes.UPDATE,
        }
      )

      return id
    },

    removepunchcard: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          ${client}.ao_puchcarddetail 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },

    insertpuchcard: async (
      root,
      { id, Intime, Outtime },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_puchcarddetail 
        SET 
           UserID=?, Intime=?, Outtime=?, CreatedBy=?, CreatedOn=NOW() , UpdatedOn=NOW()`,
        {
          replacements: [user[0].id, Intime, Outtime],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * 
        FROM ${client}.ao_puchcarddetail
        WHERE id=? LIMIT 1
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatepunchcard: async (
      root,
      { id, Intime, Outtime },
      { username, role, client, sessionid, iat }
    ) => {
      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_puchcarddetail
        SET 
          Intime=?, Outtime=?,
          UpdatedOn=NOW()
         WHERE id=? `,
        {
          replacements: [Intime, Outtime, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * 
        FROM ${client}.ao_puchcarddetail
        WHERE id=? LIMIT 1
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
