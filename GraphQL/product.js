const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const slugify = (string) => {
  const a =
    'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
  const b =
    'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}

const typeDefs = gql`
  type Product {
    id: ID
    ProductID: String
    DepartmentID: String
    Category: String
    CategoryName: String
    IDSKU: String
    ProductName: String
    Quantity: Int
    UnitPrice: Float
    Ranking: Int
    ProductDesc: String
    UnitsInStock: Int
    UnitsInOrder: Int
    ProductImage: String
    ProductPrices: String
    PriceID: ID
    Ordering: Int
    PrettyUrl: String
    Youtube: String
  }

  type TotalProducts {
    total: Int
  }

  type PriceList {
    id: ID
    ProductID: ID
    Unit: Int
    UnitPrice: Float
    Uom: String
  }

  type ProductImages {
    id: ID
    Module: String
    ModuleID: ID
    FileName: String
    FileType: String
    FileSize: Int
    Ordering: Int
  }

  extend type Query {
    pricelist(ProductID: ID): [PriceList]
    productimages(id: ID!): [ProductImages]
    products(offset: Int, limit: Int, Category: ID): [Product]
    product(id: ID): Product
    numberOfProducts: TotalProducts
  }

  extend type Mutation {
    makeprimaryimage(id: ID, ModuleID: ID): Int
    insertproduct(
      Category: String
      IDSKU: String
      ProductName: String
      Quantity: String
      Ordering: String
      UnitPrice: Float
      ProductDesc: String
      UnitsInStock: Int
    ): Product
    updateproduct(
      id: ID
      Category: String
      IDSKU: String
      ProductName: String
      Quantity: String
      Ordering: String
      UnitPrice: Float
      ProductDesc: String
      UnitsInStock: Int
    ): Product
    removeproduct(id: ID!): Int
    insertprice(
      ProductID: ID
      Unit: Int
      UnitPrice: Float
      Uom: String
    ): PriceList
    updateprice(id: ID, Unit: Int, UnitPrice: Float, Uom: String): PriceList
    removeprice(id: ID!): Int
  }
`

const resolvers = {
  Query: {
    pricelist: async (
      root,
      { ProductID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          * 
        FROM ${client}.ao_pricelist 
        WHERE ProductID=?
        `,
        {
          replacements: [ProductID],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    productimages: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `
      SELECT 
        b.*
      FROM ${client}.ao_uploadlist b 
     WHERE b.ModuleID = ? AND b.Module='UPLOAD_PRODUCTIMAGE' AND b.Active=1 ORDER by b.Ordering asc
      `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      ),

    numberOfProducts: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
        id 
      FROM ${client}.ao_stafflist
      WHERE StaffID=?
      `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return false

      const query = await db.sequelize.query(
        `
      SELECT 
        a.* ,
        b.FileName as ProductImage,
        c.ProductPrices,
        (select CategoryName from ${client}.ao_categorylist where id=a.Category) as CategoryName,
        (select id from ${client}.ao_pricelist where ProductID = a.id order by id asc limit 1) AS PriceID
      FROM ${client}.ao_productlist a 
      LEFT JOIN
      ${client}.ao_uploadlist b ON b.ModuleID = a.id
      LEFT JOIN
      (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id  order by UnitPrice asc) as ProductPrices from ${client}.ao_pricelist group by ProductID) c ON a.id = c.ProductID
      WHERE b.Module='UPLOAD_PRODUCTIMAGE' AND b.Active=1 AND b.Ordering = 0 ORDER by a.Ordering asc
      `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      return { total: query.length }
    },
    products: async (
      root,
      { offset, limit, Category },
      { username, role, client, sessionid, iat }
    ) => {
      if (Category) {
        return await db.sequelize.query(
          `
        SELECT 
          a.* ,
          b.FileName as ProductImage,
          c.ProductPrices,
          (select CategoryName from spa1.ao_categorylist where id=a.Category) as CategoryName,
          (select id from spa1.ao_pricelist where ProductID = a.id order by id asc limit 1) AS PriceID,
          (SELECT UnitPrice FROM spa1.ao_pricelist WHERE ProductID=a.id ORDER BY id ASC limit 1) as UnitPrice
        FROM spa1.ao_productlist a 
        LEFT JOIN
        spa1.ao_uploadlist b ON b.ModuleID = a.id
        LEFT JOIN
        (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.id = c.ProductID
        WHERE a.Category = ? AND b.Module='UPLOAD_PRODUCTIMAGE' AND b.Active=1 AND b.Ordering = 0 ORDER by a.Category desc, a.Ordering asc
        `,
          {
            replacements: [Category, offset > 0 ? offset * limit : 0, limit],
            type: QueryTypes.SELECT,
          }
        )
      } else {
        return await db.sequelize.query(
          `
          SELECT 
            a.* ,
            b.FileName as ProductImage,
            c.ProductPrices,
            (select CategoryName from spa1.ao_categorylist where id=a.Category) as CategoryName,
            (select id from spa1.ao_pricelist where ProductID = a.id order by id asc limit 1) AS PriceID,
            (SELECT UnitPrice FROM spa1.ao_pricelist WHERE ProductID=a.id ORDER BY id ASC limit 1) as UnitPrice
          FROM spa1.ao_productlist a 
          LEFT JOIN
          spa1.ao_uploadlist b ON b.ModuleID = a.id
          LEFT JOIN
          (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.id = c.ProductID
          WHERE b.Module='UPLOAD_PRODUCTIMAGE' AND b.Active=1 AND b.Ordering = 0 ORDER by a.Category desc, a.Ordering asc
          `,
          {
            replacements: [],
            type: QueryTypes.SELECT,
          }
        )
      }
    },
    product: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.* ,
          b.FileName as ProductImage,
          c.ProductPrices,
          (select CategoryName from spa1.ao_categorylist where id=a.Category) as CategoryName,
          (select id from spa1.ao_pricelist where ProductID = a.id order by id asc limit 1) AS PriceID,
          (SELECT UnitPrice FROM spa1.ao_pricelist WHERE ProductID=a.id ORDER BY id ASC limit 1) as UnitPrice
        FROM spa1.ao_productlist a 
        LEFT JOIN
          spa1.ao_uploadlist b ON b.ModuleID = a.id
        LEFT JOIN
          (select ProductID, group_concat("RM ", UnitPrice, "|", "RM", UnitPrice, "/", Unit, " ",Uom, "|", id, "|", Uom  order by UnitPrice asc) as ProductPrices from spa1.ao_pricelist group by ProductID) c ON a.id = c.ProductID
        WHERE a.id = ? AND b.Module='UPLOAD_PRODUCTIMAGE' AND b.Active=1 AND b.Ordering = 0 ORDER by a.Ordering asc`,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
  Mutation: {
    makeprimaryimage: async (
      root,
      { id, ModuleID },
      { username, role, client, sessionid, iat }
    ) => {
      const savePrimary = await db.sequelize.query(
        `UPDATE 
        ${client}.ao_uploadlist
        SET Ordering=0
        WHERE id=?`,
        {
          replacements: [id],
          type: QueryTypes.UPDATE,
        }
      )

      const saveCounter = await db.sequelize.query(
        `UPDATE 
        ${client}.ao_uploadlist
          SET Ordering = 1
          WHERE Module='UPLOAD_PRODUCTIMAGE' and ModuleID=? and id!=?
          `,
        {
          replacements: [ModuleID, id],
          type: QueryTypes.UPDATE,
        }
      )

      return 1
    },
    insertprice: async (
      root,
      { ProductID, Unit, UnitPrice, Uom },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      return db.pricelist.create({
        ProductID,
        Unit,
        UnitPrice,
        Uom,
      })
    },
    updateprice: async (
      root,
      { id, Unit, UnitPrice, Uom },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const update = await db.pricelist.update(
        {
          Unit,
          UnitPrice,
          Uom,
        },
        { where: { id } }
      )
      const query = await db.sequelize.query(
        `SELECT 
            * 
          FROM ${client}.ao_pricelist  
          WHERE id=?
          `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removeprice: (root, { id }, { username, role, client, sessionid, iat }) => {
      db.changeDB(client)
      return db.pricelist.destroy({ where: { id } })
    },
    insertproduct: async (
      root,
      {
        Category,
        IDSKU,
        ProductName,
        Quantity,
        Ordering,
        UnitPrice,
        ProductDesc,
        UnitsInStock,
      },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const insert = await db.product.create({
        Category,
        IDSKU,
        ProductName,
        PrettyUrl: slugify(ProductName),
        Quantity,
        Ordering,
        UnitPrice,
        ProductDesc,
        UnitsInStock,
      })

      const query = await db.sequelize.query(
        `SELECT 
        a.* , 
        (select CategoryName from ${client}.ao_categorylist where id=a.Category) as CategoryName,
        '' as ProductImage ,
        '' as ProductPrices,
        '' as PriceID ,
        (SELECT UnitPrice FROM ${client}.ao_pricelist WHERE ProductID=a.id ORDER BY id ASC limit 1) as UnitPrice
      FROM ${client}.ao_productlist a WHERE a.id=?
        `,
        {
          replacements: [insert.id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    updateproduct: async (
      root,
      {
        id,
        Category,
        IDSKU,
        ProductName,
        Quantity,
        Ordering,
        UnitPrice,
        ProductDesc,
        UnitsInStock,
      },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      const update = await db.product.update(
        {
          Category,
          IDSKU,
          ProductName,
          PrettyUrl: slugify(ProductName),
          Quantity,
          Ordering,
          UnitPrice,
          ProductDesc,
          UnitsInStock,
        },
        { where: { id } }
      )
      const query = await db.sequelize.query(
        `SELECT 
        a.* , 
        (select CategoryName from ${client}.ao_categorylist where id=a.Category) as CategoryName,
        (SELECT UnitPrice FROM ${client}.ao_pricelist WHERE ProductID=a.id ORDER BY id ASC limit 1) as UnitPrice
      FROM ${client}.ao_productlist a WHERE a.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    removeproduct: (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      return db.product.destroy({ where: { id } })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
