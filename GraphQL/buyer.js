const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Buyer {
    id: ID
    UserID: ID
    Email: String
    Phone: String
    FirstName: String
    LastName: String
    Address1: String
    Address2: String
    City: String
    Postcode: String
    State: String
    Country: String
    Password: String
    Username: String
    CreatedOn: Date
    LastUpdated: Date
    Error: String
  }

  extend type Query {
    buyers: [Buyer]
    buyer: Buyer
    checkUsername(Username: String): Int
  }

  extend type Mutation {
    insertBuyer(
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Password: String
      Username: String
    ): Buyer
    updateBuyer(
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Password: String
      Username: String
    ): Buyer
  }
`

const resolvers = {
  Query: {
    buyers: async (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
          * 
        FROM spa1.ao_buyerlist
      `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    checkUsername: async (
      root,
      { Username },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [Username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return 1
      else return 0
    },
    buyer: async (root, {}, { username, role, client, sessionid, iat }) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return false

      console.log(user)
      const query = await db.sequelize.query(
        `SELECT 
          * ,
          (SELECT StaffID FROM spa1.ao_stafflist WHERE id = UserID) as Username
          FROM spa1.ao_buyerlist
          WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
  Mutation: {
    insertBuyer: async (
      root,
      {
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        Password,
        Username,
      },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const checkuser = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [Username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', Username, Password)
      if (checkuser[0]) return { Error: 'Username exist' }

      const user = await db.sequelize.query(
        `INSERT INTO 
        spa1.ao_stafflist 
        SET 
          StaffID=?, 
          Password2=?`,
        {
          replacements: [Username, Password],
          type: QueryTypes.INSERT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          spa1.ao_buyerlist 
        SET 
          UserID=?, 
          Email=?,
          Phone=?,
          FirstName=?,
          LastName=?,
          Address1=?,
          Address2=?,
          City=?,
          Postcode=?,
          State=?,
          Country=?,
          CreatedOn=NOW()`,
        {
          replacements: [
            user[0],
            Email,
            Phone,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * ,
        (SELECT StaffID FROM spa1.ao_stafflist WHERE id = UserID) as Username
        FROM spa1.ao_buyerlist
        WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updateBuyer: async (
      root,
      {
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        Password,
      },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      console.log(Password)
      if (Password)
        await db.sequelize.query(
          `UPDATE
          spa1.ao_stafflist 
        SET 
          Password2=?
        WHERE
          StaffID=?`,
          {
            replacements: [Password, username],
            type: QueryTypes.UPDATE,
          }
        )

      const update = await db.sequelize.query(
        `UPDATE
          spa1.ao_buyerlist 
        SET 
          Email=?,
          FirstName=?,
          LastName=?,
          Address1=?,
          Address2=?,
          City=?,
          Postcode=?,
          State=?,
          Country=?,
          LastUpdated=NOW()
         WHERE UserID=? `,
        {
          replacements: [
            Email,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
            user[0].id,
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * ,
        (SELECT StaffID FROM spa1.ao_stafflist WHERE id = UserID) as Username
        FROM spa1.ao_buyerlist
        WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
