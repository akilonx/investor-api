const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Postcode {
    id: ID!
    State: String
    Town: String
    PostCode: String
    Price: Float
  }

  type Town {
    Town: String
    State: String
  }

  extend type Query {
    postcode(postcode: String): Postcode
    postcodeprice: Postcode
    towns: [Town]
    postcodes(town: String): [Postcode]
  }

  extend type Mutation {
    insertpostcode(
      Town: String
      State: String
      PostCode: String
      Price: Float
    ): Postcode
    updatepostcode(
      id: ID!
      Town: String
      State: String
      PostCode: String
      Price: Float
    ): Postcode
    removepostcode(id: ID!): Int
  }
`

const resolvers = {
  Query: {
    postcodeprice: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return false

      const query = await db.sequelize.query(
        `SELECT 
            Postcode
          FROM ${client}.ao_buyerlist
          WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      const postcodes = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_postcode WHERE PostCode = ? `,
        {
          replacements: [query[0].Postcode],
          type: QueryTypes.SELECT,
        }
      )

      return postcodes[0]
    },
    towns: async (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT Town, State FROM ${client}.ao_postcode GROUP BY Town ORDER BY Town ASC `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    postcodes: async (
      root,
      { town },
      { username, role, client, sessionid, iat }
    ) => {
      if (town) {
        return db.sequelize.query(
          `SELECT * FROM ${client}.ao_postcode WHERE Town=? ORDER BY PostCode ASC `,
          {
            replacements: [town],
            type: QueryTypes.SELECT,
          }
        )
      } else {
        return db.sequelize.query(
          `SELECT * FROM ${client}.ao_postcode ORDER BY PostCode ASC `,
          {
            replacements: [],
            type: QueryTypes.SELECT,
          }
        )
      }
    },
    postcode: async (
      root,
      { postcode },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `SELECT * FROM ${client}.ao_postcode WHERE PostCode=? ORDER BY PostCode ASC `,
        {
          replacements: [postcode],
          type: QueryTypes.SELECT,
        }
      ),
  },
  Mutation: {
    insertpostcode: async (
      root,
      { Town, State, PostCode, Price },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      return db.postcode.create({
        Town,
        State,
        PostCode,
        Price,
      })
    },
    updatepostcode: async (
      root,
      { id, Town, State, PostCode, Price },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const update = await db.postcode.update(
        {
          Price,
        },
        { where: { id } }
      )
      const query = await db.sequelize.query(
        `SELECT 
        * 
        FROM ${client}.ao_postcode
        WHERE
         id=?`,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removepostcode: (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      return db.postcode.destroy({ where: { id } })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
