const { gql } = require('apollo-server-express')
const db = require('../database')
const crypto = require('crypto')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Transaction {
    id: ID
    TransactionID: String
    TransactionRef: String
    TransactionDate: String
    TransactionAmount: String
    PaymentTo: String
    Active: String
    RefundOn: Date
    RefundBy: String
    RefundReason: String
    DebtorCode: String
    TransactionType: String
    TransactionDesc: String
    CreatedOn: String
    CreatedBy: String
    PaymentTermID: String
    ServiceTimes: String
    Qty: Int
    UnitPrice: Float
    Client: String
  }

  extend type Query {
    transactionhistory(FromDate: Date, ToDate: Date): [Transaction]
  }

  extend type Mutation {
    inserttransaction(
      UserID: ID
      ProductID: ID
      Qty: Int
      PriceID: ID
      SalesPerson: String
    ): Transaction

    updatetransaction(id: ID, RefundReason: String): Transaction
  }
`

const resolvers = {
  Query: {
    transactionhistory: (
      root,
      { FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      const fromsplit = FromDate.split('/')
      const fr = `${fromsplit[2]}-${fromsplit[0]}-${fromsplit[1]}`

      const tosplit = ToDate.split('/')
      const to = `${tosplit[2]}-${tosplit[0]}-${tosplit[1]}`

      return db.sequelize.query(
        `SELECT 
        *
        FROM spa1.ao_transactionlist 
        WHERE (TransactionDate BETWEEN ? and ?)
        ORDER BY id DESC
        `,
        {
          replacements: [fr, to],
          type: QueryTypes.SELECT,
        }
      )
    },
  },

  Mutation: {
    inserttransaction: async (
      root,
      { UserID, ProductID, Qty, PriceID, SalesPerson },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB('spa1')

      const user = await db.sequelize.query(
        `SELECT 
          a.*,
          b.Branch 
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )

      const price = await db.sequelize.query(
        `SELECT 
            a.UnitPrice, 
            b.Category,
            b.ProductName,
            a.ServiceTimes
          FROM spa1.ao_pricelist a
          LEFT JOIN spa1.ao_productlist b ON a.ProductID=b.id
          WHERE a.id=?
          `,
        {
          replacements: [PriceID],
          type: QueryTypes.SELECT,
        }
      )

      /* const insert = await db.sequelize.query(
          `INSERT INTO 
            spa1.ao_cartlist 
           SET UserID=?, ProductID=?, PriceID=?, UnitPrice=?, InvoiceAmount=?, Qty=?, SalesPerson=?, SalesClient=?, CreatedDate=NOW()`,
          {
            replacements: [
              UserID,
              ProductID,
              PriceID,
              price[0] && price[0].UnitPrice,
              price[0] && price[0].UnitPrice * Qty,
              Qty,
              SalesPerson,
              client,
            ],
            type: QueryTypes.INSERT,
          }
        ) */

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const OrderNo = `${db.pad(orderNo.LastNo, 6)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM spa1.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )
      /* 
        const buyer = await db.sequelize.query(
          `SELECT 
              Postcode
            FROM spa1.ao_buyerlist
            WHERE UserID=? LIMIT 1
          `,
          {
            replacements: [user[0].id],
            type: QueryTypes.SELECT,
          }
        )
  
        const postcodes = await db.sequelize.query(
          `SELECT * FROM spa1.ao_postcode WHERE PostCode = ? `,
          {
            replacements: [buyer[0].Postcode],
            type: QueryTypes.SELECT,
          }
        )
  
         */
      console.log('here', UserID, ProductID, Qty, PriceID, SalesPerson)

      const token = crypto.randomBytes(64).toString('hex')

      return db.order
        .create({
          OrderNo,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          UserID: user[0].id,
          CreatedBy: username,
          /*  DeliveryCharges: postcodes[0].Price, */
          PaymentMethod: 'Cash',
          CreatedOn: fn('NOW'),
          Token: token,
        })
        .then(async (a) => {
          const insertorder = await db.orderdetail.create({
            OrderID: a.id,
            OrderNo,
            UserID: user[0].id,
            ProductID,
            TimeStamp: fn('NOW'),
            InvoiceAmount: price[0].UnitPrice * Qty,
            Qty,
            CreatedDate: fn('NOW'),
            PriceID,
            UnitPrice: price[0].UnitPrice,
            Status: 'Completed',
            Category: price[0].Category,
            SalesPerson,
            SalesClient: client,
          })

          let output = {
            UserID: user[0].id,
            DebitCredit: 'CREDIT',
            TransactionID: a.id,
            TransactionRef: ProductID,
            TransactionDate: fn('NOW'),
            TransactionAmount: price[0].UnitPrice * Qty,
            Active: 1,
            CreatedOn: fn('NOW'),
            CreatedBy: username,
            SalesPersonSingle: SalesPerson,
            SalesPersonSingleClient: client,
            SalesPersonPassive: user[0].SalesPerson,
            SalesPersonPassiveClient: user[0].SalesClient,
            Client: client,
            Qty,
            UnitPrice: price[0].UnitPrice,
            PaymentMode: 'Cash',
            PriceID,
            ProductID,
            OrderNo: OrderNo,
            TransactionLocation: 'WALK_IN',
          }

          if (price[0].Category == 2) {
            output.TransactionType = 'BUY_SERVICE'
            output.TransactionDesc = price[0].ProductName
            output.ServiceTimes = price[0].ServiceTimes
          } else {
            output.TransactionType = 'BUY_PRODUCT'
            output.TransactionDesc = price[0].ProductName
          }
          //transactionSave.push(output)

          const insertTransaction = await db.transactionlist.create(output)

          return db.sequelize.query(
            ` SELECT 
                *
              FROM spa1.ao_transactionlist 
              WHERE id=?
            `,
            {
              replacements: [insertTransaction.id],
              type: QueryTypes.SELECT,
            }
          )
        })
    },

    updatetransaction: async (
      root,
      { id, RefundReason },
      { username, role, client, sessionid, iat }
    ) => {
      const update = await db.sequelize.query(
        `UPDATE
          spa1.ao_transactionlist
        SET 
          RefundReason=?,
          Refund=1,
          RefundBy=?,
          RefundOn=now()
         WHERE id=? `,
        {
          replacements: [RefundReason, username, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        ` SELECT 
            *
          FROM spa1.ao_transactionlist 
          WHERE id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
