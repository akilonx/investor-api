const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Leave {
    id: ID
    UserID: String
    LeaveType: String
    FromDate: Date
    ToDate: Date
    NoOfDays: String
    Approved: String
    LeaveDesc: String
    CreatedBy: String
    CreatedOn: Date
    UpdatedBy: String
    UpdatedOn: Date
  }

  extend type Query {
    leavehistory: [Leave]
  }

  extend type Mutation {
    updateleave(
      id: ID
      LeaveType: String
      FromDate: Date
      ToDate: Date
      NoOfDays: Float
    ): Leave
    insertleave(
      id: ID
      LeaveType: String
      FromDate: Date
      ToDate: Date
      NoOfDays: Float
    ): Leave
    removeleave(id: ID): Int
  }
`

const resolvers = {
  Query: {
    leavehistory: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
        *
        FROM ${client}.ao_leavelist 
        ORDER BY id DESC
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },

  Mutation: {
    removeleave: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          ${client}.ao_leavelist 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },

    insertleave: async (
      root,
      { id, LeaveType, FromDate, ToDate, NoOfDays },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const calcBusinessDays = (dDate1, dDate2) => {
        // input given as Date objects
        let iWeeks,
          iDateDiff,
          iAdjust = 0
        if (dDate2 < dDate1) return -1 // error code if dates transposed
        let iWeekday1 = dDate1.getDay() // day of week
        let iWeekday2 = dDate2.getDay()
        iWeekday1 = iWeekday1 == 0 ? 7 : iWeekday1 // change Sunday from 0 to 7
        iWeekday2 = iWeekday2 == 0 ? 7 : iWeekday2
        if (iWeekday1 > 5 && iWeekday2 > 5) iAdjust = 1 // adjustment if both days on weekend
        iWeekday1 = iWeekday1 > 5 ? 5 : iWeekday1 // only count weekdays
        iWeekday2 = iWeekday2 > 5 ? 5 : iWeekday2

        // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
        iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

        if (iWeekday1 < iWeekday2) {
          //Equal to makes it reduce 5 days
          iDateDiff = iWeeks * 5 + (iWeekday2 - iWeekday1)
        } else {
          iDateDiff = (iWeeks + 1) * 5 - (iWeekday1 - iWeekday2)
        }

        iDateDiff -= iAdjust // take into account both days on weekend

        return iDateDiff + 1 // add 1 because dates are inclusive
      }

      // To calculate the time difference of two dates
      const Difference_In_Days = calcBusinessDays(
        new Date(FromDate),
        new Date(ToDate)
      )

      // To
      const fromSplit = `${FromDate.split('/')[2]}-${FromDate.split('/')[0]}-${
        FromDate.split('/')[1]
      }`
      const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[0]}-${
        ToDate.split('/')[1]
      }`

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_leavelist 
        SET 
           UserID=?, LeaveType=?, FromDate=?, ToDate=?, NoOfDays=?, CreatedBy=?, CreatedOn=NOW() , UpdatedOn=NOW()`,
        {
          replacements: [
            user[0].id,
            LeaveType,
            fromSplit,
            toSplit,
            Difference_In_Days,
            username,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * 
        FROM ${client}.ao_leavelist
        WHERE id=? LIMIT 1
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updateleave: async (
      root,
      { id, LeaveType, FromDate, ToDate, NoOfDays },
      { username, role, client, sessionid, iat }
    ) => {
      const fromSplit = `${FromDate.split('/')[2]}-${FromDate.split('/')[1]}-${
        FromDate.split('/')[0]
      }`
      const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[1]}-${
        ToDate.split('/')[0]
      }`

      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_leavelist
        SET 
          LeaveType=?, FromDate=?, ToDate=?, NoOfDays=?,
          UpdatedOn=NOW()
         WHERE id=? `,
        {
          replacements: [LeaveType, fromSplit, toSplit, NoOfDays, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        * 
        FROM ${client}.ao_leavelist
        WHERE id=? LIMIT 1
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
