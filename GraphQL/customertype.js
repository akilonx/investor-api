const { gql } = require('apollo-server-express')
const db = require('../database')
const { Op } = require('sequelize')

const typeDefs = gql`
  type CustomerType {
    id: ID!
    CompanyTypeCode: String
    CompanyTypeName: String
  }

  extend type Query {
    customertypes: [CustomerType]
  }
`

const resolvers = {
  Query: {
    customertypes: () =>
      db.customertype.findAll({
        where: { Active: 'A' },
        order: [
          ['Ordering', 'DESC'],
          ['id', 'DESC']
        ]
      })
  }
}

module.exports = {
  typeDefs,
  resolvers
}
