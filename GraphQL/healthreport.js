const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')
const moment = require('moment-timezone')

const typeDefs = gql`
  type Healthreport {
    id: ID!
    BuyerID: ID
    Filename: String
    Title: String
    Gender: String
    Name: String
    Age: String
    Height: String
    Weight: String
    TestingTime: Date
  }

  type HealthreportDetail {
    id: ID!
    ReportID: ID
    TestingItem: String
    NormalRange: String
    ActualMeasurement: String
    TestingResult: String
  }

  type HealthreportHeader {
    id: ID!
    BuyerID: ID
    Filename: String
    Title: String
    Gender: String
    Name: String
    Age: String
    Height: String
    Weight: String
    TestingTime: Date
    HealthReportDetails: [HealthreportDetail]
  }

  extend type Query {
    healthreports(UserID: ID): [Healthreport]
    healthreportlatest: [Healthreport]
    healthreportdetails(id: ID, UserID: ID): [HealthreportHeader]
    healthreportdetail(id: ID!): [HealthreportDetail]
  }
`

const resolvers = {
  Query: {
    healthreportlatest: async (
      root,
      args,
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const latest = await db.sequelize.query(
        `SELECT 
        *
        FROM spa1.ao_reportheader
        WHERE UserID = ?
        GROUP By TestingTime
        ORDER BY id DESC Limit 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        *
        FROM spa1.ao_reportheader
        WHERE TestingTime = ?
        ORDER BY Title ASC
        `,
        {
          replacements: [
            moment(latest[0].TestingTime).tz('Asia/Kuala_Lumpur').format(),
          ],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    healthreports: async (
      root,
      { UserID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
        *
        FROM spa1.ao_reportheader
        WHERE UserID = ?
        GROUP By TestingTime
        ORDER BY id DESC
        `,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    healthreportdetails: async (
      root,
      { id, UserID },
      { username, role, client, sessionid, iat }
    ) => {
      if (id) {
        const report = await db.sequelize.query(
          `SELECT 
          *
          FROM spa1.ao_reportheader
          WHERE id=?
          `,
          {
            replacements: [id],
            type: QueryTypes.SELECT,
          }
        )

        if (!report[0]) return []

        const query = await db.sequelize.query(
          `SELECT 
          *
          FROM spa1.ao_reportheader
          WHERE UserID = ? AND TestingTime=?
          ORDER BY Title ASC
          `,
          {
            replacements: [
              report[0].UserID,
              moment(report[0].TestingTime).tz('Asia/Kuala_Lumpur').format(),
            ],
            type: QueryTypes.SELECT,
          }
        )

        return query

        //Filename, Title, Gender, Name, Age, Height, Weight, TestingTime
      } else {
        const report = await db.sequelize.query(
          `SELECT 
          *
          FROM spa1.ao_reportheader
          WHERE UserID = ?
          ORDER BY TestingTime DESC
          LIMIT 1
          `,
          {
            replacements: [UserID],
            type: QueryTypes.SELECT,
          }
        )

        if (!report[0]) return []

        const query = await db.sequelize.query(
          `SELECT 
          *
          FROM spa1.ao_reportheader
          WHERE UserID = ? AND TestingTime=?
          ORDER BY Title ASC
          `,
          {
            replacements: [
              UserID,
              moment(report[0].TestingTime).tz('Asia/Kuala_Lumpur').format(),
            ],
            type: QueryTypes.SELECT,
          }
        )

        return query
      }
    },
    healthreportdetail: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return []

      const query = await db.sequelize.query(
        `SELECT 
        b.*
        FROM spa1.ao_reportheader a
        LEFT JOIN spa1.ao_reportdetail b ON b.ReportID = a.id
        WHERE a.UserID = ? AND a.id=?
        ORDER BY b.id DESC
        `,
        {
          replacements: [user[0].id, id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
  },
  HealthreportHeader: {
    HealthReportDetails: async (
      { id },
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
        b.*
        FROM spa1.ao_reportheader a
        LEFT JOIN spa1.ao_reportdetail b ON b.ReportID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query && query[0].id ? query : []
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
