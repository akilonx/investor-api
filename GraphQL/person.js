const { gql } = require('apollo-server-express')
const db = require('../database')

const typeDefs = gql`
  scalar Date
  type Person {
    id: ID!
    ClientID: ID!
    FirstName: String
    LastName: String
    Email: String
    Mobile: String
    Phone: String
    PreAlert: String
    Active: String
    Username: String
    Password: String
    PersonInCharge: String
  }

  extend type Query {
    persons(ClientID: ID!): [Person]
  }

  extend type Mutation {
    createperson(
      ClientID: ID!
      FirstName: String
      LastName: String
      Email: String
      Mobile: String
      Phone: String
      PreAlert: String
      PersonInCharge: String
    ): Person!
    updateperson(
      id: ID!
      FirstName: String
      LastName: String
      Email: String
      Mobile: String
      Phone: String
      PreAlert: String
      PersonInCharge: String
    ): Person!
    deleteperson(id: ID!): Int!
  }
`

const resolvers = {
  Query: {
    persons: (root, { ClientID }) => db.person.findAll({ where: { ClientID } }),
  },
  Mutation: {
    createperson: (
      root,
      {
        ClientID,
        FirstName,
        LastName,
        Email,
        Mobile,
        Phone,
        PreAlert,
        PersonInCharge,
      }
    ) =>
      db.person.create({
        ClientID,
        FirstName,
        LastName,
        Email,
        Mobile,
        Phone,
        PreAlert,
        PersonInCharge,
        Active: 'A',
      }),
    updateperson: (
      root,
      {
        id,
        FirstName,
        LastName,
        Email,
        Mobile,
        Phone,
        PreAlert,
        PersonInCharge,
      }
    ) => {
      return db.person
        .update(
          {
            FirstName,
            LastName,
            Email,
            Mobile,
            Phone,
            PreAlert,
            PersonInCharge,
            Active: 'A',
          },
          {
            where: {
              id,
            },
          }
        )
        .then((a) => {
          return db.person.findByPk(id)
        })
    },
    deleteperson: (root, { id }) =>
      db.person.destroy({
        where: {
          id,
        },
      }),
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
