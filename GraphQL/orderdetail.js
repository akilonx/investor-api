const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type OrderDetail {
    id: ID
    OrderID: ID
    OrderNo: String
    UserID: ID
    ProductID: ID
    InvoiceAmount: Float
    Qty: Int
    CreatedDate: Date
    PriceID: ID
    UnitPrice: Float
    ProductName: String
    Category: String
    ProductImage: String
    Status: String
    SalesPerson: String
    SalesClient: String
    Error: String
  }

  type MyOrder {
    id: ID
    OrderNo: String
    Remarks: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    LastModified: Date
    StatusCode: String
    TotalItem: Int
    TotalAmount: Float
    PaymentMethod: String
    DeliveryCharges: Float
    OrderDetails: [OrderDetail]
  }
  type MyPass {
    ServiceTimes: Int
  }

  extend type Query {
    myorders: [MyOrder]
    myorder(OrderNo: String!): MyOrder
    mypass: MyPass
  }

  extend type Mutation {
    cartorder(PaymentMethod: String): Int
  }
`

const resolvers = {
  Query: {
    mypass: async (root, args, { username, role, client, sessionid, iat }) => {
      const user = await db.sequelize.query(
        `SELECT * from ${client}.ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM ${client}.ao_transactionlist where TransactionType="BUY_SERVICE" and CreatedBy=? and Active=1;`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    myorders: async (
      root,
      args,
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from ${client}.ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
            a.*, 
            (select count(*) from ${client}.ao_orderdetail where OrderID = a.id) as TotalItem, 
            (select sum(InvoiceAmount) from ${client}.ao_orderdetail where OrderID = a.id) as TotalAmount 
        from ${client}.ao_orderheader a 
        WHERE a.UserID=? 
        ORDER BY a.id Desc`,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    myorder: async (
      root,
      { OrderNo },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from ${client}.ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
            a.*, 
            (select count(*) from ${client}.ao_orderdetail where OrderID = a.id) as TotalItem, 
            (select sum(InvoiceAmount) from ${client}.ao_orderdetail where OrderID = a.id) as TotalAmount 
        from ${client}.ao_orderheader a 
        WHERE a.UserID=? AND a.OrderNo=? 
        ORDER BY a.id Desc`,
        {
          replacements: [user[0].id, OrderNo],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
  MyOrder: {
    OrderDetails: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.Category,
          (select FileName from ${client}.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
        FROM ${client}.ao_orderdetail a 
        LEFT JOIN ${client}.ao_productlist b ON a.ProductID = b.id
        WHERE a.OrderID=? 
        ORDER BY b.Ordering asc`,
        {
          replacements: [root.id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
  },
  Mutation: {
    cartorder: async (
      root,
      { PaymentMethod },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const OrderNo = `${db.pad(orderNo.LastNo, 6)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      const buyer = await db.sequelize.query(
        `SELECT 
            Postcode
          FROM ${client}.ao_buyerlist
          WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      const postcodes = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_postcode WHERE PostCode = ? `,
        {
          replacements: [buyer[0].Postcode],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', postcodes[0].Price)

      return db.order
        .create({
          OrderNo,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          UserID: user[0].id,
          CreatedBy: username,
          /*  DeliveryCharges: postcodes[0].Price, */
          PaymentMethod,
          CreatedOn: fn('NOW'),
        })
        .then(async (a) => {
          const cartSave = await db.sequelize.query(
            `SELECT 
              ? as OrderID, ? as OrderNo, UserID, ShipperID, ProductID, ProductNo, OrderDate, RequiredDate, Freight, SalesTax, TimeStamp, TransactStatus, InvoiceAmount, PaymentDate, Qty, CreatedDate, PriceID, UnitPrice,
              (select b.Category from ao_productlist b where b.id=a.ProductID) as Category
            FROM ${client}.ao_cartlist a WHERE OrderID is null AND UserID = ?
              `,
            {
              replacements: [a.id, OrderNo, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const carts = await db.sequelize.query(
            `SELECT 
              ? as OrderID, ? as OrderNo, a.UserID, a.ShipperID, a.ProductID, a.ProductNo, a.OrderDate, a.RequiredDate, a.Freight, a.SalesTax, a.TimeStamp, a.TransactStatus, a.InvoiceAmount, a.PaymentDate, a.Qty, a.CreatedDate, a.PriceID, a.UnitPrice, b.Category as PriceCategory, b.Uom as PriceUom, b.ServiceTimes as PriceServiceTimes,
              (SELECT ProductName from ${client}.ao_productlist where id=a.ProductID ) as ProductName
            FROM ${client}.ao_cartlist a LEFT JOIN ${client}.ao_pricelist b ON b.id = a.PriceID WHERE a.OrderID is null AND a.UserID = ?
              `,
            {
              replacements: [a.id, OrderNo, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const totalInvoice = cartSave.reduce((a, b) => b.InvoiceAmount + a, 0)

          console.log('cartSave', cartSave)
          cartSave.map((cart) => {
            if (cart.Category == 1) {
              cart.Status = 'Not Collected'
            } else {
              cart.Status = 'Completed'
            }
          })

          const transactionSave = []

          carts.map((cart) => {
            let output = {
              TransactionID: a.id,
              TransactionRef: cart.ProductID,
              TransactionDate: fn('NOW'),
              TransactionAmount: cart.InvoiceAmount,
              Active: 1,
              CreatedOn: fn('NOW'),
              CreatedBy: username,
            }

            if (cart.PriceCategory == 'Service') {
              output.TransactionType = 'BUY_SERVICE'
              output.TransactionDesc = cart.PriceUom
              output.ServiceTimes = cart.PriceServiceTimes
            } else {
              output.TransactionType = 'Purchase Product'
              output.TransactionDesc = cart.ProductName
            }
            transactionSave.push(output)
          })

          const insert = await db.orderdetail.bulkCreate(cartSave)

          console.log('TRANSACTION', transactionSave)

          const insertTransaction = await db.transactionlist.bulkCreate(
            transactionSave
          )

          const remove = await db.sequelize.query(
            `DELETE FROM
              ${client}.ao_cartlist 
            WHERE UserID=? AND OrderID is null`,
            {
              replacements: [user[0].id],
              type: QueryTypes.DELETE,
            }
          )

          console.log('transactionamount', totalInvoice)
          //TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType, TransactionDesc, CreatedOn, CreatedBy, PaymentTermID
          /*  const inserttransaction = await db.sequelize.query(
            `INSERT
            ${client}.ao_transactionlist
            SET
            TransactionID = ?, TransactionRef = '', TransactionDate  = NOW(), TransactionAmount = ?, Active = 1, TransactionType = 'Order - Product', TransactionDesc = 'Purchased Product Online', CreatedBy = ?,  CreatedOn = NOW()`,
            {
              replacements: [OrderNo, totalInvoice, username],
              type: QueryTypes.INSERT,
            }
          ) */

          return OrderNo
        })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
