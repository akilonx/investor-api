const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Officestaff {
    id: ID
    UserID: String
    Email: String
    Phone: String
    FirstName: String
    LastName: String
    Address1: String
    Address2: String
    City: String
    Postcode: String
    State: String
    Country: String
    CreatedOn: Date
    LastUpdated: Date
    Username: String
    Error: String
  }

  extend type Query {
    officestaffs: [Officestaff]
  }

  extend type Mutation {
    updateofficestaff(
      id: ID
      UserID: String
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Password: String
      CreatedOn: Date
      LastUpdated: Date
    ): Officestaff
    insertofficestaff(
      id: ID
      UserID: String
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Username: String
      Password: String
      CreatedOn: Date
      LastUpdated: Date
      Department: String
    ): Officestaff
    removeofficestaff(id: ID): Int
  }
`

const resolvers = {
  Query: {
    officestaffs: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
          a.*,
          (SELECT StaffID FROM ${client}.ao_stafflist WHERE id = a.UserID) as Username
        FROM ${client}.ao_staffdetail a
        ORDER BY a.id DESC
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },

  Mutation: {
    removeofficestaff: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          ${client}.ao_staffdetail 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },

    insertofficestaff: async (
      root,
      {
        id,
        UserID,
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        Username,
        Password,
        CreatedOn,
        LastUpdated,
        Department,
      },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [Username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', checkuser[0], Password)
      if (checkuser[0]) return { Error: 'Username exist' }

      const staff = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_stafflist 
        SET StaffID=?, Password2=?, Department=?, CreateBy=?, CreateOn=NOW()`,
        {
          replacements: [Username, Password, Department, username],
          type: QueryTypes.INSERT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_staffdetail 
        SET UserID= ?, Email=?, Phone=?, FirstName=?, LastName=?, Address1=?, Address2=?, City=?, Postcode=?, State=?, Country=?, CreatedOn=now(), LastUpdated=now()`,
        {
          replacements: [
            staff[0],
            Email,
            Phone,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        a.*,
        (SELECT StaffID FROM ${client}.ao_stafflist WHERE id = a.UserID) as Username
      FROM ${client}.ao_staffdetail a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updateofficestaff: async (
      root,
      {
        id,
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        Password,
      },
      { username, role, client, sessionid, iat }
    ) => {
      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_staffdetail
        SET 
        Email=? ,
        Phone=? ,
        FirstName=? ,
        LastName=? ,
        Address1=? ,
        Address2=? ,
        City=? ,
        Postcode=? , 
        State=? ,
        Country=? ,
        CreatedOn=now() ,
        LastUpdated=now()
         WHERE id=? `,
        {
          replacements: [
            Email,
            Phone,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
            id,
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        a.*,
        (SELECT StaffID FROM ${client}.ao_stafflist WHERE id = a.UserID) as Username
      FROM ${client}.ao_staffdetail a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
