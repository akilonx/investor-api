const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

//id, ProductID, OrderID, Title, Movement, Qty, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn

const typeDefs = gql`
  type Stock {
    id: ID
    ProductID: ID
    ProductName: String
    OrderID: ID
    Title: String
    Movement: String
    Qty: Int
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    ModifiedOn: Date
  }

  type RemainingStock {
    ProductName: String
    Title: String
    TotalIn: Int
    TotalOut: Int
    TotalReserve: Int
    BranchCode: String
  }

  extend type Query {
    stocks(Movement: String): [Stock]
    remainingstocks: [RemainingStock]
  }

  extend type Mutation {
    createstock(ProductID: ID, OrderID: ID, Title: String, Qty: Int): Stock
    updatestock(
      id: ID
      ProductID: ID
      OrderID: ID
      Title: String
      Qty: Int
    ): Stock
  }
`

const resolvers = {
  Query: {
    remainingstocks: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const output = []

      const webpara = await db.sequelize.query(
        `SELECT 
          DSN 
        FROM systemcontrol.webpara
        WHERE type='spa'
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      console.log(webpara)
      let stockssql = ``
      webpara.map(async (para, i) => {
        stockssql += `SELECT 
              a.*,
              (SELECT 
                      b.ProductName
                  FROM
                      spa1.ao_productlist b
                  WHERE
                      b.id = a.ProductID) AS ProductName,
              b.TotalOut,
              c.TotalIn,
              d.TotalReserve,
              '${para.DSN}' as BranchCode
          FROM
              ${para.DSN}.ao_stocklist a
                  LEFT JOIN
              (SELECT 
                  ProductID, SUM(qty) AS TotalOut
              FROM
                  ${para.DSN}.ao_stocklist
              WHERE
                  Movement = 'Out'
              GROUP BY productid) b ON b.ProductID = a.ProductID
                  LEFT JOIN
              (SELECT 
                  ProductID, SUM(qty) AS TotalIn
              FROM
                  ${para.DSN}.ao_stocklist
              WHERE
                  Movement = 'In'
              GROUP BY productid) c ON c.ProductID = a.ProductID
              LEFT JOIN
              (SELECT ProductID, OrderID, sum(qty) AS TotalReserve FROM spa1.ao_orderdetail where Status = 'Not Collected' group by ProductID) d ON d.ProductID = a.ProductID
          GROUP BY BranchCode,a.productid
        `
        if (i < webpara.length - 1) stockssql += ` UNION `
        console.log('output', output, i, webpara.length)
      })

      const stocks = await db.sequelize.query(stockssql, {
        replacements: [],
        type: QueryTypes.SELECT,
      })
      //console.log('output', output)
      return stocks
    },
    stocks: async (
      root,
      { Movement },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `SELECT 
          b.* , 
          (SELECT a.ProductName FROM spa1.ao_productlist a WHERE a.id = b.ProductID) as ProductName
        FROM ${client}.ao_stocklist b
        WHERE b.Movement = ?
      `,
        {
          replacements: [Movement],
          type: QueryTypes.SELECT,
        }
      ),
  },
  Mutation: {
    createstock: async (
      root,
      { ProductID, OrderID, Title, Movement, Qty },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_stocklist
        SET 
          ProductID=?,
          Title='Stock In',
          Movement='In',
          Qty=?,
          CreatedOn=NOW(),
          CreatedBy=?`,
        {
          replacements: [ProductID, Qty, username],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* , 
          (SELECT a.ProductName FROM spa1.ao_productlist a WHERE a.id = b.ProductID) as ProductName
        FROM ${client}.ao_stocklist b
        WHERE b.id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatestock: async (
      root,
      { id, ProductID, OrderID, Title, Movement, Qty },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_stocklist
        SET 
          ProductID=?,
          Qty=?,
          ModifiedOn=NOW(),
          ModifiedBy=?
          WHERE id=?
          `,
        {
          replacements: [ProductID, Qty, username, id],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* , 
          (SELECT a.ProductName FROM spa1.ao_productlist a WHERE a.id = b.ProductID) as ProductName
        FROM ${client}.ao_stocklist b
        WHERE b.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
