const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Config {
    COMNAM: String
    COMADD: String
    COMTEL: String
    COMFAX: String
    COMEML: String
    COMURL: String
    COMDIR: String
    COMREG: String
    INVPFX: String
    ODRPFX: String
    GSTID: String
    COMLOG: String
    COMSHO: String
    COMADD1: String
    COMADD2: String
    COMADD3: String
    WHATSAPP: String
    BTBANKNAME: String
    BTACCNO: String
    BTACCNAME: String
    COLORPRIMARY: String
    COLORSECONDARY: String
    COMLOGO: String
    FAQ: String
    TNC: String
    TOUCHNGO: String
  }

  extend type Query {
    shopconfig: Config
  }

  extend type Mutation {
    updateconfig(formKey: String, formValue: String): Int
  }
`

const resolvers = {
  Query: {
    shopconfig: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const config = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_freightconfig`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      const logo = await db.sequelize.query(
        `select FileName FROM ${client}.ao_uploadlist WHERE Module='UPLOAD_COMPANYLOGO' ORDER by ID desc limit 1`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      const tng = await db.sequelize.query(
        `select FileName FROM ${client}.ao_uploadlist WHERE Module='UPLOAD_TOUCHNGO' ORDER by ID desc limit 1`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      let obj = {}
      config.map((a) => {
        obj[a.ParmKey] = a.ParamValue
      })

      if (logo[0]) obj.COMLOGO = logo[0].FileName

      if (tng[0]) obj.TOUCHNGO = tng[0].FileName

      return obj
    },
  },
  Mutation: {
    updateconfig: async (
      root,
      { formKey, formValue },
      { username, role, client, sessionid, iat }
    ) => {
      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_freightconfig
        SET
          ParamValue=? 
          WHERE ParmKey=?`,
        {
          replacements: [formValue, formKey],
          type: QueryTypes.UPDATE,
        }
      )

      return 1
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
