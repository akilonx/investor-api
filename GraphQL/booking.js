const { gql } = require('apollo-server-express')
const db = require('../database')

const crypto = require('crypto')
const moment = require('moment-timezone')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Booking {
    id: String
    BranchID: ID
    BranchName: String
    BranchCode: String
    BookFrom: Date
    BookTo: Date
    TotalHours: Float
    Status: String
    CreatedBy: String
    CreatedAt: Date
    UpdatedBy: String
    UpdatedAt: Date
    Token: String
    ReceivedBy: String
    ReceivedOn: Date
    Client: String
    FirstName: String
    LastName: String
    Error: String
    ServiceTimes: Int
    Rating: Int
    RatingOn: Date
  }

  extend type Query {
    bookings: [Booking]
    booking(id: ID): Booking
    bookingdetails(Token: ID): Booking
  }

  extend type Mutation {
    receivedbooking(Token: ID, SalesPerson: String): Booking
    insertbooking(
      CustomerID: String
      BookDate: Date
      BookTime: String
      TotalHours: Float
    ): Int
    cancelbooking(id: ID): Int
  }
`

const resolvers = {
  Query: {
    bookings: async (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
        distinct
          a.*, 
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
       WHERE a.BranchCode = ?
          GROUP BY a.id
        ORDER BY a.BookFrom DESC
      `,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      ),

    booking: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*, 
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM ${client}.ao_bookinglist a
        WHERE
          a.id = ? AND a.CreatedBy = ?
      `,
        {
          replacements: [id, username],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    bookingdetails: async (
      root,
      { Token },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*, 
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!query[0]) return { Error: 'Not Found.' }

      const usedservicetimes = await db.sequelize.query(
        `SELECT count(*) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and CreatedBy=? and Active=1;`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      if (!transaction[0]) return { Error: 'No Service Times' }

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      query[0].ServiceTimes = remainingTimes

      return query[0]
    },
  },
  Mutation: {
    cancelbooking: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          spa1.ao_bookinglist 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },
    insertbooking: async (
      root,
      { CustomerID, BookDate, BookTime, TotalHours },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          a.*,
          b.Branch 
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [CustomerID],
          type: QueryTypes.SELECT,
        }
      )

      const splitDateFrom = new Date(BookDate.split('T')[0] + ' ' + BookTime)
      const BookFrom = moment(splitDateFrom)
        .tz('Asia/Kuala_Lumpur')
        .format()
        .toString()
        .split('T')[0]
      /* const fromSplit = `${getDate.split('/')[2]}-${getDate.split('/')[0]}-${getDate.split('/')[1]}` */

      const splitDateTo = splitDateFrom.getHours() + TotalHours
      const BookTo = moment(splitDateTo)
        .tz('Asia/Kuala_Lumpur')
        .format()
        .toString()
        .split('T')[0]

      console.log(
        BookDate,
        BookDate.split('T')[0] + ' ' + BookTime,
        splitDateFrom,
        moment(BookDate).tz('Asia/Kuala_Lumpur').format().split('T')[0],
        client,
        BookTime,
        TotalHours
      )

      const getDate = moment(BookDate)
        .tz('Asia/Kuala_Lumpur')
        .format()
        .split('T')[0]

      //id, BranchID, BranchCode, BookFrom, BookTo, TotalHours, Status, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt

      const token = crypto.randomBytes(64).toString('hex')

      const insert = await db.sequelize.query(
        `INSERT
          spa1.ao_bookinglist
        SET
          BranchCode = ?, 
          BookFrom = ?, 
          TotalHours = ?, 
          Status = 'New', 
          CreatedBy = ?, 
          CreatedAt = NOW(),
          Token=?`,
        {
          replacements: [
            client,
            BookFrom + ' ' + BookTime,
            TotalHours,
            username,
            token,
          ],
          type: QueryTypes.INSERT,
        }
      )

      if (TotalHours == 1) {
        const update = await db.sequelize.query(
          `UPDATE
            spa1.ao_bookinglist
          SET
            BookTo = DATE_ADD(BookFrom, INTERVAL '1' HOUR) 
          WHERE
            id = ?`,
          {
            replacements: [insert[0]],
            type: QueryTypes.UPDATE,
          }
        )
      }

      if (TotalHours == 1.5) {
        const update2 = await db.sequelize.query(
          `UPDATE
            spa1.ao_bookinglist
          SET
            BookTo = DATE_ADD(BookFrom, INTERVAL '1 30' HOUR_MINUTE) 
          WHERE
            id = ?`,
          {
            replacements: [insert[0]],
            type: QueryTypes.UPDATE,
          }
        )
      }

      const booking = await db.sequelize.query(
        `SELECT 
          * 
        FROM spa1.ao_bookinglist
        WHERE id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      console.log(booking)

      const checkbooking = await db.sequelize.query(
        `SELECT *
          FROM spa1.ao_bookinglist
        WHERE
          ((BookTo > ? AND BookTo <= ?) OR BookFrom = ?)
          AND BranchCode=?
          AND id!=?
        `,
        {
          replacements: [
            booking[0].BookFrom.toISOString().slice(0, 19).replace('T', ' '),
            booking[0].BookTo.toISOString().slice(0, 19).replace('T', ' '),
            booking[0].BookFrom.toISOString().slice(0, 19).replace('T', ' '),
            client,
            insert[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      const config = await db.sequelize.query(
        `SELECT 
          * 
        FROM ${client}.ao_freightconfig
        WHERE ParmKey='TOTALROOMS'
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      //update spa1.ao_bookinglist set BookTo = DATE_ADD(BookFrom, INTERVAL '1 30' HOUR_MINUTE)

      if (
        checkbooking &&
        checkbooking.length >= parseInt(config[0].ParamValue)
      ) {
        const remove = await db.sequelize.query(
          `DELETE FROM
            spa1.ao_bookinglist 
          WHERE id=? `,
          {
            replacements: [insert[0]],
            type: QueryTypes.DELETE,
          }
        )

        return 3
      }

      return 1
    },
    receivedbooking: async (
      root,
      { Token, SalesPerson },
      { username, role, client, sessionid, iat }
    ) => {
      //id, TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType, TransactionDesc, CreatedOn, CreatedBy, PaymentTermID, ServiceTimes

      let query = await db.sequelize.query(
        `SELECT 
          a.*, 
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!query[0]) return { Error: 'Not Found.' }

      const usedservicetimes = await db.sequelize.query(
        `SELECT count(*) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and CreatedBy=? and Active=1;`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      if (!transaction[0]) return { Error: 'No Service Times' }

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      query[0].ServiceTimes = remainingTimes

      const save = await db.sequelize.query(
        `UPDATE
            spa1.ao_bookinglist
          SET
            Status='Completed',
            Client=?,
            ReceivedBy=?,
            ReceivedOn=NOW()
          WHERE Token=?`,
        {
          replacements: [client, SalesPerson || username, Token],
          type: QueryTypes.UPDATE,
        }
      )

      query = await db.sequelize.query(
        `SELECT 
          a.*, 
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
