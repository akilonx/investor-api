const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Order {
    id: ID!
    OrderNo: String
    CustomerCode: String
    CustomerName: String
    ZoneID: String
    NoOfCarton: String
    Remarks: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    LastModified: Date
    DateLapse: String
    FirstName: String
    LastName: String
    Phone: String
    Email: String
    Address1: String
    Address2: String
    State: String
    City: String
    PostCode: String
    DeliveryCharges: Float
    TotalAmount: Float
    FinalTotal: Float
    PaymentMethod: String
    Received: String
    ReceivedBy: String
    ReceivedOn: Date
    Client: String
    Error: String
    Token: String
  }

  type OrderStatus {
    id: ID!
    OrderID: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
  }

  type StatusList {
    id: ID
    StatusName: String
    Ordering: Int
    StatusCode: String
  }

  extend type Query {
    orderdetails(OrderID: ID, Token: ID): [OrderDetail]
    statuslist: [StatusList]
    orders: [Order]
    order(Token: ID): Order
    nozoneorders: [Order]
    orderstatus(OrderID: ID!): [OrderStatus]
  }

  extend type Mutation {
    createorder(
      CustomerCode: String
      NoOfCarton: String
      Remarks: String
      User: String
    ): Order
    updateorder(
      id: ID!
      CustomerCode: String
      NoOfCarton: String
      Remarks: String
      User: String
    ): Order

    receivedorder(Token: ID, SalesPerson: String): Order
    removeorder(id: ID!): Int
    updatezone(
      id: ID!
      ZoneID: String
      Status: String
      StatusText: String
      Remarks: String
      CreatedBy: String
    ): Order
  }
`

const resolvers = {
  Query: {
    orderdetails: async (
      root,
      { OrderID, Token },
      { username, role, client, sessionid, iat }
    ) => {
      if (Token) {
        const query = await db.sequelize.query(
          `SELECT 
            a.*,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderheader c 
          LEFT JOIN spa1.ao_orderdetail a ON a.OrderID = c.id
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
          WHERE c.Token=? AND b.Category='1'
          ORDER BY b.Ordering asc`,
          {
            replacements: [Token],
            type: QueryTypes.SELECT,
          }
        )
        return query
      } else {
        const query = await db.sequelize.query(
          `SELECT 
            a.*,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderdetail a 
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
          WHERE a.OrderID=? AND b.Category='1'
          ORDER BY b.Ordering asc`,
          {
            replacements: [OrderID],
            type: QueryTypes.SELECT,
          }
        )

        return query
      }
    },
    order: async (
      root,
      { Token },
      { username, role, client, sessionid, iat }
    ) => {
      if (Token) {
        const query = await db.sequelize.query(
          `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.Token=?`,
          {
            replacements: [Token],
            type: QueryTypes.SELECT,
          }
        )

        if (!query[0]) return {}

        return query[0]
      } else {
        return {}
        /* return db.sequelize.query(
          `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.Status!='Completed' 
          ORDER BY a.id DESC`,
          {
            replacements: [],
            type: QueryTypes.SELECT,
          }
        ) */
      }
    },
    statuslist: (root, args, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT * from ${client}.ao_statuslist WHERE HideSelect is null order by Ordering ASC`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    orders: async (root, args, { username, role, client, sessionid, iat }) => {
      /*  sub: 136,
      username: 'ace',
      role: 'Booking Agent',
      client: 'asiaone_old7',
      sessionid: 'PU3hDduIQVTjlT5tZR_L',
      iat: 1581410557 */

      return db.sequelize.query(
        `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          LEFT JOIN
        (SELECT 
          count(*) as TotalProduct,
                  OrderID
        FROM
          spa1.ao_orderdetail
        WHERE
          Category = 1
        group by orderid) c ON c.OrderID = a.id
          WHERE b.Branch=? and c.TotalProduct>0
          ORDER BY a.ReceivedBy, a.id DESC limit 1000`,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )
    },
    /* db.order.findAll({
        order: [['id', 'DESC']],
        limit: 100
      }) */
    nozoneorders: (root, data, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode WHERE a.ZoneID is null ORDER BY a.id DESC`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    orderstatus: (
      root,
      { OrderID },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `SELECT * FROM ${client}.ao_orderstatus WHERE OrderID=? ORDER BY id ASC`,
        {
          replacements: [OrderID],
          type: QueryTypes.SELECT,
        }
      ),
  },
  Order: {
    Customer: (
      { CustomerCode },
      data,
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      db.customer.findOne({
        where: {
          CustomerCode,
        },
      })
    },
  },
  Mutation: {
    updatezone: async (
      root,
      { id, ZoneID, Status, Remarks, CreatedBy, StatusText },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      const StatusCode = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_statuslist WHERE StatusName=?`,
        {
          replacements: [Status],
          type: QueryTypes.SELECT,
        }
      )

      return db.order
        .update(
          {
            ZoneID,
            Status,
            StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
            Remarks,
          },
          {
            where: {
              id,
            },
          }
        )
        .then(async () => {
          //id, OrderID, Status, CreatedBy, CreatedOn

          if (StatusText) Status = `${Status} - ${StatusText}`

          const insert = await db.sequelize.query(
            `INSERT
            ${client}.ao_orderstatus
            SET
            OrderID = ?, Status = ?, CreatedBy = ?, StatusCode = ?, CreatedOn = NOW()`,
            {
              replacements: [
                id,
                Status,
                CreatedBy,
                StatusCode[0] && StatusCode[0].StatusCode,
              ],
              type: QueryTypes.INSERT,
            }
          )

          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
          /* db.order.findOne({
          where: {
            id
          }
        }) */
        })
    },
    createorder: async (
      root,
      { CustomerCode, NoOfCarton, Remarks, User },
      { username, role, client, sessionid, iat }
    ) => {
      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const prefix = await db.freightconfig.findOne({
        where: {
          ParmKey: 'ODRPFX',
        },
      })

      const OrderNo = `${prefix.ParamValue}${db.pad(orderNo.LastNo, 5)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      return db.order
        .create({
          OrderNo,
          CustomerCode,
          NoOfCarton,
          Remarks,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          CreatedBy: User,
          CreatedOn: fn('NOW'),
        })
        .then(async (a) => {
          const insert = await db.sequelize.query(
            `INSERT
            ${client}.ao_orderstatus
            SET
            OrderID = ?, Status = ?, CreatedBy = ?, StatusCode = ? CreatedOn = NOW()`,
            {
              replacements: [
                a.id,
                StatusCode[0] && StatusCode[0].StatusName,
                User,
                StatusCode[0] && StatusCode[0].StatusCode,
              ],
              type: QueryTypes.INSERT,
            }
          )

          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [a.id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
        })
    },
    updateorder: (
      root,
      { id, CustomerCode, NoOfCarton, Remarks, User },
      { username, role, client, sessionid, iat }
    ) =>
      db.order
        .update(
          {
            CustomerCode,
            NoOfCarton,
            Remarks,
            ModifiedBy: User,
            LastModified: fn('NOW'),
          },
          {
            where: {
              id,
            },
          }
        )
        .then(async (a) => {
          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
        }),
    receivedorder: async (
      root,
      { Token, SalesPerson },
      { username, role, client, sessionid, iat }
    ) => {
      const orderdetails = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.Category,
          (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
        FROM spa1.ao_orderheader c 
        LEFT JOIN spa1.ao_orderdetail a ON a.OrderID = c.id
        LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
        WHERE c.Token=? AND b.Category='1'
        ORDER BY b.Ordering asc`,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      const save = await db.sequelize.query(
        `UPDATE
            spa1.ao_orderheader
          SET
            Status=?,
            StatusCode=?,
            Client=?,
            Received='Received',
            ReceivedBy=?,
            ReceivedOn=NOW()
          WHERE Token=?`,
        {
          replacements: [
            'Completed',
            'COMPLETED',
            client,
            SalesPerson || username,
            Token,
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const savedetail = await db.sequelize.query(
        `UPDATE
            spa1.ao_orderdetail
          SET
            Status='Completed'
          WHERE OrderNo=?`,
        {
          replacements: [orderdetails[0].OrderNo],
          type: QueryTypes.UPDATE,
        }
      )

      orderdetails.map(async (a) => {
        await db.sequelize.query(
          `INSERT INTO 
            ${client}.ao_stocklist
          SET 
            OrderID=?,
            ProductID=?,
            Title='Stock Out',
            Movement='Out',
            Qty=?,
            CreatedOn=NOW(),
            CreatedBy=?`,
          {
            replacements: [a.OrderID, a.ProductID, a.Qty, username],
            type: QueryTypes.INSERT,
          }
        )
      })

      const query = await db.sequelize.query(
        `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            b.Branch,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.Token=?`,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removeorder: (root, { id }, { username, role, client, sessionid, iat }) => {
      db.changeDB(client)
      return db.order.destroy({
        where: {
          id,
        },
      })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
