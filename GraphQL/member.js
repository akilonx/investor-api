const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Member {
    id: ID
    UserID: String
    Email: String
    Phone: String
    FirstName: String
    LastName: String
    Address1: String
    Address2: String
    City: String
    Postcode: String
    State: String
    Country: String
    CreatedOn: Date
    LastUpdated: Date
    Username: String
    Error: String
    ServiceTimes: Int
    Client: String
  }

  extend type Query {
    members(Branch: ID): [Member]
    memberorders(UserID: ID): [OrderDetail]
  }

  extend type Mutation {
    updatemember(
      id: ID
      UserID: String
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Password: String
      CreatedOn: Date
      LastUpdated: Date
    ): Member
    insertmember(
      id: ID
      UserID: String
      Email: String
      Phone: String
      FirstName: String
      LastName: String
      Address1: String
      Address2: String
      City: String
      Postcode: String
      State: String
      Country: String
      Password: String
      Username: String
      CreatedOn: Date
      LastUpdated: Date
    ): Member
    removemember(id: ID): Int
  }
`

const resolvers = {
  Query: {
    members: (root, { Branch }, { username, role, client, sessionid, iat }) => {
      if (Branch) {
        return db.sequelize.query(
          `SELECT 
          a.*,
          c.StaffID as Username,
          b.ServiceTimes,
          (select BranchName as Client from systemcontrol.webpara where BranchCode=a.Branch) as Client
          FROM spa1.ao_buyerlist a
          LEFT JOIN spa1.ao_stafflist c ON c.id=a.UserID
          LEFT JOIN (SELECT (sum(g.ServiceTimes) - h.TotalUsed) as ServiceTimes, g.CreatedBy FROM spa1.ao_transactionlist g 
          LEFT JOIN (SELECT count(*) as TotalUsed, CreatedBy FROM spa1.ao_bookinglist GROUP BY CreatedBy) h ON h.CreatedBy=g.CreatedBy where g.TransactionType="BUY_SERVICE" and g.Active=1 group by g.CreatedBy) b ON b.CreatedBy=c.StaffID
          WHERE a.Branch=? AND c.StaffID!='walkin'
          ORDER BY a.id DESC
          `,
          {
            replacements: [Branch],
            type: QueryTypes.SELECT,
          }
        )
      } else {
        return db.sequelize.query(
          `SELECT 
          a.*,
          c.StaffID as Username,
          b.ServiceTimes,
          (select BranchName as Client from systemcontrol.webpara where BranchCode=a.Branch) as Client
          FROM spa1.ao_buyerlist a
          LEFT JOIN spa1.ao_stafflist c ON c.id=a.UserID
          LEFT JOIN (SELECT (sum(g.ServiceTimes) - h.TotalUsed) as ServiceTimes, g.CreatedBy FROM spa1.ao_transactionlist g 
          LEFT JOIN (SELECT count(*) as TotalUsed, CreatedBy FROM spa1.ao_bookinglist GROUP BY CreatedBy) h ON h.CreatedBy=g.CreatedBy 
          where g.TransactionType="BUY_SERVICE" and g.Active=1 group by g.CreatedBy) b ON b.CreatedBy=c.StaffID
          where c.StaffID!='walkin'
          ORDER BY a.id DESC
          `,
          {
            replacements: [],
            type: QueryTypes.SELECT,
          }
        )
      }
    },
    memberorders: async (
      root,
      { UserID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
            a.*,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderdetail a 
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
          WHERE a.UserID=? 
          ORDER BY a.id desc`,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )
      return query
    },
  },

  Mutation: {
    removemember: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      /* const query = await db.sequelize.query(
        `DELETE FROM
          spa1.ao_buyerlist 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      ) */

      return id
    },

    insertmember: async (
      root,
      {
        id,
        UserID,
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        CreatedOn,
        LastUpdated,
        Username,
        Password,
      },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [Username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', Username, Password)
      if (checkuser[0]) return { Error: 'Username exist' }

      const staff = await db.sequelize.query(
        `INSERT INTO 
          spa1.ao_stafflist 
        SET StaffID=?, Password2=?, SalesPerson=?, SalesClient=?`,
        {
          replacements: [Username, Password, username, client],
          type: QueryTypes.INSERT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          spa1.ao_buyerlist 
        SET UserID= ? , Email=? , Phone=? ,FirstName=? ,LastName=? ,Address1=? ,Address2=? ,City=? ,Postcode=? , State=? ,Country=? ,CreatedOn=now() ,LastUpdated=now()`,
        {
          replacements: [
            staff[0],
            Email,
            Username,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
          ],
          type: QueryTypes.INSERT,
        }
      )
      //id, StaffID, StaffName, Designation, Department, Email, Password, Active, CreateOn, CreateBy, LastModified, ModifiedBy, ContactNo, Role, Password2, AgentCode, Member

      const query = await db.sequelize.query(
        `SELECT 
        a.*,
        c.StaffID as Username,
        b.ServiceTimes
        FROM spa1.ao_buyerlist a
        LEFT JOIN spa1.ao_stafflist c ON c.id=a.UserID
        LEFT JOIN (SELECT (sum(g.ServiceTimes) - h.TotalUsed) as ServiceTimes, g.CreatedBy FROM spa1.ao_transactionlist g 
        LEFT JOIN (SELECT count(*) as TotalUsed, CreatedBy FROM spa1.ao_bookinglist GROUP BY CreatedBy) h ON h.CreatedBy=g.CreatedBy 
        where g.TransactionType="BUY_SERVICE" and g.Active=1 group by g.CreatedBy) b ON b.CreatedBy=c.StaffID
        WHERE a.id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatemember: async (
      root,
      {
        id,
        Email,
        Phone,
        FirstName,
        LastName,
        Address1,
        Address2,
        City,
        Postcode,
        State,
        Country,
        Password,
      },
      { username, role, client, sessionid, iat }
    ) => {
      const member = await db.sequelize.query(
        `SELECT 
        a.*,
        (SELECT StaffID FROM spa1.ao_stafflist WHERE id = a.UserID) as Username
        FROM spa1.ao_buyerlist a
        WHERE a.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      const staff = await db.sequelize.query(
        `UPDATE
          spa1.ao_stafflist 
        SET Password2=?
        WHERE id=?`,
        {
          replacements: [Password, member[0].UserID],
          type: QueryTypes.UPDATE,
        }
      )

      const update = await db.sequelize.query(
        `UPDATE
          spa1.ao_buyerlist
        SET 
        Email=? ,
        FirstName=? ,
        LastName=? ,
        Address1=? ,
        Address2=? ,
        City=? ,
        Postcode=? , 
        State=? ,
        Country=? ,
        CreatedOn=now() ,
        LastUpdated=now()
        WHERE id=? `,
        {
          replacements: [
            Email,
            FirstName,
            LastName,
            Address1,
            Address2,
            City,
            Postcode,
            State,
            Country,
            id,
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
        a.*,
        c.StaffID as Username,
        b.ServiceTimes
        FROM spa1.ao_buyerlist a
        LEFT JOIN spa1.ao_stafflist c ON c.id=a.UserID
        LEFT JOIN (SELECT (sum(g.ServiceTimes) - h.TotalUsed) as ServiceTimes, g.CreatedBy FROM spa1.ao_transactionlist g 
        LEFT JOIN (SELECT count(*) as TotalUsed, CreatedBy FROM spa1.ao_bookinglist GROUP BY CreatedBy) h ON h.CreatedBy=g.CreatedBy 
        where g.TransactionType="BUY_SERVICE" and g.Active=1 group by g.CreatedBy) b ON b.CreatedBy=c.StaffID
        WHERE a.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
