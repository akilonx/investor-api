const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Branch {
    BranchName: ID
    BranchCode: String
  }

  extend type Query {
    branches: [Branch]
  }
`

const resolvers = {
  Query: {
    branches: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
            a.*
          FROM systemcontrol.webpara a 
          WHERE a.Type='spa' AND a.BranchCode is not null
          ORDER BY a.BranchName desc`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
