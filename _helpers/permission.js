const pool = require('../_helpers/database')

const permit = (...allowed) => {
  const isAllowed = role => allowed.indexOf(role) > -1

  // return a middleware
  return async (req, res, next) => {
    //global.dbname = req.user.client;

    try {
      var expiry = await pool.query(
        `SELECT * FROM systemcontrol.payment WHERE NextExpiry > CURDATE() AND DSN = ? AND (SubID='default-1M' OR SubID='trial-14D') ORDER BY id DESC LIMIT 1`,
        [req.user.client]
      )
      //if (!expiry[0]) throw "Expired";

      var subscription = await pool.query(
        'SELECT a.*, b.Type as SingleMultiType FROM systemcontrol.payment a LEFT JOIN systemcontrol.subscriptions b ON b.SubID=a.SubID WHERE a.DSN = ? ORDER BY a.id DESC LIMIT 1',
        [req.user.client]
      )
      //if (!subscription[0]) throw "Invalid subscription, please contact the developer.";

      //if (subscription[0].SingleMultiType == 'su') {
      //id, DSN, User, LoggedDate, iat
      var loggedin = await pool.query(
        `Select * FROM ` +
          req.user.client +
          `.ao_loggedin WHERE DSN=? AND SessionID=? ORDER BY id DESC LIMIT 1`,
        [req.user.client, req.user.sessionid]
      )
      //if (!loggedin[0]) throw "Multiple user login detected.";
      //}

      console.log('here ' + req.user.client + ', ' + req.user.role)
      if (req.user && isAllowed(req.user.role)) next()
      // role is allowed, so continue on the next middleware
      else {
        res.status(403).json({ message: 'Forbidden' }) // user is forbidden
      }
    } catch (err) {
      //throw new Error(err)
      res.status(403).json({ message: err }) // user is forbidden
    }
  }
}

module.exports = permit
