const util = require('util')
const mysql = require('mysql')
const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'systemcontrol',
})

// Ping database to check for common exception errors.
pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has too many connections.')
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused.')
    }
  }

  if (connection) connection.release()

  return
})

pool.changeDB = (dbhost) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      // handle/report error
      return
    }
    conn.changeUser(
      {
        database: dbhost,
      },
      function (err) {
        if (err) {
          // handle/report error
          return
        }
        // Use the updated connection here, eventually
        // release it:
        conn.release()
      }
    )
  })
}

// Promisify for Node.js async/await.
pool.query = util.promisify(pool.query)

pool.dbhost = function () {
  let host = window.location.host
  let protocol = window.location.protocol
  let parts = host.split('.')
  /* let subdomain = "";
// If we get more than 3 parts, then we have a subdomain
// INFO: This could be 4, if you have a co.uk TLD or something like that.
if (parts.length >= 3) {
  subdomain = parts[0];
  // Remove the subdomain from the parts list
  parts.splice(0, 1);
  // Set the location to the new url
  window.location = protocol + "//" + parts.join(".") + "/" + subdomain; */

  return parts[0]
}

pool.pad = function (n, width, z) {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}

pool.activitylog = async (user, data) => {
  //id, LogType, ConsignmentID, MAWBID, HAWBID, User, Action, ActionType, Module, LogDate
  var insert = await pool.query(
    `INSERT INTO ` +
      user.client +
      `.ao_activitylog SET LogType=?, ConsignmentID=?, MAWBID=?, HAWBID=?, User=?, Action=?, ActionType=?, Module=?, LogDate=NOW()`,
    [
      data.LogType,
      data.ConsignmentID,
      data.MAWBID,
      data.HAWBID,
      data.User,
      data.Action,
      data.ActionType,
      data.Module,
    ]
  )
}

module.exports = pool
