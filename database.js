const Sequelize = require('sequelize')

var db = {}

const pad = (n, width, z) => {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}

db.pad = pad

let sequelize = new Sequelize('spa2', 'root', '', {
  host: 'localhost',
  port: '3306',
  dialect: 'mysql',
  define: {
    freezeTableName: true,
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false,
})

let models = [
  require('./models/person.js'),
  require('./models/freightrunno.js'),
  require('./models/freightconfig.js'),
  require('./models/staff.js'),
  require('./models/order.js'),
  require('./models/buyer.js'),
  require('./models/orderdetail.js'),
  require('./models/product.js'),
  require('./models/pricelist.js'),
  require('./models/transaction.js'),
]

// Initialize models
models.forEach((model) => {
  const seqModel = model(sequelize, Sequelize)
  db[seqModel.name] = seqModel
})

// Apply associations
Object.keys(db).forEach((key) => {
  if ('associate' in db[key]) {
    db[key].associate(db)
  }
})

db.changeDB = (dbhost) => {
  sequelize = new Sequelize(dbhost, 'root', 'w8midh.', {
    host: 'localhost',
    port: '3306',
    dialect: 'mysql',
    define: {
      freezeTableName: true,
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    operatorsAliases: false,
  })

  // Initialize models
  models.forEach((model) => {
    const seqModel = model(sequelize, Sequelize)
    db[seqModel.name] = seqModel
  })

  // Apply associations
  Object.keys(db).forEach((key) => {
    if ('associate' in db[key]) {
      db[key].associate(db)
    }
  })
}

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
