module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'buyer',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      UserID: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
      },
      Email: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      Phone: {
        type: DataTypes.STRING(45),
      },
      FirstName: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      LastName: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      Address1: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      Address2: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      City: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      Postcode: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      State: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Country: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      CreatedOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      LastUpdated: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      tableName: 'ao_buyerlist',
      timestamps: false,
    }
  )
}
