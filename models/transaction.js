module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'transactionlist',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      TransactionID: {
        type: DataTypes.INTEGER(8),
        allowNull: true,
      },
      TransactionRef: {
        type: DataTypes.STRING(145),
        allowNull: true,
      },
      TransactionDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      TransactionAmount: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true,
      },
      PaymentTo: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      DebtorCode: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      TransactionType: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      TransactionDesc: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      CreatedOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      CreatedBy: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      PaymentTermID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      ServiceTimes: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      SalesPersonSingle: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonSingleClient: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonPassive: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonPassiveClient: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Client: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Qty: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      UnitPrice: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true,
      },
      PaymentMode: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      PriceID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      Refund: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      RefundReason: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      RefundBy: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      RefundOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      ProductID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      OrderNo: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      TransactionLocation: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
    },
    {
      tableName: 'ao_transactionlist',
      timestamps: false,
    }
  )
}

/*
  `PaymentMode` varchar(145) DEFAULT NULL,
  `PriceID` smallint DEFAULT NULL,
  `Refund` smallint DEFAULT NULL,
  `RefundReason` text,
  `RefundBy` varchar(45) DEFAULT NULL,
  `RefundOn` datetime DEFAULT NULL,
  `ItemID` smallint DEFAULT NULL,
  `OrderNo` varchar(45) DEFAULT NULL,
  `TransactionLocation` varchar(45) DEFAULT NULL,
*/
