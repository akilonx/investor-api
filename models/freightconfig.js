module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'freightconfig',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      ParmKey: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      ParamDesc: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      ParamValue: {
        type: DataTypes.STRING(255),
        allowNull: true
      }
    },
    {
      tableName: 'ao_freightconfig',
      timestamps: false
    }
  )
}
//id, ParmKey, ParamDesc, ParamValue
