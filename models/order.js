module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'order',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      OrderNo: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      CustomerCode: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      ZoneID: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      NoOfCarton: {
        type: DataTypes.STRING(150),
        allowNull: true,
      },
      DeliveryCharges: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      Remarks: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      Status: {
        type: DataTypes.STRING(10),
        allowNull: true,
      },
      PaymentMethod: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      CreatedOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      CreatedBy: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      StatusCode: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      LastModified: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      ModifiedBy: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      Token: {
        type: DataTypes.STRING(145),
        allowNull: true,
      },
    },
    {
      tableName: 'ao_orderheader',
      timestamps: false,
    }
  )
}
//id, OrderNo, CustomerCode, ZoneID, NoOfCarton, Remarks, Status
/* CREATE TABLE `ao_orderheader` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `OrderNo` varchar(255) DEFAULT NULL,
  `CustomerCode` varchar(255) DEFAULT NULL,
  `ZoneID` varchar(255) DEFAULT NULL,
  `NoOfCarton` varchar(255) DEFAULT NULL,
  `Remarks` text,
  `Status` varchar(255) DEFAULT NULL,
  `OrderBy` varchar(255) DEFAULT NULL,
  `OrderPhone` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderNo` (`OrderNo`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `ZoneID` (`ZoneID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
 */
