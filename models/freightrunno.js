module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'freightrunno',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      Prefix: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      LastNo: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      RunDesc: {
        type: DataTypes.STRING(100),
        allowNull: true
      }
    },
    {
      tableName: 'ao_freightrunno',
      timestamps: false
    }
  )
}
//id, Prefix, LastNo, RunDesc
