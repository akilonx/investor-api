module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'orderdetail',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      OrderID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },

      OrderNo: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },

      ShipperID: {
        type: DataTypes.STRING(8),
        allowNull: true,
      },

      ProductID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },

      ProductNo: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },

      OrderDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      RequiredDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      Freight: {
        type: DataTypes.DECIMAL(10, 0),
        allowNull: true,
      },
      SalesTax: {
        type: DataTypes.DECIMAL(10, 0),
        allowNull: true,
      },
      TransactStatus: {
        type: DataTypes.STRING(25),
        allowNull: true,
      },
      InvoiceAmount: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      PaymentDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      Qty: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      CreatedDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      PriceID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      UnitPrice: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: true,
      },
      Status: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Category: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPerson: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesClient: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
    },
    {
      tableName: 'ao_orderdetail',
      timestamps: false,
    }
  )
}
/* CREATE TABLE `ao_orderdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` varchar(8) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `ShipperID` varchar(8) DEFAULT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `ProductNo` varchar(45) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `RequiredDate` date DEFAULT NULL,
  `Freight` decimal(10,0) DEFAULT NULL,
  `SalesTax` decimal(10,0) DEFAULT NULL,
  `TimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TransactStatus` varchar(25) DEFAULT NULL,
  `InvoiceAmount` int(11) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `PriceID` int(11) DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UserID_idx` (`UserID`),
  KEY `ShipperID_idx` (`ShipperID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

   */
