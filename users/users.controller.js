﻿const express = require('express')
const router = express.Router()
const userService = require('./user.service')

// routes
router.post('/authenticate', authenticate)
module.exports = router

function authenticate(req, res, next) {
  userService
    .authenticate(req.body)
    .then((user) => {
      if (user.token) {
        res.cookie('jwt', user.token, {
          httpOnly: true,
          //secure: true, //on HTTPS
          //domain: 'example.com', //set your domain
        })
        return user
          ? res.json(user)
          : res
              .status(400)
              .json({ message: 'Username or password is incorrect' })
      }
      //  (user ? res.json(user) : res.status(400).json({ message: "Username or password is incorrect" })))
    })
    .catch((err) => next(err))
}
