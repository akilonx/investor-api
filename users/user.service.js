﻿const config = require('config.json')
const jwt = require('jsonwebtoken')
const pool = require('../_helpers/database')
const passwordgenerator = require('password-generator')

const util = require('util')
const exec = util.promisify(require('child_process').exec)

module.exports = {
  authenticate,
}

async function authenticate({ username, password, client, clientlogin }) {
  try {
    var dsn = await pool.query(
      'SELECT * FROM systemcontrol.webpara WHERE Orgname = ? ',
      [client]
    )

    if (!dsn[0]) throw 'Invalid login, please try again.'
    if (!dsn[0].DSN) throw 'Invalid login, please try again.'

    var user = await pool.query(
      `SELECT * FROM ` +
        dsn[0].DSN +
        `.ao_stafflist WHERE StaffID = ? AND Password2 = ?`,
      [username, password]
    )

    if (user[0]) {
      const sessionid = passwordgenerator(20, false)
      const token = jwt.sign(
        {
          sub: user[0].id,
          username: user[0].StaffID,
          role: user[0].Department,
          client: dsn[0].DSN,
          sessionid,
        },
        config.secret,
        {
          expiresIn: '365d', // expires in 365 days
        }
      )

      delete user[0].Password2
      delete user[0].Password
      delete user[0].ModifiedBy
      delete user[0].CreateBy
      const { password, ...userWithoutPassword } = user[0]

      return {
        Expired: !expiry[0] ? 'yes' : 'no',
        ...userWithoutPassword,
        token,
      }
    }
  } catch (err) {
    throw new Error(err)
  }
}
