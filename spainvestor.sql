-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: spainvestor
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ao_activitylog`
--

DROP TABLE IF EXISTS `ao_activitylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_activitylog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `LogType` varchar(45) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `MAWBID` int DEFAULT NULL,
  `HAWBID` int DEFAULT NULL,
  `User` varchar(45) DEFAULT NULL,
  `Action` varchar(245) DEFAULT NULL,
  `ActionType` varchar(45) DEFAULT NULL,
  `Module` varchar(45) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_activitylog`
--

LOCK TABLES `ao_activitylog` WRITE;
/*!40000 ALTER TABLE `ao_activitylog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_activitylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_agentlist`
--

DROP TABLE IF EXISTS `ao_agentlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_agentlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AgentCode` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AgentName` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AgentCode` (`AgentCode`),
  KEY `Status` (`Status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_agentlist`
--

LOCK TABLES `ao_agentlist` WRITE;
/*!40000 ALTER TABLE `ao_agentlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_agentlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_airlinemaster`
--

DROP TABLE IF EXISTS `ao_airlinemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_airlinemaster` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AirlineCode` varchar(30) NOT NULL,
  `AirlineID` varchar(30) DEFAULT NULL,
  `AirlineName` varchar(150) DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  `Select` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AirlineCode` (`AirlineCode`),
  KEY `AirlineID` (`AirlineID`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_airlinemaster`
--

LOCK TABLES `ao_airlinemaster` WRITE;
/*!40000 ALTER TABLE `ao_airlinemaster` DISABLE KEYS */;
INSERT INTO `ao_airlinemaster` VALUES (1,'232','MH','Malaysia Airlines',1,NULL),(2,'807','AK','Air Asia',2,NULL),(9,'816','OD','Malindo',3,NULL),(13,'999','ZZZ','Last',4,NULL),(12,'000','First ','AAA',5,NULL),(14,'539','TH','Raya Airways',6,NULL),(15,' 380','3G','ACE',7,NULL),(16,'639','RN','RAYANI AIR',8,NULL),(17,'0','All','General',0,1);
/*!40000 ALTER TABLE `ao_airlinemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_authorisationlist`
--

DROP TABLE IF EXISTS `ao_authorisationlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_authorisationlist` (
  `ProgramID` varchar(20) DEFAULT NULL,
  `StaffID` varchar(20) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(20) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_authorisationlist`
--

LOCK TABLES `ao_authorisationlist` WRITE;
/*!40000 ALTER TABLE `ao_authorisationlist` DISABLE KEYS */;
INSERT INTO `ao_authorisationlist` VALUES ('CSG010              ','CHANDRA             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','CHELSEA             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','CHANDRA             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('INV010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('MNT010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('CSG010              ','CHELSEA             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','DAMIEN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','CHELSEA             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('INV010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('MNT010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('CSG010              ','CLTEH               ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','CLTEH               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','CHELSEA             ','2011-09-22 12:17:20','PEILING             ',NULL,NULL),('INV010              ','CHELSEA             ','2011-09-22 12:17:20','PEILING             ',NULL,NULL),('CUS010              ','DAMIEN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','DRIVER              ','2011-11-09 12:27:30','PEILING             ',NULL,NULL),('CSG010              ','DAMIEN              ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','JASLINE             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','JASLINE             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('INV010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('MNT010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('CSG010              ','JASLINE             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('INQ010              ','IDA                 ','2009-10-19 19:50:20','JAYA                ',NULL,NULL),('CSG010              ','JAYA2               ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','LEE                 ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CSG010              ','LEE                 ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('CSG010              ','MAYYEN              ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','MAYYEN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CST010              ','MAYYEN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','PEILING             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('INV010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('CUS010              ','SESIM               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','TGCHAN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','YAPHS               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CST010              ','PEILING             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','SESIM               ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','TGCHAN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','YAPHS               ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CSG010              ','PEILING             ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','SESIM               ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','SYAFIQ              ','2011-10-07 10:11:33','PEILING             ',NULL,NULL),('CSG010              ','TGCHAN              ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','YAPHS               ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CUS010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('INQ010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('INV010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('CUS010              ','SYAFIQ              ','2011-10-07 10:11:33','PEILING             ',NULL,NULL),('CSG010              ','ZAC                 ','2011-11-01 13:56:23','PEILING             ',NULL,NULL),('CUS010              ','ZAC                 ','2011-11-01 13:56:23','PEILING             ',NULL,NULL),('MNT010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('INQ010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('MNT010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('RPT010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('CSG010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('CUS010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('INQ010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('INQ010              ','LIM                 ','2009-02-10 17:01:35','JAYA                ',NULL,NULL),('INV010              ','LIM                 ','2009-02-10 17:01:35','JAYA                ',NULL,NULL),('INV010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('CSG010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('CUS010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('INQ010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('CSG010              ','ABU                 ','2012-02-21 14:43:24','PEILING             ',NULL,NULL),('CUS010              ','ABU                 ','2012-02-21 14:43:24','PEILING             ',NULL,NULL),('CSG010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('CUS010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('INQ010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('INQ010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('INV010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('MNT010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('RPT010              ','DAMIEN              ','2009-04-02 09:12:07','YAPHS               ',NULL,NULL),('RPT010              ','YAPHS               ','2009-04-02 09:12:07','YAPHS               ',NULL,NULL),('CSG010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('CUS010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('INQ010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('INV010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('CSG010              ','SHAMSUL             ','2012-11-19 16:25:21','PEILING             ',NULL,NULL),('CUS010              ','SHAMSUL             ','2012-11-19 16:25:21','PEILING             ',NULL,NULL),('CSG010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('CUS010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('INQ010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('INV010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('RPT010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('INV010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('INQ010              ','SELIM               ','2009-09-11 18:10:46','PEILING             ',NULL,NULL),('INV010              ','SELIM               ','2009-09-11 18:10:46','PEILING             ',NULL,NULL),('INQ010              ','SESIM               ','2009-09-11 18:11:35','PEILING             ',NULL,NULL),('INV010              ','SESIM               ','2009-09-11 18:11:35','PEILING             ',NULL,NULL),('INQ010              ','CHANDRA             ','2009-09-17 12:21:05','PEILING             ',NULL,NULL),('INV010              ','CHANDRA             ','2009-09-17 12:21:05','PEILING             ',NULL,NULL),('RPT010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('INQ010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('INV010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('MNT010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('INQ010              ','LEE                 ','2011-08-12 11:05:03','CLTEH               ',NULL,NULL),('INV010              ','LEE                 ','2011-08-12 11:05:03','CLTEH               ',NULL,NULL),('INQ010              ','MAYYEN              ','2011-08-12 11:03:22','CLTEH               ',NULL,NULL),('INV010              ','MAYYEN              ','2011-08-12 11:03:22','CLTEH               ',NULL,NULL),('INQ010              ','JASLINE             ','2011-08-12 11:03:53','CLTEH               ',NULL,NULL),('INV010              ','JASLINE             ','2011-08-12 11:03:53','CLTEH               ',NULL,NULL);
/*!40000 ALTER TABLE `ao_authorisationlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_awb`
--

DROP TABLE IF EXISTS `ao_awb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_awb` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AWBNo` varchar(30) NOT NULL,
  `MAWBID` int DEFAULT NULL,
  `MAWBNo` varchar(45) DEFAULT NULL,
  `ETD` datetime DEFAULT NULL,
  `ETA` datetime DEFAULT NULL,
  `POL` varchar(20) DEFAULT NULL,
  `POD` varchar(20) DEFAULT NULL,
  `AirlineID` varchar(20) DEFAULT NULL,
  `FlightNo` varchar(20) DEFAULT NULL,
  `AgentID` varchar(20) DEFAULT NULL,
  `Remark` text,
  `Pieces` int DEFAULT NULL,
  `Weight` decimal(18,2) DEFAULT NULL,
  `Status` datetime DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL,
  `LockDate` datetime DEFAULT NULL,
  `LockBy` varchar(20) DEFAULT NULL,
  `BillComplete` datetime DEFAULT NULL,
  `BillType` varchar(3) DEFAULT NULL,
  `CoLoaderCode` varchar(45) DEFAULT NULL,
  `ShipperCode` varchar(45) DEFAULT NULL,
  `ConsigneeCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AWBNo` (`AWBNo`),
  KEY `LockDate` (`LockDate`,`LockBy`),
  KEY `LockDate_2` (`LockDate`),
  KEY `LockBy` (`LockBy`),
  KEY `BillComplete` (`BillComplete`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_awb`
--

LOCK TABLES `ao_awb` WRITE;
/*!40000 ALTER TABLE `ao_awb` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_awb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_billingdetail`
--

DROP TABLE IF EXISTS `ao_billingdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_billingdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `BillingID` int DEFAULT NULL,
  `AWBNo` varchar(30) DEFAULT NULL,
  `AWBID` int DEFAULT NULL,
  `ConsignmentNo` varchar(30) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `ItemCode` int DEFAULT NULL,
  `ItemType` varchar(50) DEFAULT NULL,
  `Quantity` decimal(18,1) DEFAULT NULL,
  `UnitPrice` decimal(18,2) DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  `lastUpdatedBy` varchar(100) DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `BillingType` varchar(20) DEFAULT NULL,
  `VendorCode` varchar(20) DEFAULT NULL,
  `MAWBNo` varchar(20) DEFAULT NULL,
  `MAWBID` int DEFAULT NULL,
  `Weight` decimal(18,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AWBNo` (`AWBNo`),
  KEY `ConsignmentNo` (`ConsignmentNo`),
  KEY `ConsignmentID` (`ConsignmentID`),
  KEY `BillingID` (`BillingID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_billingdetail`
--

LOCK TABLES `ao_billingdetail` WRITE;
/*!40000 ALTER TABLE `ao_billingdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_billingdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_billingheader`
--

DROP TABLE IF EXISTS `ao_billingheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_billingheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AWBNo` varchar(30) DEFAULT NULL,
  `AWBID` int DEFAULT NULL,
  `ConsignmentNo` varchar(30) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `ShipperCode` varchar(30) DEFAULT NULL,
  `ConsigneeCode` varchar(30) DEFAULT NULL,
  `BillTo` varchar(30) DEFAULT NULL,
  `BillToCode` varchar(30) DEFAULT NULL,
  `InvoiceToCode` varchar(30) DEFAULT NULL,
  `PPorCC` varchar(10) DEFAULT NULL,
  `Parcel` varchar(10) DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  `Weight` decimal(18,2) DEFAULT NULL,
  `BillDate` datetime DEFAULT NULL,
  `BillBy` varchar(20) DEFAULT NULL,
  `InvoiceNo` varchar(30) DEFAULT NULL,
  `InvoiceID` int DEFAULT NULL,
  `InvoiceDate` date DEFAULT NULL,
  `InvoiceDueDate` date DEFAULT NULL,
  `InvoiceBy` varchar(30) DEFAULT NULL,
  `Remark` text,
  `ETD` datetime DEFAULT NULL,
  `ETA` datetime DEFAULT NULL,
  `EmailPOD` varchar(3) DEFAULT NULL,
  `EmailPODBy` varchar(100) DEFAULT NULL,
  `EmailPODOn` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(100) DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `POD` varchar(15) DEFAULT NULL,
  `POL` varchar(15) DEFAULT NULL,
  `ConsignmentDate` datetime DEFAULT NULL,
  `MAWBNo` varchar(45) DEFAULT NULL,
  `MAWBID` int DEFAULT NULL,
  `CoLoaderCode` varchar(45) DEFAULT NULL,
  `AgentID` varchar(45) DEFAULT NULL,
  `BillingType` varchar(10) DEFAULT NULL,
  `VendorCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AWBNo` (`AWBNo`),
  KEY `ConsignmentNo` (`ConsignmentNo`),
  KEY `InvoiceDate` (`InvoiceDate`),
  KEY `InvoiceNo` (`InvoiceNo`),
  KEY `InvoicetocodeInvoiceNo` (`InvoiceToCode`,`InvoiceNo`),
  KEY `etd` (`ETD`),
  KEY `AWBID` (`AWBID`),
  KEY `Invoicetocodeinvoicedate` (`InvoiceToCode`,`InvoiceDate`),
  KEY `forinvoice` (`ETD`,`InvoiceNo`,`BillToCode`,`InvoiceToCode`),
  KEY `forinvoice2` (`ETD`,`InvoiceNo`,`InvoiceToCode`),
  KEY `ConsignmentID` (`ConsignmentID`),
  KEY `forreport` (`ConsignmentID`,`InvoiceToCode`,`InvoiceDate`,`InvoiceNo`),
  KEY `InvoiceNoAWBIDetd` (`AWBID`,`InvoiceNo`,`ETD`,`BillDate`),
  KEY `awbnoMawbid` (`AWBNo`,`MAWBID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_billingheader`
--

LOCK TABLES `ao_billingheader` WRITE;
/*!40000 ALTER TABLE `ao_billingheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_billingheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_bookinglist`
--

DROP TABLE IF EXISTS `ao_bookinglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_bookinglist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `BranchID` int DEFAULT NULL,
  `BranchCode` varchar(45) DEFAULT NULL,
  `BookFrom` datetime DEFAULT NULL,
  `BookTo` datetime DEFAULT NULL,
  `TotalHours` decimal(8,2) DEFAULT NULL,
  `Status` varchar(145) DEFAULT NULL,
  `CreatedBy` varchar(145) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedBy` varchar(145) DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Token` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_bookinglist`
--

LOCK TABLES `ao_bookinglist` WRITE;
/*!40000 ALTER TABLE `ao_bookinglist` DISABLE KEYS */;
INSERT INTO `ao_bookinglist` VALUES (26,NULL,'spa2','2020-09-07 13:00:00','2020-09-07 14:00:00',1.00,'New','01110340242','2020-09-07 07:55:48',NULL,NULL,NULL),(30,NULL,'spa2','2020-09-07 14:00:00','2020-09-07 15:00:00',1.00,'New','01110340242','2020-09-07 07:57:43',NULL,NULL,NULL),(55,NULL,'spa2','2020-09-07 15:00:00','2020-09-07 16:30:00',1.50,'New','01110340242','2020-09-07 08:22:47',NULL,NULL,NULL),(60,NULL,'spa2','2020-09-07 17:00:00','2020-09-07 18:00:00',1.00,'New','01110340242','2020-09-07 08:27:30',NULL,NULL,NULL),(63,NULL,'spa2','2020-09-07 12:00:00','2020-09-07 13:00:00',1.00,'New','01110340242','2020-09-07 08:28:10',NULL,NULL,NULL),(66,NULL,'spa2','2020-09-07 11:00:00','2020-09-07 12:00:00',1.00,'New','01110340242','2020-09-07 08:41:18',NULL,NULL,NULL),(95,NULL,'spa2','2020-09-09 09:00:00','2020-09-09 10:00:00',1.00,'New','01110340242','2020-09-07 13:54:19',NULL,NULL,NULL),(97,NULL,'spa2','2020-09-10 09:00:00','2020-09-10 10:00:00',1.00,'New','01110340242','2020-09-07 13:54:58',NULL,NULL,NULL),(100,NULL,'spa2','2020-09-10 11:00:00','2020-09-10 12:00:00',1.00,'New','01110340242','2020-09-07 13:57:47',NULL,NULL,NULL),(102,NULL,'spa2','2020-09-06 12:00:00','2020-09-06 13:00:00',1.00,'New','01110340242','2020-09-07 13:59:40',NULL,NULL,NULL),(103,NULL,'spa2','2020-09-07 09:00:00','2020-09-07 10:30:00',1.50,'New','01110340242','2020-09-07 18:23:27',NULL,NULL,NULL),(105,NULL,'spa2','2020-09-09 11:00:00','2020-09-09 12:00:00',1.00,'New','01110340242','2020-09-09 10:46:02',NULL,NULL,NULL),(107,NULL,'spa2','2020-11-04 09:00:00','2020-11-04 10:00:00',1.00,'New','admin','2020-11-04 09:29:23',NULL,NULL,'46b60701805cb562192973a2bc213412ff2842f69b0f6918ec7f2cc87f08644cc4ac5111b9bcd55b244e243015fa8cf228fc8d1526a213cf9da0c048eaf0991d'),(108,NULL,'spa2','2020-11-23 11:00:00','2020-11-23 12:00:00',1.00,'New','01110340242','2020-11-23 08:13:41',NULL,NULL,NULL),(110,NULL,'spa2','2020-11-24 11:00:00','2020-11-24 12:00:00',1.00,'New','01110340242','2020-11-23 08:14:05',NULL,NULL,NULL),(112,NULL,'spa2','2020-11-23 11:00:00','2020-11-23 12:00:00',1.00,'New','01110340242','2020-11-23 08:18:08',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ao_bookinglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_buyerlist`
--

DROP TABLE IF EXISTS `ao_buyerlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_buyerlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `FirstName` varchar(245) DEFAULT NULL,
  `LastName` varchar(245) DEFAULT NULL,
  `Address1` varchar(245) DEFAULT NULL,
  `Address2` varchar(245) DEFAULT NULL,
  `City` varchar(245) DEFAULT NULL,
  `Postcode` varchar(20) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_buyerlist`
--

LOCK TABLES `ao_buyerlist` WRITE;
/*!40000 ALTER TABLE `ao_buyerlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_buyerlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_cartlist`
--

DROP TABLE IF EXISTS `ao_cartlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_cartlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` varchar(8) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `ShipperID` varchar(8) DEFAULT NULL,
  `ProductID` int DEFAULT NULL,
  `ProductNo` varchar(45) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `RequiredDate` date DEFAULT NULL,
  `Freight` decimal(10,0) DEFAULT NULL,
  `SalesTax` decimal(10,0) DEFAULT NULL,
  `TimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TransactStatus` varchar(25) DEFAULT NULL,
  `InvoiceAmount` decimal(10,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `Qty` int DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `PriceID` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UserID_idx` (`UserID`),
  KEY `ShipperID_idx` (`ShipperID`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_cartlist`
--

LOCK TABLES `ao_cartlist` WRITE;
/*!40000 ALTER TABLE `ao_cartlist` DISABLE KEYS */;
INSERT INTO `ao_cartlist` VALUES (265,NULL,54,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:20',NULL,1000.00,NULL,1,'2020-09-03 10:14:39',43,1000.00),(306,NULL,54,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:15',NULL,196.00,NULL,2,'2020-09-14 20:24:22',38,98.00),(307,NULL,174,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-09-18 07:17:25',NULL,110.00,NULL,1,'2020-09-18 07:17:25',42,110.00),(308,NULL,54,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:19',NULL,330.00,NULL,3,'2020-11-04 07:49:44',42,110.00);
/*!40000 ALTER TABLE `ao_cartlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_categorylist`
--

DROP TABLE IF EXISTS `ao_categorylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_categorylist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) DEFAULT NULL,
  `ParentID` int DEFAULT NULL,
  `CategoryType` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `Prettier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_categorylist`
--

LOCK TABLES `ao_categorylist` WRITE;
/*!40000 ALTER TABLE `ao_categorylist` DISABLE KEYS */;
INSERT INTO `ao_categorylist` VALUES (1,'Product',NULL,'product',1,'product'),(2,'Service',NULL,'product',2,'service');
/*!40000 ALTER TABLE `ao_categorylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_companytypelist`
--

DROP TABLE IF EXISTS `ao_companytypelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_companytypelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CompanyTypeCode` longtext NOT NULL,
  `CompanyTypeName` longtext,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(45) DEFAULT NULL,
  `UpdateOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_companytypelist`
--

LOCK TABLES `ao_companytypelist` WRITE;
/*!40000 ALTER TABLE `ao_companytypelist` DISABLE KEYS */;
INSERT INTO `ao_companytypelist` VALUES (1,'FGA_ADA','AF Airline Direct Agent',NULL,NULL,NULL,NULL,3,'A'),(2,'FGA_AIR','AF Air Line',NULL,NULL,NULL,NULL,3,'A'),(3,'FGA_ECS','AF Export Consignee',NULL,NULL,NULL,NULL,NULL,NULL),(4,'FGA_ECT','AF Export Customer / Bill To',NULL,NULL,NULL,NULL,NULL,NULL),(5,'FGA_ESA','AF Export Sub Agent',NULL,NULL,NULL,NULL,NULL,NULL),(6,'FGA_ESP','AF Export Shipper',NULL,NULL,NULL,NULL,NULL,NULL),(7,'FGA_ICS','AF Import Consignee',NULL,NULL,NULL,NULL,NULL,NULL),(8,'FGA_ICT','AF Import Customer / Bill To',NULL,NULL,NULL,NULL,NULL,NULL),(9,'FGA_ISP','AF Import Shipper',NULL,NULL,NULL,NULL,NULL,NULL),(10,'FGA_OAG','AF Oversea Agent',NULL,NULL,NULL,NULL,NULL,NULL),(11,'FGA_VDR','AF Vendor',NULL,NULL,NULL,NULL,NULL,NULL),(15,'FGA_AGN','AF Agent',NULL,NULL,NULL,NULL,NULL,'A');
/*!40000 ALTER TABLE `ao_companytypelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_consignmentlog`
--

DROP TABLE IF EXISTS `ao_consignmentlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_consignmentlog` (
  `ConsignmentNo` longtext NOT NULL,
  `CreateOn` datetime NOT NULL,
  `CreateBy` longtext NOT NULL,
  `Message` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_consignmentlog`
--

LOCK TABLES `ao_consignmentlog` WRITE;
/*!40000 ALTER TABLE `ao_consignmentlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_consignmentlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_consignmentnote`
--

DROP TABLE IF EXISTS `ao_consignmentnote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_consignmentnote` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ConsignmentNo` varchar(30) DEFAULT NULL,
  `Pieces` int DEFAULT NULL,
  `Weight` decimal(18,1) DEFAULT NULL,
  `Destination` varchar(30) DEFAULT NULL,
  `AirlineID` int DEFAULT NULL,
  `AWB` varchar(30) DEFAULT NULL,
  `AWBID` int DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(20) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL,
  `PrintDate` datetime DEFAULT NULL,
  `Image` varchar(150) DEFAULT NULL,
  `Thumb` varchar(150) DEFAULT NULL,
  `EmailPOD` varchar(10) DEFAULT NULL,
  `EmailPODBy` varchar(20) DEFAULT NULL,
  `EmailPODOn` datetime DEFAULT NULL,
  `OrderNo` varchar(30) DEFAULT NULL,
  `ShipperCode` varchar(30) DEFAULT NULL,
  `Express` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ConsignmentNo` (`ConsignmentNo`),
  KEY `AWB` (`AWB`),
  KEY `consAwb` (`ConsignmentNo`,`AWB`),
  KEY `AWBID` (`AWBID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_consignmentnote`
--

LOCK TABLES `ao_consignmentnote` WRITE;
/*!40000 ALTER TABLE `ao_consignmentnote` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_consignmentnote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_consignmentstatus`
--

DROP TABLE IF EXISTS `ao_consignmentstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_consignmentstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ConsignmentID` int DEFAULT NULL,
  `Status` varchar(245) DEFAULT NULL,
  `CreatedOn` date DEFAULT NULL,
  `StatusID` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_consignmentstatus`
--

LOCK TABLES `ao_consignmentstatus` WRITE;
/*!40000 ALTER TABLE `ao_consignmentstatus` DISABLE KEYS */;
INSERT INTO `ao_consignmentstatus` VALUES (1,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(2,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(3,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(4,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(5,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(6,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(7,609,'Proof Of Delivery attached.','2020-03-28','STSPOD'),(8,609,'Proof Of Delivery attached.','2020-03-29','STSPOD');
/*!40000 ALTER TABLE `ao_consignmentstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_costingdetail`
--

DROP TABLE IF EXISTS `ao_costingdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_costingdetail` (
  `AWBNo` varchar(30) NOT NULL,
  `CostingCode` varchar(30) NOT NULL,
  `Cost` decimal(18,2) DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL,
  `LastChanged` datetime DEFAULT NULL,
  KEY `AWBNo` (`AWBNo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_costingdetail`
--

LOCK TABLES `ao_costingdetail` WRITE;
/*!40000 ALTER TABLE `ao_costingdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_costingdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_costingheader`
--

DROP TABLE IF EXISTS `ao_costingheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_costingheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CostType` varchar(45) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `AWBID` int DEFAULT NULL,
  `InvoiceNo` varchar(45) DEFAULT NULL,
  `InvoiceDate` date DEFAULT NULL,
  `InvoiceDueDate` date DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  `Title` varchar(145) DEFAULT NULL,
  `VendorCode` varchar(145) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `UploadFile` varchar(245) DEFAULT NULL,
  `AirFreightCharges` decimal(18,2) DEFAULT NULL,
  `FuelCharges` decimal(18,2) DEFAULT NULL,
  `TerminalScreeningFee` decimal(18,2) DEFAULT NULL,
  `EHUHandlingCharges` decimal(18,2) DEFAULT NULL,
  `AWBFee` decimal(18,2) DEFAULT NULL,
  `OtherLocalChargesAmount` decimal(18,2) DEFAULT NULL,
  `OtherLocalChargesTitle` varchar(245) DEFAULT NULL,
  `DeliveryCharges` decimal(18,2) DEFAULT NULL,
  `TerminalCharges` decimal(18,2) DEFAULT NULL,
  `CustomClearance` decimal(18,2) DEFAULT NULL,
  `DocumentAirportHandling` decimal(18,2) DEFAULT NULL,
  `OtherEastMalaysiaAmount` decimal(18,2) DEFAULT NULL,
  `OtherEastMalaysiaTitle` varchar(245) DEFAULT NULL,
  `EHUHandlingChargesEM` decimal(18,2) DEFAULT NULL,
  `CostingID` int DEFAULT NULL,
  `CostingCode` varchar(45) DEFAULT NULL,
  `MAWBID` int DEFAULT NULL,
  `MAWBNo` varchar(45) DEFAULT NULL,
  `InvoiceID` int DEFAULT NULL,
  `Deleted` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_costingheader`
--

LOCK TABLES `ao_costingheader` WRITE;
/*!40000 ALTER TABLE `ao_costingheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_costingheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_costingitem`
--

DROP TABLE IF EXISTS `ao_costingitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_costingitem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CostingCode` varchar(10) NOT NULL,
  `CostingItemDesc` longtext,
  `Status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_costingitem`
--

LOCK TABLES `ao_costingitem` WRITE;
/*!40000 ALTER TABLE `ao_costingitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_costingitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_countrylist`
--

DROP TABLE IF EXISTS `ao_countrylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_countrylist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CountryCode` longtext NOT NULL,
  `CountryName` longtext,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_countrylist`
--

LOCK TABLES `ao_countrylist` WRITE;
/*!40000 ALTER TABLE `ao_countrylist` DISABLE KEYS */;
INSERT INTO `ao_countrylist` VALUES (1,'AD','ANDORRA',NULL,NULL,NULL,NULL,1),(2,'AE','UNITED ARAB EMIRATES',NULL,NULL,NULL,NULL,1),(3,'AF','AFGHANISTAN',NULL,NULL,NULL,NULL,1),(4,'AG','ANTIGUA AND BARBUDA',NULL,NULL,NULL,NULL,1),(5,'AI','ANGUILLA',NULL,NULL,'2020-03-04 07:54:16',NULL,2),(6,'AL','ALBANIA',NULL,NULL,NULL,NULL,1),(7,'AM','ARMENIA',NULL,NULL,NULL,NULL,1),(8,'AN','NETHERLANDS ANTILLES',NULL,NULL,NULL,NULL,1),(9,'AO','ANGOLA',NULL,NULL,'2020-03-04 07:59:28',NULL,2),(10,'AQ','ANTARCTICA',NULL,NULL,'2020-03-04 07:58:54',NULL,2),(11,'AR','ARGENTINA',NULL,NULL,NULL,NULL,1),(12,'AS','AMERICAN SAMOA',NULL,NULL,NULL,NULL,1),(13,'AT','AUSTRIA',NULL,NULL,NULL,NULL,1),(14,'AU','AUSTRALIA',NULL,NULL,NULL,NULL,1),(15,'AW','ARUBA',NULL,NULL,NULL,NULL,1),(16,'AZ','AZERBAIJAN',NULL,NULL,NULL,NULL,1),(17,'BA','BOSNIA AND HERZEGOWINA',NULL,NULL,NULL,NULL,1),(18,'BB','BARBADOS',NULL,NULL,NULL,NULL,1),(19,'BD','BANGLADESH',NULL,NULL,NULL,NULL,1),(20,'BE','BELGIUM',NULL,NULL,NULL,NULL,1),(21,'BF','BURKINA FASO',NULL,NULL,NULL,NULL,1),(22,'BG','BULGARIA',NULL,NULL,NULL,NULL,1),(23,'BH','BAHRAIN',NULL,NULL,NULL,NULL,1),(24,'BI','BURUNDI',NULL,NULL,NULL,NULL,1),(25,'BJ','BENIN',NULL,NULL,NULL,NULL,1),(26,'BM','BERMUDA',NULL,NULL,NULL,NULL,1),(27,'BN','BRUNEI DARUSSALAM',NULL,NULL,NULL,NULL,1),(28,'BO','BOLIVIA',NULL,NULL,NULL,NULL,1),(29,'BR','BRAZIL',NULL,NULL,NULL,NULL,1),(30,'BS','BAHAMAS',NULL,NULL,NULL,NULL,1),(31,'BT','BHUTAN',NULL,NULL,NULL,NULL,1),(32,'BU','BURMA (MYANMAR)',NULL,NULL,NULL,NULL,1),(33,'BV','BOUVET ISLAND',NULL,NULL,NULL,NULL,1),(34,'BW','BOTSWANA',NULL,NULL,NULL,NULL,1),(35,'BY','BELARUS',NULL,NULL,NULL,NULL,1),(36,'BZ','BELIZE',NULL,NULL,NULL,NULL,1),(37,'CA','CANADA',NULL,NULL,NULL,NULL,1),(38,'CC','COCOS (KEELING) ISLANDS',NULL,NULL,NULL,NULL,1),(39,'CF','CENTRAL AFRICAN REPUBLIC',NULL,NULL,NULL,NULL,1),(40,'CG','CONGO',NULL,NULL,NULL,NULL,1),(41,'CH','SWITZERLAND',NULL,NULL,NULL,NULL,1),(42,'CI','COTE D’IVOIRE',NULL,NULL,NULL,NULL,1),(43,'CK','COOK ISLANDS',NULL,NULL,NULL,NULL,1),(44,'CL','CHILE',NULL,NULL,NULL,NULL,1),(45,'CM','CAMEROON',NULL,NULL,NULL,NULL,1),(46,'CN','CHINA',NULL,NULL,NULL,NULL,1),(47,'CO','COLOMBIA',NULL,NULL,NULL,NULL,1),(48,'CR','COSTA RICA',NULL,NULL,NULL,NULL,1),(49,'CS','CZECH AND SLOVAK',NULL,NULL,NULL,NULL,1),(50,'CU','CUBA',NULL,NULL,NULL,NULL,1),(51,'CV','CAPE VERDE',NULL,NULL,NULL,NULL,1),(52,'CX','CHRISTMAS ISLAND',NULL,NULL,NULL,NULL,1),(53,'CY','CYPRUS',NULL,NULL,NULL,NULL,1),(54,'CZ','CZECH REPUPLIC',NULL,NULL,NULL,NULL,1),(55,'DD','GERMAN DEMOCRATIC REPUBLIC',NULL,NULL,NULL,NULL,1),(56,'DE','GERMANY',NULL,NULL,NULL,NULL,1),(57,'DJ','DJBOUTI',NULL,NULL,NULL,NULL,1),(58,'DK','DENMARK',NULL,NULL,NULL,NULL,1),(59,'DM','DOMINICA',NULL,NULL,NULL,NULL,1),(60,'DO','DOMINICAN REPUBLIC',NULL,NULL,NULL,NULL,1),(61,'DZ','ALGERIA',NULL,NULL,'2020-03-12 16:56:29',NULL,2),(62,'EC','ECUADOR',NULL,NULL,NULL,NULL,1),(63,'EE','ESTONIA',NULL,NULL,NULL,NULL,1),(64,'EG','EGYPT',NULL,NULL,NULL,NULL,1),(65,'EH','WESTERN SAHARA',NULL,NULL,NULL,NULL,1),(66,'ER','ERITREA',NULL,NULL,NULL,NULL,1),(67,'ES','SPAIN AND CANARY ISLANDS',NULL,NULL,NULL,NULL,1),(68,'ET','ETHIOPIA',NULL,NULL,NULL,NULL,1),(69,'FI','FINLAND',NULL,NULL,NULL,NULL,1),(70,'FJ','FIJI',NULL,NULL,NULL,NULL,1),(71,'FK','FALKLAND ISLANDS',NULL,NULL,NULL,NULL,1),(72,'FM','MICRONESIA',NULL,NULL,NULL,NULL,1),(73,'FO','FAROE ISLANDS',NULL,NULL,NULL,NULL,1),(74,'FR','FRANCE',NULL,NULL,NULL,NULL,1),(75,'GA','GABON',NULL,NULL,NULL,NULL,1),(76,'GB','UNITED KINGDOM',NULL,NULL,NULL,NULL,1),(77,'GD','GRENADA',NULL,NULL,NULL,NULL,1),(78,'GE','GEORGIA',NULL,NULL,NULL,NULL,1),(79,'GF','FRENCH GUIANA',NULL,NULL,NULL,NULL,1),(80,'GH','GHANA',NULL,NULL,NULL,NULL,1),(81,'GI','GIBRALTAR',NULL,NULL,NULL,NULL,1),(82,'GL','GREENLAND',NULL,NULL,NULL,NULL,1),(83,'GM','GAMBIA',NULL,NULL,NULL,NULL,1),(84,'GN','GUINEA',NULL,NULL,NULL,NULL,1),(85,'GP','GUADELOUPE',NULL,NULL,NULL,NULL,1),(86,'GQ','EQUAORITAL GUINEA',NULL,NULL,NULL,NULL,1),(87,'GR','GREECE',NULL,NULL,NULL,NULL,1),(88,'GT','GUATEMALA',NULL,NULL,NULL,NULL,1),(89,'GU','GUAM',NULL,NULL,NULL,NULL,1),(90,'GW','GUINEA-BISSAU',NULL,NULL,NULL,NULL,1),(91,'GY','GUYANA',NULL,NULL,NULL,NULL,1),(92,'HK','HONG KONG',NULL,NULL,NULL,NULL,1),(93,'HM','HEARD AND MCDONALD',NULL,NULL,NULL,NULL,1),(94,'HN','HONDURAS',NULL,NULL,NULL,NULL,1),(95,'HR','CROATIA',NULL,NULL,NULL,NULL,1),(96,'HT','HAITI',NULL,NULL,NULL,NULL,1),(97,'HU','HUNGARY',NULL,NULL,NULL,NULL,1),(98,'ID','INDONESIA',NULL,NULL,NULL,NULL,1),(99,'IE','IRELAND',NULL,NULL,NULL,NULL,1),(100,'IL','ISRAEL',NULL,NULL,NULL,NULL,1),(101,'IN','INDIA',NULL,NULL,NULL,NULL,1),(102,'IO','BRITISH INDIAN OCEAN TERRITORY',NULL,NULL,NULL,NULL,1),(103,'IQ','IRAQ',NULL,NULL,NULL,NULL,1),(104,'IR','IRAN',NULL,NULL,NULL,NULL,1),(105,'IS','CELAND',NULL,NULL,NULL,NULL,1),(106,'IT','ITALY',NULL,NULL,NULL,NULL,1),(107,'JM','JAMAICA',NULL,NULL,NULL,NULL,1),(108,'JO','JORDAN',NULL,NULL,NULL,NULL,1),(109,'JP','JAPAN',NULL,NULL,NULL,NULL,1),(110,'KE','KENYA',NULL,NULL,NULL,NULL,1),(111,'KG','KYRGYZSTAN',NULL,NULL,NULL,NULL,1),(112,'KH','CAMBODIA',NULL,NULL,NULL,NULL,1),(113,'KI','KIRIBATI',NULL,NULL,NULL,NULL,1),(114,'KM','COMOROS',NULL,NULL,NULL,NULL,1),(115,'KN','SAINT KITTS AND NEVIS',NULL,NULL,NULL,NULL,1),(116,'KP','KOREA, DEMOCRATIC PEOPLE’S REPUBLIC',NULL,NULL,NULL,NULL,1),(117,'KR','KOREA, REPUBLIC OF',NULL,NULL,NULL,NULL,1),(118,'KW','KUWAIT',NULL,NULL,NULL,NULL,1),(119,'KY','CAYMAN ISLANDS',NULL,NULL,NULL,NULL,1),(120,'KZ','KAZAKHSTAN',NULL,NULL,NULL,NULL,1),(121,'LA','LAO PEOPLE’S DEMOCRATIC REPUBLIC',NULL,NULL,NULL,NULL,1),(122,'LB','LEBONAN',NULL,NULL,NULL,NULL,1),(123,'LC','SAINT LUCIA',NULL,NULL,NULL,NULL,1),(124,'LI','LIECTENSTEIN',NULL,NULL,NULL,NULL,1),(125,'LK','SRI LANKA',NULL,NULL,NULL,NULL,1),(126,'LR','LIBERIA',NULL,NULL,NULL,NULL,1),(127,'LS','LESOTHO',NULL,NULL,NULL,NULL,1),(128,'LT','LITHUANIA',NULL,NULL,NULL,NULL,1),(129,'LU','LUXEMBOURG',NULL,NULL,NULL,NULL,1),(130,'LV','LATVIA',NULL,NULL,NULL,NULL,1),(131,'LY','LLIBYAN ARAB JAMAHIRIYA',NULL,NULL,NULL,NULL,1),(132,'MA','MOROCCO',NULL,NULL,NULL,NULL,1),(133,'MC','MONACO',NULL,NULL,NULL,NULL,1),(134,'MD','MOLDOVA, REPUBLIC OF',NULL,NULL,NULL,NULL,1),(135,'MG','MADAGASCAR',NULL,NULL,NULL,NULL,1),(136,'MH','MARSHALL ISLANDS',NULL,NULL,NULL,NULL,1),(137,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',NULL,NULL,NULL,NULL,1),(138,'ML','MALI',NULL,NULL,NULL,NULL,1),(139,'MM','MYANMAR',NULL,NULL,NULL,NULL,1),(140,'MN','MONGOLIA',NULL,NULL,NULL,NULL,1),(141,'MO','MACAU',NULL,NULL,NULL,NULL,1),(142,'MP','NORTHERN MARIANA ISLANDS',NULL,NULL,NULL,NULL,1),(143,'MQ','MARTINIQUE',NULL,NULL,NULL,NULL,1),(144,'MR','MAURITANIA',NULL,NULL,NULL,NULL,1),(145,'MS','MONTSERRAT',NULL,NULL,NULL,NULL,1),(146,'MT','MALTA',NULL,NULL,NULL,NULL,1),(147,'MU','MAURITITUS',NULL,NULL,NULL,NULL,1),(148,'MV','MALDIVES',NULL,NULL,NULL,NULL,1),(149,'MW','MALAWI',NULL,NULL,NULL,NULL,1),(150,'MX','MEXICO',NULL,NULL,NULL,NULL,1),(151,'MY','MALAYSIA',NULL,NULL,NULL,NULL,1),(152,'MZ','MOZAMBIQUE',NULL,NULL,NULL,NULL,1),(153,'NA','NAMIBIA',NULL,NULL,NULL,NULL,1),(154,'NC','NEW CALEDONIA',NULL,NULL,NULL,NULL,1),(155,'NE','NIGER',NULL,NULL,NULL,NULL,1),(156,'NF','NORFOLK ISLAND',NULL,NULL,NULL,NULL,1),(157,'NG','NIGERIA',NULL,NULL,NULL,NULL,1),(158,'NI','NICARAGUA',NULL,NULL,NULL,NULL,1),(159,'NL','NETHERLANDS',NULL,NULL,NULL,NULL,1),(160,'NO','NORWAY',NULL,NULL,NULL,NULL,1),(161,'NP','NEPAL',NULL,NULL,NULL,NULL,1),(162,'NR','NAURU',NULL,NULL,NULL,NULL,1),(163,'NT','NEUTRAL ZONE',NULL,NULL,NULL,NULL,1),(164,'NU','NIUE',NULL,NULL,NULL,NULL,1),(165,'NZ','NEW ZEALAND',NULL,NULL,NULL,NULL,1),(166,'OM','OMAN',NULL,NULL,NULL,NULL,1),(167,'PA','PANAMA',NULL,NULL,NULL,NULL,1),(168,'PE','PERU',NULL,NULL,NULL,NULL,1),(169,'PF','FRENCH POLYNESIA',NULL,NULL,NULL,NULL,1),(170,'PG','PAPUA NEW GUINEA',NULL,NULL,NULL,NULL,1),(171,'PH','PHILIPPINES',NULL,NULL,NULL,NULL,1),(172,'PK','PAKISTAN',NULL,NULL,NULL,NULL,1),(173,'PL','POLAND',NULL,NULL,NULL,NULL,1),(174,'PM','ST. PIERRE AND MIQUELON',NULL,NULL,NULL,NULL,1),(175,'PN','PITCAIRA',NULL,NULL,NULL,NULL,1),(176,'PR','PUERTO RICO',NULL,NULL,NULL,NULL,1),(177,'PT','PORTUGAL',NULL,NULL,NULL,NULL,1),(178,'PW','PALAU',NULL,NULL,NULL,NULL,1),(179,'PY','PARAGUAY',NULL,NULL,NULL,NULL,1),(180,'QA','QATAR',NULL,NULL,NULL,NULL,1),(181,'RE','REUNION',NULL,NULL,NULL,NULL,1),(182,'RO','ROMANIA',NULL,NULL,NULL,NULL,1),(183,'RU','RUSSIAN FEDERATION',NULL,NULL,NULL,NULL,1),(184,'RW','RWANDA',NULL,NULL,NULL,NULL,1),(185,'SA','SAUDI ARABIA',NULL,NULL,NULL,NULL,1),(186,'SB','SOLOMON ISLANDS',NULL,NULL,NULL,NULL,1),(187,'SC','SEYCHELLES',NULL,NULL,NULL,NULL,1),(188,'SD','SUDAN',NULL,NULL,NULL,NULL,1),(189,'SE','SWEDEN',NULL,NULL,NULL,NULL,1),(190,'SG','SINGAPORE',NULL,NULL,NULL,NULL,1),(191,'SH','ST. HELENA',NULL,NULL,NULL,NULL,1),(192,'SI','SLOVENIA',NULL,NULL,NULL,NULL,1),(193,'SJ','SVALBARD AND JAN MAYEN ISLANDS',NULL,NULL,NULL,NULL,1),(194,'SK','SLOVAKIA',NULL,NULL,NULL,NULL,1),(195,'SL','SIERRA LEONE',NULL,NULL,NULL,NULL,1),(196,'SM','SAN MARINO',NULL,NULL,NULL,NULL,1),(197,'SN','SENEGAL',NULL,NULL,NULL,NULL,1),(198,'SO','SOMALIA',NULL,NULL,NULL,NULL,1),(199,'SR','SURINAM',NULL,NULL,NULL,NULL,1),(200,'ST','SAO TOME AND PRINCIPE',NULL,NULL,NULL,NULL,1),(201,'SU','RUSSIA',NULL,NULL,NULL,NULL,1),(202,'SV','EL SALVADOR',NULL,NULL,NULL,NULL,1),(203,'SY','SYRIAN ARAB REPUBLIC',NULL,NULL,NULL,NULL,1),(204,'SZ','SWAZILAND',NULL,NULL,NULL,NULL,1),(205,'TC','TURKS AND CAICOS ISLANDS',NULL,NULL,NULL,NULL,1),(206,'TD','CHAD',NULL,NULL,NULL,NULL,1),(207,'TF','FRENCH SOUTHERN TERRITORIES',NULL,NULL,NULL,NULL,1),(208,'TG','TOGO',NULL,NULL,NULL,NULL,1),(209,'TH','THAILAND',NULL,NULL,NULL,NULL,1),(210,'TJ','TAJISKISTAN',NULL,NULL,NULL,NULL,1),(211,'TK','TOKELAU',NULL,NULL,NULL,NULL,1),(212,'TM','TURMENISTAN',NULL,NULL,NULL,NULL,1),(213,'TN','TUNISIA',NULL,NULL,NULL,NULL,1),(214,'TO','TONGA',NULL,NULL,NULL,NULL,1),(215,'TP','EAST TIMOR',NULL,NULL,NULL,NULL,1),(216,'TR','TURKEY',NULL,NULL,NULL,NULL,1),(217,'TT','TRINIDAD AND TOBAGO',NULL,NULL,NULL,NULL,1),(218,'TV','TUVALU',NULL,NULL,NULL,NULL,1),(219,'TW','TAIWAN',NULL,NULL,NULL,NULL,1),(220,'TZ','TANZANIA, UNITED REPUBLIC OF',NULL,NULL,NULL,NULL,1),(221,'UA','UKRAINE',NULL,NULL,NULL,NULL,1),(222,'UG','UGANDA',NULL,NULL,NULL,NULL,1),(223,'UM','UNITED STATES MINOR',NULL,NULL,NULL,NULL,1),(224,'UN','VIET NAM',NULL,NULL,NULL,NULL,1),(225,'US','UNITED STATES',NULL,NULL,NULL,NULL,1),(226,'UY','URUGUAY',NULL,NULL,NULL,NULL,1),(227,'UZ','UZBEKISTAN',NULL,NULL,NULL,NULL,1),(228,'VA','HOLY SEE (VATICAN CITY STATE)',NULL,NULL,NULL,NULL,1),(229,'VC','SAINT VINCENT AND THE GRENADINES',NULL,NULL,NULL,NULL,1),(230,'VE','VENEZUELA',NULL,NULL,NULL,NULL,1),(231,'VG','VIRGIN ISLANDS (BRITISH)',NULL,NULL,NULL,NULL,1),(232,'VI','VIRGIN ISLANDS (U.S.)',NULL,NULL,NULL,NULL,1),(233,'VN','VIETNAM',NULL,NULL,NULL,NULL,1),(234,'VU','VANUATU',NULL,NULL,NULL,NULL,1),(235,'WF','WALLIS AND FUTUNA ISLANDS',NULL,NULL,NULL,NULL,1),(236,'WS','SAMOA',NULL,NULL,NULL,NULL,1),(237,'XU','RUSSIAN FEDERATION',NULL,NULL,NULL,NULL,1),(238,'YE','YEMEN',NULL,NULL,NULL,NULL,1),(239,'YT','MAYOTTE',NULL,NULL,NULL,NULL,1),(240,'YU','YUGOSLAVIA',NULL,NULL,NULL,NULL,1),(241,'ZA','SOUTH AFRICA',NULL,NULL,NULL,NULL,1),(242,'ZM','ZAMBIA',NULL,NULL,NULL,NULL,1),(243,'ZR','ZAIRIE',NULL,NULL,NULL,NULL,1),(244,'ZW','ZIMBABWE',NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `ao_countrylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_customerlist`
--

DROP TABLE IF EXISTS `ao_customerlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_customerlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CustomerCode` char(10) NOT NULL,
  `CustomerName` varchar(255) DEFAULT NULL,
  `CompanyCode` varchar(255) DEFAULT NULL,
  `PersonInCharge` varchar(255) DEFAULT NULL,
  `PersonInCharge2` varchar(255) DEFAULT NULL,
  `CustomerType` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `PaymentTermCode` varchar(10) DEFAULT NULL,
  `StaffID` varchar(100) DEFAULT NULL,
  `Address1` varchar(250) DEFAULT NULL,
  `Address2` text,
  `Address3` varchar(250) DEFAULT NULL,
  `Address4` varchar(250) DEFAULT NULL,
  `TelNo1` varchar(50) DEFAULT NULL,
  `TelNo2` varchar(50) DEFAULT NULL,
  `FaxNo1` varchar(50) DEFAULT NULL,
  `FaxNo2` varchar(50) DEFAULT NULL,
  `Email1` varchar(50) DEFAULT NULL,
  `Email2` varchar(50) DEFAULT NULL,
  `Remark` varchar(250) DEFAULT NULL,
  `UpdatedBy` varchar(20) DEFAULT NULL,
  `LastModified` timestamp NULL DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `CreateOn` timestamp NULL DEFAULT NULL,
  `Status` char(4) DEFAULT NULL,
  `ActiveQuotation` varchar(10) DEFAULT NULL,
  `CoLoaderCode` varchar(10) DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  `GSTID` varchar(45) DEFAULT NULL,
  `TPCustomerCode` varchar(25) DEFAULT NULL,
  `PaymentTermCodeReversed` varchar(10) DEFAULT NULL,
  `InvoiceDisputeTerm` varchar(10) DEFAULT NULL,
  `InvoiceInterestRate` smallint DEFAULT NULL,
  `InvoicePrefix` varchar(15) DEFAULT NULL,
  `InvoiceRunningNo` int DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_CustomerList` (`CustomerCode`),
  KEY `CustomerList_1` (`Status`,`CustomerName`),
  KEY `CustomerList_1152` (`CustomerType`),
  KEY `CustomerList_1197` (`CustomerName`),
  KEY `CustomerList_1272` (`CompanyCode`),
  KEY `CustomerList_2` (`CoLoaderCode`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `CompanyCode` (`CompanyCode`),
  KEY `StaffID` (`StaffID`),
  KEY `ActiveQuotation` (`ActiveQuotation`),
  KEY `CoLoaderCode` (`CoLoaderCode`),
  KEY `OrderingName` (`Ordering`,`CustomerName`),
  KEY `customercodestaffid` (`CustomerCode`,`StaffID`),
  KEY `NameCode` (`CustomerCode`,`CustomerName`,`Ordering`,`CustomerType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_customerlist`
--

LOCK TABLES `ao_customerlist` WRITE;
/*!40000 ALTER TABLE `ao_customerlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_customerlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_driverlist`
--

DROP TABLE IF EXISTS `ao_driverlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_driverlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DriverNo` varchar(15) DEFAULT NULL,
  `DriverName` varchar(145) DEFAULT NULL,
  `ContactNo` varchar(45) DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Active` varchar(1) DEFAULT 'A',
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_driverlist`
--

LOCK TABLES `ao_driverlist` WRITE;
/*!40000 ALTER TABLE `ao_driverlist` DISABLE KEYS */;
INSERT INTO `ao_driverlist` VALUES (1,'WUR2087','Armizan','016-201 6367','wmw8136','WUR2087','A',NULL,NULL),(2,'WRA6321','Izzat','016-201 6394',NULL,'abc123','A',NULL,NULL),(3,'WTR 2396','Fasniza','016-201 6327',NULL,NULL,'A',NULL,NULL),(4,'WRF 8375','Shaiful','016-201 6349',NULL,NULL,'A',NULL,NULL),(5,'WNW 5483','Sakri','016-201 6354',NULL,NULL,'A',NULL,NULL),(6,'WPB 6539','Faizal','016-201 6302',NULL,NULL,'A',NULL,NULL),(7,'WSU 4519','Adnan','016-201 6391',NULL,NULL,'A',NULL,NULL),(8,'WTR 9193','Khidzir','016-9545574',NULL,NULL,'A',NULL,NULL),(9,'WTR 9391','Zamri','016-201 6382',NULL,NULL,'A',NULL,NULL),(10,'WUQ 1032','Erzzad','016-201 6375',NULL,NULL,'A',NULL,NULL),(11,'WWP 8935','Rizal','018-391 6202',NULL,NULL,'A',NULL,NULL),(12,'WVH 783','Shahrul','016-2016357',NULL,NULL,'A',NULL,NULL),(13,'WWP 1157','WWP 1157',NULL,NULL,NULL,'A',NULL,NULL),(14,'WRW 3858','Irwan','016-201 6384',NULL,NULL,'A',NULL,NULL),(15,'WTQ 6293','WTQ 6293',NULL,NULL,NULL,'A',NULL,NULL),(16,'W6115F','Bustami','013-8853058',NULL,NULL,'A',NULL,NULL);
/*!40000 ALTER TABLE `ao_driverlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_freightconfig`
--

DROP TABLE IF EXISTS `ao_freightconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_freightconfig` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ParmKey` varchar(20) DEFAULT NULL,
  `ParamDesc` longtext,
  `ParamValue` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_freightconfig`
--

LOCK TABLES `ao_freightconfig` WRITE;
/*!40000 ALTER TABLE `ao_freightconfig` DISABLE KEYS */;
INSERT INTO `ao_freightconfig` VALUES (1,'COMNAM','Company Name','ASIAONE EXPRESS (M) SDN BHD'),(2,'COMADD','Company Address','No 38, Jalan Anggerik Mokara 31/60, Kota Kemuning,\noff jalan pipit'),(3,'COMTEL','Company Telephone','603-51249888'),(4,'COMFAX','Company Fax','603-51247888'),(5,'COMEML','Company Email','general@asiaone.com.my'),(6,'COMURL','Company URL','http://www.asiaone.com.my'),(7,'COMDIR','Associate Director','Damien Tan'),(10,'COMREG','Company Registration','569431 - P'),(11,'INVPFX','Invoice Prefix','201302'),(41,'TNC','Terms & Condition','<div>\r <div class=\"fr-view\">\r <p><strong>Coverage area&nbsp;</strong></p>\r <p>We cover areas within Klang Valley only.</p>\r <p>Please do contact us if your address is out of our coverage area and interested to place order.</p>\r <p>&nbsp;</p>\r <p><strong>Delivery time</strong></p>\r <p>2 - 3 working day from order date.</p>\r <p>Our working hours are Monday to Friday, 9am to 5pm. We are closed on Saturday, Sunday and Public Holiday.</p>\r <p>&nbsp;</p>\r <p><strong>Delivery charge</strong></p>\r <p>RM6.00 to RM15.00 based on location.</p>\r <p>&nbsp;</p>\r <p><strong>Contact details&nbsp;</strong></p>\r <p>Contact us through&nbsp;<strong>Whatsapp at +6012 311 1531</strong>&nbsp;or email&nbsp;<strong><a href=\"mailto:info@sfnoodles.com\" data-fr-linked=\"true\">info@sfnoodles.com</a>&nbsp;</strong>if you need further assistance.</p>\r </div>\r </div>'),(13,'ODRPFX','Order Prefix','ODR'),(15,'GSTID','Company GST ID','001826537472'),(38,'COLORPRIMARY','Primary Color','#0B7AF1'),(39,'COLORSECONDARY','Secondary Color','#00acee'),(40,'FAQ','FAQ','<p><strong>1. Will you deliver my order during MCO?</strong></p>\n<p>Yes, we have gotten approval from MITI to operate throughout the MCO. Our trucks are able to travel on the road to deliver to your doorsteps.</p>\n<p>As requested by the government authorities, all of our trucks are disinfected before and after each batch of deliveries. Also, our drivers are equipped with face mask and hand sanitizer, to ensure they are safe to perform the deliveries.</p>\n<p>&nbsp;</p>\n<p><strong>2. When will I receive my order and where do you deliver to?</strong></p>\n<p>We do&nbsp;<em><strong>2-3 days delivery</strong>&nbsp;</em>from Monday to Friday only. Orders made on Friday, Saturday and Sunday will be delivered on the following Monday. Please refer to&nbsp;<em>Terms and Conditions</em>&nbsp;page for more information.&nbsp;</p>'),(22,'COMLOGO','Company Logo Image','logo.png'),(23,'COMSHO','Company Short Name','Soon Fatt Foods'),(35,'BTBANKNAME','Bank Transfer Bank Name','Public Bank'),(36,'BTACCNO','Bank Transfer Account No','3176375112'),(37,'BTACCNAME','Bank Transfer Account Name','Soon Fatt Foods Sdn Bhd'),(31,'COMADD1','Address1','No 38, Jalan Anggerik Mokara 31/60, '),(32,'COMADD2','Address2','Kota Kemuning, 40460 Shah Alam,'),(33,'COMADD3','Address3','Selangor, Malaysia.'),(34,'WHATSAPP','Whatsapp No','0123111531'),(43,'TOTALROOMS','Total Rooms','4');
/*!40000 ALTER TABLE `ao_freightconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_freightrunno`
--

DROP TABLE IF EXISTS `ao_freightrunno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_freightrunno` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Prefix` longtext,
  `LastNo` int DEFAULT NULL,
  `RunDesc` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_freightrunno`
--

LOCK TABLES `ao_freightrunno` WRITE;
/*!40000 ALTER TABLE `ao_freightrunno` DISABLE KEYS */;
INSERT INTO `ao_freightrunno` VALUES (1,'CUS',12071,'Customer'),(2,'QUA',6087,'Quotation'),(3,'201302',38573,'Invoice'),(4,'COS',0,'Costing Item'),(5,'ODR',104069,'Order'),(8,'HAWB',50,'HAWB'),(9,'JOB',58671,'Job No');
/*!40000 ALTER TABLE `ao_freightrunno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_invoicedetail`
--

DROP TABLE IF EXISTS `ao_invoicedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_invoicedetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(255) DEFAULT NULL,
  `InvoiceID` int DEFAULT NULL,
  `ConsignmentNo` varchar(255) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `ItemDesc` varchar(255) DEFAULT NULL,
  `AccountsCode` varchar(255) DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_invoicedetail`
--

LOCK TABLES `ao_invoicedetail` WRITE;
/*!40000 ALTER TABLE `ao_invoicedetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_invoicedetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_invoiceheader`
--

DROP TABLE IF EXISTS `ao_invoiceheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_invoiceheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(100) NOT NULL,
  `ConsignmentNo` varchar(100) NOT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `InvoiceAmount` decimal(18,2) NOT NULL,
  `InvoiceDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`),
  KEY `ConsignmentNo` (`ConsignmentNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_invoiceheader`
--

LOCK TABLES `ao_invoiceheader` WRITE;
/*!40000 ALTER TABLE `ao_invoiceheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_invoiceheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_itemcatlist`
--

DROP TABLE IF EXISTS `ao_itemcatlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_itemcatlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ItemCat` longtext NOT NULL,
  `ItemCatDesc` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_itemcatlist`
--

LOCK TABLES `ao_itemcatlist` WRITE;
/*!40000 ALTER TABLE `ao_itemcatlist` DISABLE KEYS */;
INSERT INTO `ao_itemcatlist` VALUES (1,'CHR','Charges'),(2,'SBH','Sabah'),(3,'SRW','Sarawak');
/*!40000 ALTER TABLE `ao_itemcatlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_itemlist`
--

DROP TABLE IF EXISTS `ao_itemlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_itemlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ItemCode` varchar(20) NOT NULL,
  `ItemType` varchar(20) NOT NULL,
  `ItemCat` varchar(100) DEFAULT NULL,
  `ItemCatID` int DEFAULT NULL,
  `AccountsCode` varchar(50) NOT NULL,
  `ItemDesc` varchar(200) DEFAULT NULL,
  `PortID` varchar(20) DEFAULT NULL,
  `Status` varchar(1) DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  `UomType` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ItemCode` (`ItemCode`),
  KEY `ItemCatID` (`ItemCatID`),
  KEY `AccountsCode` (`AccountsCode`),
  KEY `PortID` (`PortID`),
  KEY `Status` (`Status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_itemlist`
--

LOCK TABLES `ao_itemlist` WRITE;
/*!40000 ALTER TABLE `ao_itemlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_itemlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_itemtypelist`
--

DROP TABLE IF EXISTS `ao_itemtypelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_itemtypelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ItemType` longtext NOT NULL,
  `ItemTypeDesc` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_itemtypelist`
--

LOCK TABLES `ao_itemtypelist` WRITE;
/*!40000 ALTER TABLE `ao_itemtypelist` DISABLE KEYS */;
INSERT INTO `ao_itemtypelist` VALUES (1,'EHU','Direct Parcel Express'),(2,'EXP','Parcel Express'),(4,'IMP','Import ');
/*!40000 ALTER TABLE `ao_itemtypelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_leavelist`
--

DROP TABLE IF EXISTS `ao_leavelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_leavelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `LeaveType` varchar(145) DEFAULT NULL,
  `FromDate` date DEFAULT NULL,
  `ToDate` date DEFAULT NULL,
  `NoOfDays` decimal(10,1) DEFAULT NULL,
  `Approved` smallint DEFAULT NULL,
  `LeaveDesc` varchar(245) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_leavelist`
--

LOCK TABLES `ao_leavelist` WRITE;
/*!40000 ALTER TABLE `ao_leavelist` DISABLE KEYS */;
INSERT INTO `ao_leavelist` VALUES (11,54,'Full Day','2020-09-01','2020-09-02',1.0,NULL,NULL,'admin','2020-09-19 07:54:39',NULL,'2020-09-19 07:54:39'),(12,54,'Full Day','2020-09-01','2020-09-25',-24.0,NULL,NULL,'admin','2020-09-19 09:03:22',NULL,'2020-09-19 09:03:22'),(13,54,'Full Day','2020-09-01','2020-09-08',0.0,NULL,NULL,'admin','2020-09-19 09:06:20',NULL,'2020-09-19 09:06:20'),(14,54,'Full Day','2020-09-01','2020-09-08',11.0,NULL,NULL,'admin','2020-09-19 09:07:12',NULL,'2020-09-19 09:07:12');
/*!40000 ALTER TABLE `ao_leavelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_loggedin`
--

DROP TABLE IF EXISTS `ao_loggedin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_loggedin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DSN` varchar(45) DEFAULT NULL,
  `User` varchar(45) DEFAULT NULL,
  `LoggedDate` datetime DEFAULT NULL,
  `SessionID` varchar(230) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_loggedin`
--

LOCK TABLES `ao_loggedin` WRITE;
/*!40000 ALTER TABLE `ao_loggedin` DISABLE KEYS */;
INSERT INTO `ao_loggedin` VALUES (262,'asiaone_old7','54','2020-04-17 01:00:07','4Uj5hb9sJKhBMaD1w2Ac'),(305,'akishop','165','2020-05-13 15:47:48','A5fWxauHnHqJxCWLobUx'),(400,'soonfatt','174','2020-05-28 10:30:33','dUlFAVoYHMQxPLJisvEU'),(422,'shop','174','2020-07-05 11:41:07','jrlYcaKJ9xDvz23cJEg7'),(446,'spa1','174','2020-09-09 18:44:40','55M4G0SjewV49PEGhpA6'),(512,'spa2','174','2020-12-22 11:20:21','EoDj2UGE_DdCuH9p3PaX'),(520,'spainvestor','182','2020-12-29 14:56:01','4ET2QGFLqr5uh4NpK62x');
/*!40000 ALTER TABLE `ao_loggedin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_mawb`
--

DROP TABLE IF EXISTS `ao_mawb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_mawb` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AWBNo` varchar(30) NOT NULL,
  `ETD` datetime DEFAULT NULL,
  `ETA` datetime DEFAULT NULL,
  `POL` varchar(20) DEFAULT NULL,
  `POD` varchar(20) DEFAULT NULL,
  `AirlineID` varchar(20) DEFAULT NULL,
  `FlightNo` varchar(20) DEFAULT NULL,
  `AgentID` varchar(20) DEFAULT NULL,
  `CoLoaderCode` varchar(20) DEFAULT NULL,
  `Express` varchar(45) DEFAULT NULL,
  `JobNo` int DEFAULT NULL,
  `Remark` text,
  `Pieces` int DEFAULT NULL,
  `Weight` decimal(18,2) DEFAULT NULL,
  `Status` datetime DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL,
  `LockDate` datetime DEFAULT NULL,
  `LockBy` varchar(20) DEFAULT NULL,
  `BillComplete` datetime DEFAULT NULL,
  `BillType` varchar(3) DEFAULT NULL,
  `ShipperCode` varchar(45) DEFAULT NULL,
  `ConsigneeCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AWBNo` (`AWBNo`),
  KEY `LockDate` (`LockDate`,`LockBy`),
  KEY `LockDate_2` (`LockDate`),
  KEY `LockBy` (`LockBy`),
  KEY `BillComplete` (`BillComplete`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_mawb`
--

LOCK TABLES `ao_mawb` WRITE;
/*!40000 ALTER TABLE `ao_mawb` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_mawb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_notificationlist`
--

DROP TABLE IF EXISTS `ao_notificationlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_notificationlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Message` text NOT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_notificationlist`
--

LOCK TABLES `ao_notificationlist` WRITE;
/*!40000 ALTER TABLE `ao_notificationlist` DISABLE KEYS */;
INSERT INTO `ao_notificationlist` VALUES (1,'test','2014-03-27 03:12:42'),(2,'Miss Pickup - ODR00109 - NEFFUL (M) S/B','2014-03-29 03:22:34'),(3,'Out Of Area - ODR00214 - SAN-EI INDUSTRIES & TRADING SDN BHD','2014-04-04 15:05:12'),(4,'Out Of Area - ODR00218 - IGROW MARKETING','2014-04-04 16:27:51'),(5,'Out Of Area - ODR00286 - HANG SAN ELECT. SUPPLIES S/B','2014-04-07 12:46:07'),(6,'Out Of Area - ODR00313 - NEW ERA ENTERPRISE','2014-04-07 15:46:49'),(7,'Out Of Area - ODR00387 - VICTOR TECHNOLOGY ASIA SDN BHD','2014-04-08 14:40:04'),(8,'Out Of Area - ODR00395 - Eita Electric Sdn Bhd (MNX)','2014-04-08 15:35:23'),(9,'Out Of Area - ODR00474 - X POLER GAMER','2014-04-09 19:42:16'),(10,'Out Of Area - ODR00481 - Dascomprinters Sdn Bhd','2014-04-09 19:44:05'),(11,'Out Of Area - ODR00608 - HD ELECTRICAL SDN BHD','2014-04-11 15:42:18'),(12,'Out Of Area - ODR00629 - WAI LIM TRADING','2014-04-12 14:17:43'),(13,'Out Of Area - ODR00710 - Aspek Liga S/B (MNX)','2014-04-14 15:30:20'),(14,'Out Of Area - ODR00848 - RED PEAK POWER SDN BHD','2014-04-16 15:21:38'),(15,'Out Of Area - ODR01072 - CYC NETWORKING SDN BHD','2014-04-21 14:41:01'),(16,'Out Of Area - ODR01086 - Eita Electric Sdn Bhd (MNX)','2014-04-21 15:18:50'),(17,'Out Of Area - ODR01147 - Tozen (M) Sdn Bhd (MNX)','2014-04-22 14:44:47'),(18,'Out Of Area - ODR01464 - Heap Heng Leong (MNX)','2014-04-28 20:55:59'),(19,'Out Of Area - ODR01889 - Jitbo Trading Sdn Bhd (Pref)','2014-05-06 14:53:42'),(20,'Out Of Area - ODR01884 - Perniagaan Jaya Hias (MNX)','2014-05-06 15:03:10'),(21,'Out Of Area - ODR02229 - Cyc Manufacturing (m) Sdn Bhd (pref)','2014-05-12 14:43:34'),(22,'Out Of Area - ODR02250 - Cynics Solution Sdn Bhd','2014-05-12 16:23:15'),(23,'Out Of Area - ODR02572 - Yau Enterprise (MNX)','2014-05-20 10:34:27'),(24,'Out Of Area - ODR02662 - JHH Trading Sdn Bhd (MNX)','2014-05-20 15:48:03'),(25,'Out Of Area - ODR02664 - Ascent Electric Sdn Bhd (MNX)','2014-05-20 16:06:55'),(26,'Out Of Area - ODR02822 - Yau Enterprise (MNX)','2014-05-22 17:14:20'),(27,'Out Of Area - ODR02911 - Zen Ten Audio Enterprise (MNX)','2014-05-24 09:59:49'),(28,'Out Of Area - ODR03141 - MAX SAFETY & ENGINEERING S/B','2014-05-28 16:13:45'),(29,'Out Of Area - ODR03227 - Benton Corporation Sdn Bhd','2014-05-30 17:22:45'),(30,'Out Of Area - ODR03368 - YLT Design (MNX)','2014-06-03 20:29:15'),(31,'Out Of Area - ODR03430 - Sun Power Automation Sdn Bhd (MNX)','2014-06-04 14:55:22'),(32,'Out Of Area - ODR03448 - Yulita Parts Trd (M) Sdn Bhd (MNX)','2014-06-04 16:07:33'),(33,'Out Of Area - ODR04810 - Perniagaan Elektrik Aman Bina (MNX)','2014-06-27 17:12:05'),(34,'Out Of Area - ODR04895 - TRACTMOTOR PART (M) S/B','2014-06-30 15:27:35'),(35,'Out Of Area - ODR05171 - Fipper Marketing Sdn Bhd (Bersatu )','2014-07-04 14:46:19'),(36,'Out Of Area - ODR05356 - HA Fabric Cottage (MNX)','2014-07-09 09:49:15'),(37,'Out Of Area - ODR06077 - Perniagaan Elektrik Aman Bina (MNX)','2014-07-24 10:50:21'),(38,'Out Of Area - ODR07394 - Stardex Trading Sdn Bhd','2014-08-20 14:32:23'),(39,'Out Of Area - ODR07399 - Gallery Dominance Sdn Bhd (MNX)','2014-08-20 14:32:43'),(40,'Out Of Area - ODR07403 - Syamaltra Sdn Bhd (MNX)','2014-08-20 15:55:46'),(41,'Out Of Area - ODR07404 - AO Advance Automation Sdn Bhd (MNX)','2014-08-20 15:59:25'),(42,'Out Of Area - ODR07405 - Ascent Electric Sdn Bhd (MNX)','2014-08-20 15:59:36'),(43,'Out Of Area - ODR08138 - UNT MARKETING SDN BHD','2014-09-04 12:44:42'),(44,'Out Of Area - ODR09274 - Perhiasan Zennik Sdn Bhd (MNX)','2014-09-25 14:54:18'),(45,'Out Of Area - ODR10391 -  Polyspeed Trading  Sdn Bhd','2014-10-18 13:19:58'),(46,'Out Of Area - ODR11381 - Automaster Spare Parts (M) Sdn Bhd (MNX)','2014-11-05 16:39:54'),(47,'Out Of Area - ODR19966 - Gallery Dominance Sdn Bhd (MNX)','2015-05-14 09:23:53'),(48,'Out Of Area - ODR67816 - Tritech Sdn Bhd (MNX)','2018-08-03 16:19:11');
/*!40000 ALTER TABLE `ao_notificationlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderdetail`
--

DROP TABLE IF EXISTS `ao_orderdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` int DEFAULT NULL,
  `OrderNo` varchar(10) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `ShipperID` varchar(8) DEFAULT NULL,
  `ProductID` int DEFAULT NULL,
  `ProductNo` varchar(45) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `RequiredDate` date DEFAULT NULL,
  `Freight` decimal(10,0) DEFAULT NULL,
  `SalesTax` decimal(10,0) DEFAULT NULL,
  `TimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TransactStatus` varchar(25) DEFAULT NULL,
  `InvoiceAmount` decimal(10,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `Qty` int DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `PriceID` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UserID_idx` (`UserID`),
  KEY `ShipperID_idx` (`ShipperID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderdetail`
--

LOCK TABLES `ao_orderdetail` WRITE;
/*!40000 ALTER TABLE `ao_orderdetail` DISABLE KEYS */;
INSERT INTO `ao_orderdetail` VALUES (1,1,'104068',174,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-23 03:10:55',NULL,294.00,NULL,3,'2020-11-23 03:10:55',38,98.00,'Completed','1'),(2,2,'104069',174,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-23 03:14:08',NULL,392.00,NULL,4,'2020-11-23 03:14:08',38,98.00,'Completed','1');
/*!40000 ALTER TABLE `ao_orderdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderheader`
--

DROP TABLE IF EXISTS `ao_orderheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderNo` varchar(10) DEFAULT NULL,
  `CustomerCode` varchar(255) DEFAULT NULL,
  `ZoneID` varchar(255) DEFAULT NULL,
  `NoOfCarton` varchar(255) DEFAULT NULL,
  `Remarks` text,
  `Status` varchar(255) DEFAULT NULL,
  `OrderBy` varchar(255) DEFAULT NULL,
  `OrderPhone` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `PaymentMethod` varchar(45) DEFAULT NULL,
  `DeliveryCharges` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderNo` (`OrderNo`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `ZoneID` (`ZoneID`),
  KEY `zonestatus` (`ZoneID`,`Status`,`CreatedOn`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderheader`
--

LOCK TABLES `ao_orderheader` WRITE;
/*!40000 ALTER TABLE `ao_orderheader` DISABLE KEYS */;
INSERT INTO `ao_orderheader` VALUES (1,'104068',NULL,NULL,NULL,NULL,'Order Created',NULL,NULL,'01110340242','2020-11-23 03:10:55',NULL,NULL,'ORDERCREATED',174,'Cash',NULL),(2,'104069',NULL,NULL,NULL,NULL,'Order Created',NULL,NULL,'01110340242','2020-11-23 03:14:07',NULL,NULL,'ORDERCREATED',174,'Cash',NULL);
/*!40000 ALTER TABLE `ao_orderheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderstatus`
--

DROP TABLE IF EXISTS `ao_orderstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` varchar(255) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `HideSelect` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderID` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderstatus`
--

LOCK TABLES `ao_orderstatus` WRITE;
/*!40000 ALTER TABLE `ao_orderstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_orderstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymentdetail`
--

DROP TABLE IF EXISTS `ao_paymentdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymentdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DebtorCode` int DEFAULT NULL,
  `PaymentID` int DEFAULT NULL,
  `TransactionID` int DEFAULT NULL,
  `PaidAmount` decimal(13,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymentdetail`
--

LOCK TABLES `ao_paymentdetail` WRITE;
/*!40000 ALTER TABLE `ao_paymentdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_paymentdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymentlist`
--

DROP TABLE IF EXISTS `ao_paymentlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymentlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DebtorCode` int DEFAULT NULL,
  `PaymentMode` varchar(145) DEFAULT NULL,
  `Amount` decimal(13,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `PaymentRef` varchar(45) DEFAULT NULL,
  `PaymentDesc` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymentlist`
--

LOCK TABLES `ao_paymentlist` WRITE;
/*!40000 ALTER TABLE `ao_paymentlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_paymentlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymenttermlist`
--

DROP TABLE IF EXISTS `ao_paymenttermlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymenttermlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `PaymentTermCode` varchar(255) NOT NULL,
  `PaymentTermDesc` varchar(255) DEFAULT NULL,
  `PaymentTermDays` int DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `Paymenttermcode` (`PaymentTermCode`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymenttermlist`
--

LOCK TABLES `ao_paymenttermlist` WRITE;
/*!40000 ALTER TABLE `ao_paymenttermlist` DISABLE KEYS */;
INSERT INTO `ao_paymenttermlist` VALUES (1,'14D','14 Days',14,2),(2,'30D','30 Days',30,1),(3,'45D','45 Days',45,3),(4,'60D','60 Days',60,2),(5,'90D','90 Days',90,1),(6,'COD','C.O.D',0,1);
/*!40000 ALTER TABLE `ao_paymenttermlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_payrolldetail`
--

DROP TABLE IF EXISTS `ao_payrolldetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_payrolldetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `PayrollID` int DEFAULT NULL,
  `Title` varchar(245) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_payrolldetail`
--

LOCK TABLES `ao_payrolldetail` WRITE;
/*!40000 ALTER TABLE `ao_payrolldetail` DISABLE KEYS */;
INSERT INTO `ao_payrolldetail` VALUES (2,4,'abnnas',221.30,'admin','2020-09-22 19:12:34','admin','2020-09-22 19:19:08');
/*!40000 ALTER TABLE `ao_payrolldetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_payrollheader`
--

DROP TABLE IF EXISTS `ao_payrollheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_payrollheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `FromDate` date DEFAULT NULL,
  `ToDate` date DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_payrollheader`
--

LOCK TABLES `ao_payrollheader` WRITE;
/*!40000 ALTER TABLE `ao_payrollheader` DISABLE KEYS */;
INSERT INTO `ao_payrollheader` VALUES (1,183,'2020-09-01','2020-09-30','New',NULL,NULL,NULL,NULL),(2,183,'2020-09-01','2020-09-30','New','admin','2020-09-22 10:55:47',NULL,NULL),(3,183,'2020-10-01','2020-10-31','New','admin','2020-09-22 10:56:41','admin','2020-09-22 17:48:14'),(4,183,'2020-08-01','2020-08-31','New','admin','2020-09-22 17:50:40',NULL,NULL),(5,183,'2020-07-01','2020-07-31','New','admin','2020-09-22 17:50:53',NULL,NULL),(6,188,'2020-11-01','2020-11-30','New','admin','2020-11-04 03:19:42',NULL,NULL),(7,182,'2020-11-01','2020-11-30','New','admin','2020-11-04 15:57:01',NULL,NULL),(8,182,'2020-08-01','2020-11-30','New','liubao','2020-11-06 06:13:44',NULL,NULL);
/*!40000 ALTER TABLE `ao_payrollheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_personlist`
--

DROP TABLE IF EXISTS `ao_personlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_personlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClientID` int DEFAULT NULL,
  `FirstName` varchar(145) DEFAULT NULL,
  `LastName` varchar(145) DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Mobile` varchar(45) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `PreAlert` varchar(2) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `UpdatedOn` date DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(145) DEFAULT NULL,
  `PersonInCharge` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ClientID` (`ClientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_personlist`
--

LOCK TABLES `ao_personlist` WRITE;
/*!40000 ALTER TABLE `ao_personlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_personlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_portmaster`
--

DROP TABLE IF EXISTS `ao_portmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_portmaster` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `PortID` varchar(20) NOT NULL,
  `PortName` varchar(50) DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `CountryCode` varchar(30) DEFAULT NULL,
  `CountryName` varchar(100) DEFAULT NULL,
  `Sequence` int DEFAULT '1',
  `PortType` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PortID` (`PortID`),
  KEY `State` (`State`),
  KEY `CountryCode` (`CountryCode`),
  KEY `Sequence` (`Sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_portmaster`
--

LOCK TABLES `ao_portmaster` WRITE;
/*!40000 ALTER TABLE `ao_portmaster` DISABLE KEYS */;
INSERT INTO `ao_portmaster` VALUES (1,'BFT','BEAUFORTH','SABAH','MY','MALAYSIA',11,NULL),(2,'BKI','KOTA KINABALU','SABAH','MY','MALAYSIA',9,NULL),(3,'BRN','BRUNEI DARUSSALAM','SARAWAK','MY','MALAYSIA',12,NULL),(4,'BTR','BINTAGOR','SARAWAK','MY','MALAYSIA',13,NULL),(5,'BTU','BINTULU','SARAWAK','MY','MALAYSIA',3,NULL),(6,'BWN','BERUNAI','SARAWAK','MY','MALAYSIA',14,NULL),(7,'KBR','KOTA BARAM','SARAWAK','MY','MALAYSIA',15,NULL),(8,'KBU','KOTA BELUD','SABAH','MY','MALAYSIA',16,NULL),(9,'KCH','KUCHING','SARAWAK','MY','MALAYSIA',1,NULL),(10,'KGU','KENINGAU','SABAH','MY','MALAYSIA',17,NULL),(11,'KMR','KOTA MARUDU','SABAH','MY','MALAYSIA',18,NULL),(12,'KNK','KUNAK','SABAH','MY','MALAYSIA',19,NULL),(13,'KNW','KINOWIT','SARAWAK','MY','MALAYSIA',20,NULL),(14,'KPT','KAPIT','SARAWAK','MY','MALAYSIA',21,NULL),(15,'KSM','KOTA SAMARAHAN','SARAWAK','MY','MALAYSIA',22,NULL),(16,'KUD','KUDAT','SABAH','MY','MALAYSIA',23,NULL),(17,'KUL','KLIA SEPANG','SELANGOR','MY','MALAYSIA',27,NULL),(18,'LBU','LABUAN','SABAH','MY','MALAYSIA',6,NULL),(19,'LDU','LAHAD DATU','SABAH','MY','MALAYSIA',9,NULL),(20,'LMN','LIMBANG','SABAH','MY','MALAYSIA',25,NULL),(21,'LWS','LAWAS','SABAH','MY','MALAYSIA',26,NULL),(22,'MKH','MUKAH','SARAWAK','MY','MALAYSIA',27,NULL),(23,'MRD','MARUDI','SARAWAK','MY','MALAYSIA',28,NULL),(24,'MYY','MIRI','SARAWAK','MY','MALAYSIA',7,NULL),(25,'PAR','PAPAR','SABAH','MY','MALAYSIA',29,NULL),(26,'POT','PORT KLANG','SELANGOR','MY','MALAYSIA',30,NULL),(27,'RNU','RANAU','SABAH','MY','MALAYSIA',31,NULL),(28,'SAM','SRI AMAN','SARAWAK','MY','MALAYSIA',10,NULL),(29,'SBW','SIBU','SARAWAK','MY','MALAYSIA',2,NULL),(30,'SDK','SANDAKAN','SABAH','MY','MALAYSIA',8,NULL),(31,'SMJ','SIMANJAN','SARAWAK','MY','MALAYSIA',32,NULL),(32,'SPN','SEMPORNA','SABAH','MY','MALAYSIA',33,NULL),(33,'SPT','SIPITANG','SABAH','MY','MALAYSIA',34,NULL),(34,'SRK','SARIKEI','SARAWAK','MY','MALAYSIA',35,NULL),(35,'SRN','SERIAN','SARAWAK','MY','MALAYSIA',36,NULL),(36,'STK','SARATOK','SARAWAK','MY','MALAYSIA',37,NULL),(37,'TAM','TAMBUNAN','SABAH','MY','MALAYSIA',38,NULL),(38,'TNM','TENOM','SABAH','MY','MALAYSIA',39,NULL),(39,'TPD','TELUPID','SABAH','MY','MALAYSIA',57,NULL),(40,'TWU','TAWAU','SABAH','MY','MALAYSIA',8,NULL),(41,'KPW','KOTA PADAWAN','SARAWAK','MY','MALAYSIA',89,NULL),(42,'MTG','MATANG','SARAWAK','MY','MALAYSIA',61,NULL),(43,'BTG','BETONG','SARAWAK','MY','MALAYSIA',46,NULL),(44,'OTH','OTHER','SARAWAK','MY','MALAYSIA',127,NULL),(45,'OTR','OTHER','SABAH','MY','MALAYSIA',NULL,NULL),(47,'BAU','BAU','SARAWAK','MY','MALAYSIA',122,NULL);
/*!40000 ALTER TABLE `ao_portmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_postcode`
--

DROP TABLE IF EXISTS `ao_postcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_postcode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Town` varchar(245) DEFAULT NULL,
  `State` varchar(145) DEFAULT NULL,
  `PostCode` varchar(10) DEFAULT NULL,
  `Price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_postcode`
--

LOCK TABLES `ao_postcode` WRITE;
/*!40000 ALTER TABLE `ao_postcode` DISABLE KEYS */;
INSERT INTO `ao_postcode` VALUES (4,'Petaling Jaya','Selangor Darul Ehsan','46150',15.00),(5,'Petaling Jaya','Selangor Darul Ehsan','46200',15.00),(6,'Petaling Jaya','Selangor Darul Ehsan','46300',15.00),(7,'Petaling Jaya','Selangor Darul Ehsan','46350',15.00),(8,'Petaling Jaya','Selangor Darul Ehsan','46400',15.00),(9,'Petaling Jaya','Selangor Darul Ehsan','47300',15.00),(10,'Petaling Jaya','Selangor Darul Ehsan','47301',15.00),(11,'Petaling Jaya','Selangor Darul Ehsan','47400',15.00),(12,'Petaling Jaya','Selangor Darul Ehsan','47800',15.00),(13,'Petaling Jaya','Selangor Darul Ehsan','47810',15.00),(14,'Petaling Jaya','Selangor Darul Ehsan','47820',15.00),(15,'Puchong','Selangor Darul Ehsan','47100',6.00),(16,'Puchong','Selangor Darul Ehsan','47110',10.00),(17,'Puchong','Selangor Darul Ehsan','47120',10.00),(18,'Puchong','Selangor Darul Ehsan','47130',10.00),(19,'Puchong','Selangor Darul Ehsan','47140',6.00),(20,'Puchong','Selangor Darul Ehsan','47150',6.00),(21,'Puchong','Selangor Darul Ehsan','47160',6.00),(22,'Puchong','Selangor Darul Ehsan','47170',6.00),(23,'Puchong','Selangor Darul Ehsan','47180',8.00),(24,'Puchong','Selangor Darul Ehsan','47190',6.00),(25,'Kuala Lumpur','WP Kuala Lumpur','50088',15.00),(26,'Kuala Lumpur','WP Kuala Lumpur','50100',15.00),(27,'Kuala Lumpur','WP Kuala Lumpur','50250',15.00),(28,'Kuala Lumpur','WP Kuala Lumpur','50300',15.00),(29,'Kuala Lumpur','WP Kuala Lumpur','50350',15.00),(30,'Kuala Lumpur','WP Kuala Lumpur','50400',15.00),(31,'Kuala Lumpur','WP Kuala Lumpur','50450',15.00),(32,'Kuala Lumpur','WP Kuala Lumpur','50460',15.00),(33,'Kuala Lumpur','WP Kuala Lumpur','50470',15.00),(34,'Kuala Lumpur','WP Kuala Lumpur','50480',15.00),(35,'Kuala Lumpur','WP Kuala Lumpur','50490',15.00),(36,'Kuala Lumpur','WP Kuala Lumpur','50500',15.00),(37,'Kuala Lumpur','WP Kuala Lumpur','50586',15.00),(38,'Kuala Lumpur','WP Kuala Lumpur','50603',15.00),(39,'Kuala Lumpur','WP Kuala Lumpur','51000',15.00),(40,'Kuala Lumpur','WP Kuala Lumpur','51100',15.00),(41,'Kuala Lumpur','WP Kuala Lumpur','51200',15.00),(42,'Kuala Lumpur','WP Kuala Lumpur','52000',15.00),(43,'Kuala Lumpur','WP Kuala Lumpur','52100',15.00),(44,'Kuala Lumpur','WP Kuala Lumpur','52200',15.00),(45,'Kuala Lumpur','WP Kuala Lumpur','53000',15.00),(46,'Kuala Lumpur','WP Kuala Lumpur','53100',15.00),(47,'Kuala Lumpur','WP Kuala Lumpur','53200',15.00),(48,'Kuala Lumpur','WP Kuala Lumpur','53300',15.00),(49,'Kuala Lumpur','WP Kuala Lumpur','54000',15.00),(50,'Kuala Lumpur','WP Kuala Lumpur','54100',15.00),(51,'Kuala Lumpur','WP Kuala Lumpur','54200',15.00),(52,'Kuala Lumpur','WP Kuala Lumpur','55000',15.00),(53,'Kuala Lumpur','WP Kuala Lumpur','55100',15.00),(54,'Kuala Lumpur','WP Kuala Lumpur','55200',15.00),(55,'Kuala Lumpur','WP Kuala Lumpur','55300',15.00),(56,'Kuala Lumpur','WP Kuala Lumpur','56000',15.00),(57,'Kuala Lumpur','WP Kuala Lumpur','56100',15.00),(58,'Kuala Lumpur','WP Kuala Lumpur','57000',15.00),(59,'Kuala Lumpur','WP Kuala Lumpur','57100',15.00),(78,'Shah Alam','Selangor Darul Ehsan','40000',15.00),(79,'Shah Alam','Selangor Darul Ehsan','40100',15.00),(80,'Shah Alam','Selangor Darul Ehsan','40150',15.00),(81,'Shah Alam','Selangor Darul Ehsan','40160',15.00),(82,'Shah Alam','Selangor Darul Ehsan','40170',15.00),(83,'Shah Alam','Selangor Darul Ehsan','40200',15.00),(84,'Shah Alam','Selangor Darul Ehsan','40300',15.00),(85,'Shah Alam','Selangor Darul Ehsan','40400',15.00),(86,'Shah Alam','Selangor Darul Ehsan','40450',15.00),(87,'Shah Alam','Selangor Darul Ehsan','40460',15.00),(88,'Shah Alam','Selangor Darul Ehsan','40470',15.00),(89,'Subang Jaya','Selangor Darul Ehsan','47200',15.00),(90,'Subang Jaya','Selangor Darul Ehsan','47500',10.00),(91,'Subang Jaya','Selangor Darul Ehsan','47600',10.00),(92,'Subang Jaya','Selangor Darul Ehsan','47610',10.00),(93,'Subang Jaya','Selangor Darul Ehsan','47620',10.00),(94,'Subang Jaya','Selangor Darul Ehsan','47630',10.00),(95,'Subang Jaya','Selangor Darul Ehsan','47640',10.00),(96,'Subang Jaya','Selangor Darul Ehsan','47650',10.00),(99,'Seri Kembangan','Selangor','43300',15.00),(101,'Putrajaya','Wilayah Persekutuan','62000',15.00),(103,'Cyberjaya','Selangor','63000',15.00),(104,'Cheras','Selangor','43200',15.00),(105,'Dengkil','Selangor','43800',15.00),(106,'Kuala Lumpur','Wilayah Persekutuan','59000',15.00),(107,'Kuala Lumpur','Wilayah Persekutuan','59200',15.00),(108,'Kuala Lumpur','Wilayah Persekutuan','59100',15.00),(109,'Kuala Lumpur','Wilayah Persekutuan','58200',15.00),(110,'Kuala Lumpur','Wilayah Persekutuan','50490',15.00),(111,'Kuala Lumpur','Wilayah Persekutuan','50470',15.00),(112,'Kuala Lumpur','Wilayah Persekutuan','58200',15.00);
/*!40000 ALTER TABLE `ao_postcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_prealert`
--

DROP TABLE IF EXISTS `ao_prealert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_prealert` (
  `id` int NOT NULL AUTO_INCREMENT,
  `awb` varchar(45) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `awb` (`awb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_prealert`
--

LOCK TABLES `ao_prealert` WRITE;
/*!40000 ALTER TABLE `ao_prealert` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_prealert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_prealertmail`
--

DROP TABLE IF EXISTS `ao_prealertmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_prealertmail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `billingheaderID` int DEFAULT NULL,
  `AWBID` int DEFAULT NULL,
  `sent` int DEFAULT NULL,
  `remarks` varchar(145) DEFAULT NULL,
  `sentDate` datetime DEFAULT NULL,
  `failDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `billingheaderID_UNIQUE` (`billingheaderID`),
  KEY `awbid` (`AWBID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_prealertmail`
--

LOCK TABLES `ao_prealertmail` WRITE;
/*!40000 ALTER TABLE `ao_prealertmail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_prealertmail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_pricelist`
--

DROP TABLE IF EXISTS `ao_pricelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_pricelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ProductID` int DEFAULT NULL,
  `Unit` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Uom` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `UnitPriceMember` decimal(10,2) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `ServiceTimes` varchar(45) DEFAULT NULL,
  `NoDelete` smallint DEFAULT NULL,
  `SingleItem` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_pricelist`
--

LOCK TABLES `ao_pricelist` WRITE;
/*!40000 ALTER TABLE `ao_pricelist` DISABLE KEYS */;
INSERT INTO `ao_pricelist` VALUES (7,2,1,10.50,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,3,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,4,1,13.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,5,1,5.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,6,1,12.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,7,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,12,1,4.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,13,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,14,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,15,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,16,20,10.00,'piece',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,17,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,18,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,19,1,7.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,1,1,98.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,20,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,21,1,5.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,22,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,24,1,21.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,30,1,112.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,31,1,12.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,25,1,100.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,33,1,100.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,34,1,98.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,35,1,2688.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,36,1,2000.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,37,1,128.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,38,1,110.00,'One Time Pass',NULL,NULL,80.00,'Service','1',NULL,1),(43,38,1,1000.00,'Package A - 14 Time Pass',NULL,NULL,NULL,'Service','14',1,1),(44,38,1,2000.00,'Package B - 26 Time Pass',NULL,NULL,NULL,'Service','26',1,1),(45,38,1,3000.00,'Package C - 40 Times Pass',NULL,NULL,NULL,'Service','40',1,1),(47,38,1,4000.00,'Package D - 54 Times Pass',NULL,NULL,NULL,'Service','54',1,1);
/*!40000 ALTER TABLE `ao_pricelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_productlist`
--

DROP TABLE IF EXISTS `ao_productlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_productlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ProductID` varchar(8) DEFAULT NULL,
  `DepartmentID` varchar(8) DEFAULT NULL,
  `Category` int DEFAULT NULL,
  `IDSKU` varchar(8) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `Quantity` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Ranking` int DEFAULT NULL,
  `ProductDesc` text,
  `UnitsInStock` int DEFAULT '0',
  `UnitsInOrder` int DEFAULT '0',
  `Picture` blob,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `OriginalURL` text,
  `PrettyUrl` varchar(255) DEFAULT NULL,
  `DoneScrap` smallint DEFAULT NULL,
  `Youtube` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_productlist`
--

LOCK TABLES `ao_productlist` WRITE;
/*!40000 ALTER TABLE `ao_productlist` DISABLE KEYS */;
INSERT INTO `ao_productlist` VALUES (34,NULL,NULL,1,NULL,'SB96 - Health Series',NULL,NULL,NULL,'<p>商品详情</p>\n<p>SB96 拥有 3 大功效</p>\n<p>1. 杀菌消毒</p>\n<p>2. 消炎修复</p>\n<p>3. 消除疲劳</p>\n<p>► 完全无酒精 &bull; 无氯&nbsp;&bull;&nbsp;无杀菌剂 &bull; 无色素 &bull;&nbsp;无化学防腐剂</p>\n<p>► 完全是天然的成份哦!</p>\n<p>只需要RM98 ( SB96 100ml ) 就能把健康带回家！SB96 让你用得开心！ 用得安心！</p>\n<p>SB96 SIBEH GAOLAT Powerful Sterilizer During COVID - 19</p>\n<p>►Experimental report proven effectiveness up to 99.99%</p>\n<p>►SB96 3 Effective Uses</p>\n<p>1. Sterilization</p>\n<p>2. Decrease inflammation</p>\n<p>3. Reduce Tiredness</p>\n<p>Nature and health are our focus and needs in our products. It is the route we pursue towards a pure and healthy lifestyle and attitude. An inspiration from the mother nature, has been developed to now known as Light Activated Energy, which has the ability to improve our blood circulation. SB96 has 4 unique elements present, which gives SB96 the ability to improve metabolism, the healing of wounds, the ability to kill bacteria and many more. Experience 4.5 billion years of light together with the mother nature. Energy, harmony as well as prosperity, all given in one. SB96 wholeheartedly protects your health.</p>',10000000,0,NULL,NULL,NULL,1,NULL,'sb96-health-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/h3s05mEUAYs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(35,NULL,NULL,1,NULL,'WTE - Beauty Series',NULL,NULL,NULL,'<p>快速，有效，安全清洗！</p>\n<p>EASY, QUICK YET SAFE FOR CLEANING!</p>\n<p>In this era of digital technologies, EMR pollution is widespread. Affecting not just human beings but all life forms on Earth.</p>\n<p>Water, the essential needs for survival, is not only polluted with chemical waste. In fact, it is mainly polluted by EMR. More than half of the world\'s population consumes water of big water molecules resulting in increasing diseases caused by metabolic problems.</p>',10000000,0,NULL,NULL,NULL,3,NULL,'wte-beauty-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/JegUBlosVN0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(36,NULL,NULL,1,NULL,'LAE - Beauty Series',NULL,NULL,NULL,'<div class=\"s2\">\n<div class=\"full-desc ckeditor\">\n<p>快速，有效，安全清洗！</p>\n<p>EASY, QUICK YET SAFE FOR CLEANING!</p>\n<p>In this era of digital technologies, EMR pollution is widespread. Affecting not just human beings but all life forms on Earth.</p>\n<p>Water, the essential needs for survival, is not only polluted with chemical waste. In fact, it is mainly polluted by EMR. More than half of the world\'s population consumes water of big water molecules resulting in increasing diseases caused by metabolic problems.</p>\n</div>\n</div>',10000000,0,NULL,NULL,NULL,4,NULL,'lae-beauty-series',NULL,NULL),(37,NULL,NULL,1,NULL,'Ezurose - Beauty Series',NULL,NULL,NULL,'<p>Experience the finest skin care product, specially developed based on our core technology LAE Light Activated Energy.<br /><br />A beauty secret finally revealed. The world\'s first beauty and healthcare 2-in-1 product.<br />6 substantial efficacies and 4 unique elements are all you need to instantly restores skin elasticity.<br /><br />ezurose, a rose hidden within the light</p>\n<p>&nbsp;</p>\n<p>ezurose 936光的秘密，美颜大公开！</p>\n<p><strong>世界首创美容与保健双效合一的护肤产品</strong>，拥有&nbsp;<strong>LAE育成光能&nbsp;</strong>技术，<strong>天然成分&nbsp;</strong>和&nbsp;<strong>淡淡的玫瑰香</strong>。她就是女人的最佳选择！</p>\n<p><strong>6大功效&nbsp;</strong>和&nbsp;<strong>4大独特研发配方&nbsp;</strong>就是您恢复肌肤弹性的秘诀</p>\n<p>随身随地带着一瓶ezurose，让她的玫瑰香 散发出您的魅力</p>',10000000,0,NULL,NULL,NULL,2,NULL,'ezurose-beauty-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/Oai6nPGX8-I\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(38,NULL,NULL,2,NULL,'936 LAE Energy Room Treatment',NULL,NULL,NULL,'<p>商品详情</p>\n<p>SB96 拥有 3 大功效</p>\n<p>1. 杀菌消毒</p>\n<p>2. 消炎修复</p>\n<p>3. 消除疲劳</p>\n<p>► 完全无酒精 &bull; 无氯&nbsp;&bull;&nbsp;无杀菌剂 &bull; 无色素 &bull;&nbsp;无化学防腐剂</p>\n<p>► 完全是天然的成份哦!</p>\n<p>只需要RM98 ( SB96 100ml ) 就能把健康带回家！SB96 让你用得开心！ 用得安心！</p>\n<p>SB96 SIBEH GAOLAT Powerful Sterilizer During COVID - 19</p>\n<p>►Experimental report proven effectiveness up to 99.99%</p>\n<p>►SB96 3 Effective Uses</p>\n<p>1. Sterilization</p>\n<p>2. Decrease inflammation</p>\n<p>3. Reduce Tiredness</p>\n<p>Nature and health are our focus and needs in our products. It is the route we pursue towards a pure and healthy lifestyle and attitude. An inspiration from the mother nature, has been developed to now known as Light Activated Energy, which has the ability to improve our blood circulation. SB96 has 4 unique elements present, which gives SB96 the ability to improve metabolism, the healing of wounds, the ability to kill bacteria and many more. Experience 4.5 billion years of light together with the mother nature. Energy, harmony as well as prosperity, all given in one. SB96 wholeheartedly protects your health.</p>',1,0,NULL,NULL,NULL,1,NULL,'936-lae-energy-room-treatment',NULL,NULL);
/*!40000 ALTER TABLE `ao_productlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_programmaster`
--

DROP TABLE IF EXISTS `ao_programmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_programmaster` (
  `ProgramID` varchar(10) NOT NULL,
  `Seq` int DEFAULT NULL,
  `ProgramDesc` varchar(20) DEFAULT NULL,
  `ProgramURL` varchar(50) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(20) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_programmaster`
--

LOCK TABLES `ao_programmaster` WRITE;
/*!40000 ALTER TABLE `ao_programmaster` DISABLE KEYS */;
INSERT INTO `ao_programmaster` VALUES ('CSG010    ',3,'Consignment         ','ConsignmentMain.aspx                              ',NULL,NULL,NULL,NULL),('CST010    ',5,'Costing             ','CostingMain.aspx                                  ',NULL,NULL,NULL,NULL),('CUS010    ',2,'Customer            ','CustomerMain.aspx                                 ',NULL,NULL,NULL,NULL),('INQ010    ',1,'Inquiry             ','InquiryMain.aspx                                  ',NULL,NULL,NULL,NULL),('INV010    ',4,'Invoice             ','BillingMain.aspx                                  ',NULL,NULL,NULL,NULL),('MNT010    ',6,'Maintenance         ','Admin.aspx                                        ',NULL,NULL,NULL,NULL),('RPT010    ',7,'Reporting           ','ReportingMain.aspx                                ',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ao_programmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_puchcarddetail`
--

DROP TABLE IF EXISTS `ao_puchcarddetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_puchcarddetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `intime` varchar(145) DEFAULT NULL,
  `outtime` varchar(145) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `PunchType` varchar(45) DEFAULT NULL,
  `PunchDate` datetime DEFAULT NULL,
  `Token` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_puchcarddetail`
--

LOCK TABLES `ao_puchcarddetail` WRITE;
/*!40000 ALTER TABLE `ao_puchcarddetail` DISABLE KEYS */;
INSERT INTO `ao_puchcarddetail` VALUES (16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2020-09-25 16:37:01','04caae48df968e2148b5019085084a282a381bd98f54202b76027d7f38426a9d0a3ac12bc0e47806b70abcdc0d99f9cf2e3cde3989b8ff4eac1d00bc4e09c4ed'),(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkout','2020-09-25 16:40:21','7669b56de4935ddf86d58953123f4fa7417350c3d2d002560dfb06ed4a54a21328e76bcc6be32515f90ceb24c156fbe746ed195870749bad4279bdfc43b0c541'),(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2020-09-25 16:46:25','7a43c3d042b3bc1eabfcdbc60709b66baf924a1a935e190087e6c53075396cd47f442818e19e0498bf0c9187ca9b751c7fd8ce4d6a04e06693b5b0a4e16892fd'),(19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2020-09-25 16:47:03','d434f6a4231b85944620b7618c4f41454b80c6375004e841e56f02e8c3471a5cac3dc39d7b85cf515825d4f382c773771e0b0b880fee4f67fc826302a1e8316e'),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkout','2020-09-25 16:47:37','fe92a7d3eb8f229c1ed48b4a9ca988fc79750056e86af74c81e87ad2dc9069e3cde97ca89abcc7e1f5033d12ac3ec44a23ad1631a1f6c1b25d035b69a02aefc2'),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkout','2020-09-25 17:13:41','9e4677eb14924731b364727efe63905ae8700ef5d5e0257da50801eca591aeb8c88c38b0845c4077c61d7d265edfee2703c40b1b1cd84cfe5e97c932007f9eba'),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkout','2020-09-25 17:34:00','e680f10d4894397670ec0ad47d1edae50a72c8801ebbc33997032b410e7c0c8d8db952446eeb908d358e45c60803bd3899cc2642a15b61363ca4b54abeb5d235'),(23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkin','2020-09-25 17:35:06','5975ecf47facbad71576c5498c84c88a9c373357da3db1949da4c37e183c8a80dbb83d706146546d792e1a5736c6d184d017b1ab1bd7ff5cb39a57db509dc55d'),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'929ba66a112e7f045149b0bd338a38f8330cbaf9bfa4955d0947ec14a70aa393180644ec679513c17f66910792cf383d5f8b35384d42648017197d3a5b94e680');
/*!40000 ALTER TABLE `ao_puchcarddetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_quotationdetail`
--

DROP TABLE IF EXISTS `ao_quotationdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_quotationdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `QuotationNo` char(10) NOT NULL,
  `PortID` varchar(15) DEFAULT NULL,
  `ItemCode` varchar(20) DEFAULT NULL,
  `UnitPrice` decimal(18,2) DEFAULT NULL,
  `ItemDesc` varchar(255) DEFAULT NULL,
  `CustomerCode` varchar(15) DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  `ItemType` varchar(3) DEFAULT NULL,
  `ItemCat` varchar(45) DEFAULT NULL,
  `AirlineCode` varchar(10) DEFAULT NULL,
  `QuotationForType` varchar(20) DEFAULT NULL,
  `VendorCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_QuotationDetail` (`QuotationNo`,`ItemCode`),
  KEY `QuotationDetail_1255` (`ItemCode`),
  KEY `QuotationDetail_16` (`UnitPrice`),
  KEY `QuotationDetail_3` (`ItemCode`,`UnitPrice`),
  KEY `QuotationNo` (`QuotationNo`),
  KEY `active` (`CustomerCode`,`Active`,`PortID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_quotationdetail`
--

LOCK TABLES `ao_quotationdetail` WRITE;
/*!40000 ALTER TABLE `ao_quotationdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_quotationdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_quotationheader`
--

DROP TABLE IF EXISTS `ao_quotationheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_quotationheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `QuotationNo` char(10) DEFAULT NULL,
  `CustomerCode` varchar(20) DEFAULT NULL,
  `CurrencyCode` varchar(10) DEFAULT NULL,
  `AgreementDate` timestamp NULL DEFAULT NULL,
  `EffectiveDate` timestamp NULL DEFAULT NULL,
  `ExpiryDate` timestamp NULL DEFAULT NULL,
  `Remark` text,
  `CreateOn` timestamp NULL DEFAULT NULL,
  `CreateBy` varchar(20) DEFAULT NULL,
  `LastModified` timestamp NULL DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL,
  `Active` varchar(20) DEFAULT 'D',
  `InsuranceBase` varchar(20) DEFAULT '0.5',
  `InsuranceMinCharge` decimal(11,2) DEFAULT '80.00',
  `InsuranceGovtTax` varchar(20) DEFAULT '6',
  `InsuranceStampFee` decimal(11,2) DEFAULT '10.00',
  `AirlineCode` varchar(30) DEFAULT NULL,
  `QuotationForType` varchar(20) DEFAULT NULL,
  `VendorCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `QuotationHeader_index_3` (`CustomerCode`),
  KEY `QuotationNo` (`QuotationNo`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_quotationheader`
--

LOCK TABLES `ao_quotationheader` WRITE;
/*!40000 ALTER TABLE `ao_quotationheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_quotationheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_salesreportconsignmentlist`
--

DROP TABLE IF EXISTS `ao_salesreportconsignmentlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_salesreportconsignmentlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(255) NOT NULL,
  `StaffID` varchar(255) NOT NULL,
  `ConsignmentNo` varchar(255) NOT NULL,
  `AWBNo` varchar(255) NOT NULL,
  `CustomerCode` varchar(255) NOT NULL,
  `CustomerName` varchar(255) NOT NULL,
  `Port` varchar(255) NOT NULL,
  `State` varchar(255) NOT NULL,
  `InvoiceDate` date NOT NULL,
  `TotalAmount` varchar(255) NOT NULL,
  `TotalWeight` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_salesreportconsignmentlist`
--

LOCK TABLES `ao_salesreportconsignmentlist` WRITE;
/*!40000 ALTER TABLE `ao_salesreportconsignmentlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_salesreportconsignmentlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_salesreportlist`
--

DROP TABLE IF EXISTS `ao_salesreportlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_salesreportlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(255) NOT NULL,
  `StaffID` varchar(255) NOT NULL,
  `CustomerCode` varchar(255) NOT NULL,
  `CustomerName` varchar(255) NOT NULL,
  `Port` varchar(255) NOT NULL,
  `State` varchar(255) NOT NULL,
  `InvoiceDate` date NOT NULL,
  `TotalTotalAmount` varchar(255) NOT NULL,
  `TotalWeight` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`),
  KEY `StaffID` (`StaffID`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `InvoiceDate` (`InvoiceDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_salesreportlist`
--

LOCK TABLES `ao_salesreportlist` WRITE;
/*!40000 ALTER TABLE `ao_salesreportlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_salesreportlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_staffdetail`
--

DROP TABLE IF EXISTS `ao_staffdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_staffdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `FirstName` varchar(245) DEFAULT NULL,
  `LastName` varchar(245) DEFAULT NULL,
  `Address1` varchar(245) DEFAULT NULL,
  `Address2` varchar(245) DEFAULT NULL,
  `City` varchar(245) DEFAULT NULL,
  `Postcode` varchar(20) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_staffdetail`
--

LOCK TABLES `ao_staffdetail` WRITE;
/*!40000 ALTER TABLE `ao_staffdetail` DISABLE KEYS */;
INSERT INTO `ao_staffdetail` VALUES (39,179,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 09:55:07','2020-09-21 09:55:07'),(40,180,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 10:18:00','2020-09-21 10:18:00'),(41,181,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 10:19:20','2020-09-21 10:19:20'),(42,182,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 11:18:13','2020-09-21 11:18:13'),(44,185,'akilonx@gmail.com','1111111111','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-29 02:29:06','2020-09-29 02:29:06'),(45,187,'akilonx@gmail.com','234234234','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor','Malaysia','2020-11-04 03:14:39','2020-11-04 03:14:39'),(46,188,'akilonx@gmail.com','2323','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor','Malaysia','2020-11-04 03:16:28','2020-11-04 03:16:28');
/*!40000 ALTER TABLE `ao_staffdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_staffgrouplist`
--

DROP TABLE IF EXISTS `ao_staffgrouplist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_staffgrouplist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `StartPage` varchar(255) DEFAULT NULL,
  `PageDesc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_staffgrouplist`
--

LOCK TABLES `ao_staffgrouplist` WRITE;
/*!40000 ALTER TABLE `ao_staffgrouplist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_staffgrouplist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_stafflist`
--

DROP TABLE IF EXISTS `ao_stafflist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_stafflist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StaffID` longtext,
  `StaffName` longtext,
  `Designation` longtext,
  `Department` longtext,
  `Email` longtext,
  `Password` longtext,
  `Active` varchar(1) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` longtext,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` longtext,
  `ContactNo` longtext,
  `Role` varchar(15) DEFAULT NULL,
  `Password2` varchar(145) DEFAULT NULL,
  `AgentCode` varchar(45) DEFAULT NULL,
  `Member` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_stafflist`
--

LOCK TABLES `ao_stafflist` WRITE;
/*!40000 ALTER TABLE `ao_stafflist` DISABLE KEYS */;
INSERT INTO `ao_stafflist` VALUES (37,'akilon','Akilon','Sales Exec','Supervisor','akilon@blackforestcreations.com','f0849d6bc21c233f6e73fc3a13dd866e:74GqnEXZvTVQHYMlCAY663jlAZ6OzoAh','','2013-12-24 05:12:27','asiaone-admin','2014-01-02 02:40:36','AKILON','011-123802242',NULL,'akilon',NULL,NULL),(54,'admin','Administrator','Administrator','Management','admin@sfnoodles.com.my','9dcf18b675d6de976bf3e2298bc2d147:sE4Wv6cdEFgRdq1Iw6g2RdTYdZk693Fz','','2014-01-06 17:12:21','Akilon','2014-10-27 15:10:39','Akilon','012-3111531',NULL,'password','',NULL),(175,'01110390242',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'password',NULL,NULL),(176,'0162221431',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'88888888',NULL,NULL),(174,'01110340242',NULL,NULL,'Management',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'password',NULL,NULL),(172,'0123707774',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'akilon@321',NULL,NULL),(177,'0123053761',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'494104',NULL,NULL),(178,'0123111531',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sp2304329$P',NULL,NULL),(179,'dahong',NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-21 09:55:07','admin',NULL,NULL,NULL,NULL,'dahong',NULL,NULL),(180,'hongpao',NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-21 10:18:00','admin',NULL,NULL,NULL,NULL,'hongpao',NULL,NULL),(181,'jinjun',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-09-21 10:19:20','admin',NULL,NULL,NULL,NULL,'jinjun',NULL,NULL),(182,'liubao',NULL,NULL,'Management',NULL,NULL,NULL,'2020-09-21 11:18:12','admin',NULL,NULL,NULL,NULL,'liubao',NULL,NULL),(185,'teststaff',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-09-29 02:29:06','liubao',NULL,NULL,NULL,NULL,'teststaff123',NULL,NULL),(186,'attendance',NULL,NULL,'Attendance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'attendance',NULL,NULL),(187,'asdfasdf',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:14:39','admin',NULL,NULL,NULL,NULL,'',NULL,NULL),(188,'asdfasdf1',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:16:27','admin',NULL,NULL,NULL,NULL,'abc123',NULL,NULL),(189,'sdasda',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:19:07','admin',NULL,NULL,NULL,NULL,'abc123',NULL,NULL);
/*!40000 ALTER TABLE `ao_stafflist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_statuslist`
--

DROP TABLE IF EXISTS `ao_statuslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_statuslist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(45) DEFAULT NULL,
  `Ordering` smallint DEFAULT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `HideSelect` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_statuslist`
--

LOCK TABLES `ao_statuslist` WRITE;
/*!40000 ALTER TABLE `ao_statuslist` DISABLE KEYS */;
INSERT INTO `ao_statuslist` VALUES (1,'Assigned',1,'ASSIGNED','Yes'),(2,'Pending Pickup',2,'PENDINGPICKUP',NULL),(3,'Miss Pickup',3,'MISSPICKUP',NULL),(4,'Out Of Area',4,'OUTOFAREA',NULL),(5,'Wrong Info',5,'WRONGINFO',NULL),(6,'Parcel Not Ready',6,'PARCELNOTREADY',NULL),(7,'Closed',7,'CLOSED',NULL),(8,'Completed',8,'COMPLETED',NULL),(9,'Order Created',9,'ORDERCREATED','Yes'),(10,'Order Confirmed',10,'ORDERCONFIRMED','Yes');
/*!40000 ALTER TABLE `ao_statuslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_stocklist`
--

DROP TABLE IF EXISTS `ao_stocklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_stocklist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ProductID` int DEFAULT NULL,
  `OrderID` int DEFAULT NULL,
  `Title` varchar(245) DEFAULT NULL,
  `Movement` varchar(10) DEFAULT NULL,
  `Qty` decimal(10,2) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_stocklist`
--

LOCK TABLES `ao_stocklist` WRITE;
/*!40000 ALTER TABLE `ao_stocklist` DISABLE KEYS */;
INSERT INTO `ao_stocklist` VALUES (1,34,NULL,'Stock In','In',20.00,NULL,NULL,NULL,NULL),(2,34,NULL,'Stock Out','Out',10.00,NULL,NULL,NULL,NULL),(3,34,NULL,'Stock Out','Out',5.00,NULL,NULL,NULL,NULL),(4,37,NULL,'Stock In','In',12.00,'admin','2020-09-17 10:46:26','liubao','2020-11-09 16:24:04'),(5,34,NULL,'Stock In','In',2.00,'admin','2020-09-17 15:37:16',NULL,NULL),(6,34,NULL,'Stock In','In',2.00,'admin','2020-09-17 15:37:18',NULL,NULL),(7,34,NULL,'Stock In','In',10.00,'liubao','2020-11-09 16:34:01',NULL,NULL),(8,34,1,'Stock Out','Out',1.00,'liubao','2020-11-09 17:03:41',NULL,NULL),(9,34,4,'Stock Out','Out',1.00,'liubao','2020-11-09 17:05:42',NULL,NULL),(10,37,3,'Stock Out','Out',1.00,'liubao','2020-11-09 17:05:53',NULL,NULL),(11,34,5,'Stock Out','Out',1.00,'liubao','2020-11-10 03:14:17',NULL,NULL),(12,36,NULL,'Stock In','In',2.00,'liubao','2020-11-16 08:53:50',NULL,NULL),(13,37,1,'Stock Out','Out',3.00,'admin','2020-11-30 06:54:55',NULL,NULL),(14,34,3,'Stock Out','Out',7.00,'admin','2020-11-30 07:16:38',NULL,NULL),(15,34,4,'Stock Out','Out',3.00,'liubao','2020-12-03 06:53:26',NULL,NULL);
/*!40000 ALTER TABLE `ao_stocklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_subscriptionlist`
--

DROP TABLE IF EXISTS `ao_subscriptionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_subscriptionlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `SubID` varchar(15) DEFAULT NULL,
  `SubName` varchar(245) DEFAULT NULL,
  `Price` varchar(10) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `Active` varchar(1) DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_subscriptionlist`
--

LOCK TABLES `ao_subscriptionlist` WRITE;
/*!40000 ALTER TABLE `ao_subscriptionlist` DISABLE KEYS */;
INSERT INTO `ao_subscriptionlist` VALUES (1,'frapp80-1m','1 Month Subscription @ RM80','80','su',NULL,'A'),(2,'frapp80-3m','3 Month Subscription - 10% discount @ RM216','216','su',NULL,'A'),(3,'frapp80-12m','12 Month Subscription - 20% discount @ RM216','768','su',NULL,'A'),(4,'frapp400-1m','1 Month Subscription @ RM400','400','mu',NULL,'A'),(5,'frapp400-3m','3 Month Subscription - 10% discount @ RM1080','1080','mu',NULL,'A'),(6,'frapp400-12m','12 Month Subscription - 20% discount @ RM3840','3840','mu',NULL,'A');
/*!40000 ALTER TABLE `ao_subscriptionlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_traininglist`
--

DROP TABLE IF EXISTS `ao_traininglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_traininglist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `TrainingType` varchar(145) DEFAULT NULL,
  `Step` int DEFAULT NULL,
  `TrainingCompleted` int DEFAULT NULL,
  `CreateQuotation` smallint DEFAULT NULL,
  `CreateCustomer` smallint DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_traininglist`
--

LOCK TABLES `ao_traininglist` WRITE;
/*!40000 ALTER TABLE `ao_traininglist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_traininglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_transactionlist`
--

DROP TABLE IF EXISTS `ao_transactionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_transactionlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `TransactionID` int DEFAULT NULL,
  `TransactionRef` varchar(145) DEFAULT NULL,
  `TransactionDate` date DEFAULT NULL,
  `TransactionAmount` decimal(13,2) DEFAULT NULL,
  `PaymentTo` varchar(245) DEFAULT NULL,
  `Active` int DEFAULT '1',
  `DebtorCode` varchar(45) DEFAULT NULL,
  `TransactionType` varchar(45) DEFAULT NULL,
  `TransactionDesc` varchar(245) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `PaymentTermID` int DEFAULT NULL,
  `ServiceTimes` int DEFAULT NULL,
  `SalesPersonSingle` varchar(45) DEFAULT NULL,
  `SalesPersonPassive` varchar(45) DEFAULT NULL,
  `SalesPersonSingleClient` varchar(45) DEFAULT NULL,
  `SalesPersonPassiveClient` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_transactionlist`
--

LOCK TABLES `ao_transactionlist` WRITE;
/*!40000 ALTER TABLE `ao_transactionlist` DISABLE KEYS */;
INSERT INTO `ao_transactionlist` VALUES (1,1,'34','2020-11-23',294.00,NULL,1,NULL,'BUY_PRODUCT','936 Wellness Retreat Mist','2020-11-23 03:10:56','01110340242',NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'34','2020-11-23',392.00,NULL,1,NULL,'BUY_PRODUCT','936 Wellness Retreat Mist','2020-11-23 03:14:08','01110340242',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ao_transactionlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_unitlist`
--

DROP TABLE IF EXISTS `ao_unitlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_unitlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UnitName` varchar(145) DEFAULT NULL,
  `UnitCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_unitlist`
--

LOCK TABLES `ao_unitlist` WRITE;
/*!40000 ALTER TABLE `ao_unitlist` DISABLE KEYS */;
INSERT INTO `ao_unitlist` VALUES (1,'Pieces','pieces'),(2,'KG','kg'),(3,'Pack','pack');
/*!40000 ALTER TABLE `ao_unitlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_uploadlist`
--

DROP TABLE IF EXISTS `ao_uploadlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_uploadlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Module` varchar(45) DEFAULT NULL,
  `ModuleID` int DEFAULT NULL,
  `FileName` varchar(245) DEFAULT NULL,
  `FileType` varchar(200) DEFAULT NULL,
  `FileSize` int DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `Active` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modulefilename` (`Module`,`FileName`) /*!80000 INVISIBLE */,
  KEY `moduleorderingactive` (`Module`,`Ordering`,`Active`,`ModuleID`),
  KEY `moduleactive` (`Module`,`Active`,`ModuleID`),
  KEY `moduleid` (`Module`,`ModuleID`,`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_uploadlist`
--

LOCK TABLES `ao_uploadlist` WRITE;
/*!40000 ALTER TABLE `ao_uploadlist` DISABLE KEYS */;
INSERT INTO `ao_uploadlist` VALUES (35,'UPLOAD_PRODUCTIMAGE',3,'Pan_Mee_1588422466110.jpg','image/jpeg',51445,1,'damien','2020-05-02 20:27:46',NULL,NULL,1),(36,'UPLOAD_PRODUCTIMAGE',4,'Hakka_Mee_1588422512346.jpg','image/jpeg',54148,1,'damien','2020-05-02 20:28:32',NULL,NULL,1),(37,'UPLOAD_PRODUCTIMAGE',6,'Kueh_Teow_Bulat_1588422989749.jpg','image/jpeg',48423,1,'damien','2020-05-02 20:36:29',NULL,NULL,1),(38,'UPLOAD_PRODUCTIMAGE',7,'Laksa_Pendek_1588423075051.jpg','image/jpeg',57248,1,'damien','2020-05-02 20:37:55',NULL,NULL,1),(39,'UPLOAD_PRODUCTIMAGE',12,'Laksa_Panjang_1588423337523.jpg','image/jpeg',58596,1,'damien','2020-05-02 20:42:17',NULL,NULL,1),(40,'UPLOAD_PRODUCTIMAGE',13,'Mee_Kuning_Kecil_1588423427739.jpg','image/jpeg',88655,0,'damien','2020-05-02 20:43:47',NULL,NULL,1),(41,'UPLOAD_PRODUCTIMAGE',14,'Mee_Besar_1588423491088.jpg','image/jpeg',62911,1,'damien','2020-05-02 20:44:51',NULL,NULL,1),(42,'UPLOAD_PRODUCTIMAGE',15,'Kueh_Teow_Halus_1588423733799.jpg','image/jpeg',68111,1,'damien','2020-05-02 20:48:53',NULL,NULL,1),(43,'UPLOAD_PRODUCTIMAGE',16,'Kueh_Teow_Kasar_1588423880453.jpg','image/jpeg',68563,0,'damien','2020-05-02 20:51:20',NULL,NULL,1),(44,'UPLOAD_PRODUCTIMAGE',17,'Yee_Mee_1588424001884.jpg','image/jpeg',45895,1,'damien','2020-05-02 20:53:21',NULL,NULL,1),(46,'UPLOAD_PRODUCTIMAGE',19,'Fucuk_Goreng_1588424192101.jpg','image/jpeg',52643,1,'damien','2020-05-02 20:56:32',NULL,NULL,1),(47,'UPLOAD_PRODUCTIMAGE',18,'MSFD500_1590213717942.png','image/png',478583,0,'admin','2020-05-23 06:01:58',NULL,NULL,1),(48,'UPLOAD_PRODUCTIMAGE',17,'YMS5_1590213749875.png','image/png',238401,0,'admin','2020-05-23 06:02:29',NULL,NULL,1),(49,'UPLOAD_PRODUCTIMAGE',19,'fucuk300_1590213793336.png','image/png',375390,0,'admin','2020-05-23 06:03:13',NULL,NULL,1),(50,'UPLOAD_PRODUCTIMAGE',5,'kulitwantan_1590213843388.png','image/png',251778,0,'admin','2020-05-23 06:04:03',NULL,NULL,1),(51,'UPLOAD_PRODUCTIMAGE',5,'kulit wantan_1590213855969.png','image/png',455623,1,'admin','2020-05-23 06:04:16',NULL,NULL,1),(54,'UPLOAD_PRODUCTIMAGE',15,'KTP900_1590214032016.png','image/png',326242,0,'admin','2020-05-23 06:07:12',NULL,NULL,1),(55,'UPLOAD_PRODUCTIMAGE',15,'KTM900_1590214041545.png','image/png',340063,1,'admin','2020-05-23 06:07:21',NULL,NULL,1),(56,'UPLOAD_PRODUCTIMAGE',14,'mb900_1590214071222.png','image/png',320241,0,'admin','2020-05-23 06:07:51',NULL,NULL,1),(57,'UPLOAD_PRODUCTIMAGE',13,'MK MEEDO900_1590214096051.png','image/png',346192,1,'admin','2020-05-23 06:08:16',NULL,NULL,1),(58,'UPLOAD_PRODUCTIMAGE',12,'LPJ_1590214114260.png','image/png',317011,0,'admin','2020-05-23 06:08:34',NULL,NULL,1),(59,'UPLOAD_PRODUCTIMAGE',7,'LSF_1590214140862.png','image/png',282130,0,'admin','2020-05-23 06:09:00',NULL,NULL,1),(60,'UPLOAD_PRODUCTIMAGE',6,'ccff_1590214169068.png','image/png',254562,0,'admin','2020-05-23 06:09:29',NULL,NULL,1),(61,'UPLOAD_PRODUCTIMAGE',6,'ccfbox_1590214179346.png','image/png',410487,1,'admin','2020-05-23 06:09:39',NULL,NULL,1),(62,'UPLOAD_PRODUCTIMAGE',4,'HKpacking_1590214218421.png','image/png',342367,0,'admin','2020-05-23 06:10:18',NULL,NULL,1),(63,'UPLOAD_PRODUCTIMAGE',3,'PMpacking_1590214252651.png','image/png',327033,0,'admin','2020-05-23 06:10:52',NULL,NULL,1),(64,'UPLOAD_PRODUCTIMAGE',3,'PK2packing_1590214258274.png','image/png',326722,1,'admin','2020-05-23 06:10:58',NULL,NULL,1),(65,'UPLOAD_PRODUCTIMAGE',2,'RamenFD500_1590214320207.png','image/png',384140,0,'admin','2020-05-23 06:12:00',NULL,NULL,1),(66,'UPLOAD_PRODUCTIMAGE',20,'FB CKT_1590217676200.jpg','image/jpeg',61930,1,'admin','2020-05-23 07:07:56',NULL,NULL,1),(67,'UPLOAD_PRODUCTIMAGE',20,'FBCKT_1590218580349.png','image/png',149345,0,'admin','2020-05-23 07:23:00',NULL,NULL,1),(68,'UPLOAD_PRODUCTIMAGE',21,'shabupacking_1590218999223.png','image/png',441585,1,'admin','2020-05-23 07:29:59',NULL,NULL,1),(70,'UPLOAD_PRODUCTIMAGE',21,'Shabu10_1590219030552.png','image/png',389830,0,'admin','2020-05-23 07:30:30',NULL,NULL,1),(72,'UPLOAD_PRODUCTIMAGE',22,'Pan_Mee_1590657445850.jpg','image/jpeg',51445,0,'admin','2020-05-28 09:17:25',NULL,NULL,1),(73,'UPLOAD_ORDERRECEIPT',104019,'thumbnail_image001_1590657656915.jpg','image/jpeg',55361,0,'01110390242','2020-05-28 09:20:56',NULL,NULL,1),(74,'UPLOAD_ORDERRECEIPT',104020,'thumbnail_image001_1590659033781.jpg','image/jpeg',55361,0,'01110390242','2020-05-28 09:43:53',NULL,NULL,1),(75,'UPLOAD_PRODUCTIMAGE',25,'WhatsApp Image 2020-05-12 at 4_1593057665751.jpeg','image/jpeg',33973,0,'01110340242','2020-06-25 12:01:05',NULL,NULL,1),(76,'UPLOAD_PRODUCTIMAGE',25,'ba7499f4-4260-4d98-8d23-a0cda2a35380_1593057749773.jpg','image/jpeg',50017,1,'01110340242','2020-06-25 12:02:30',NULL,NULL,1),(77,'UPLOAD_PRODUCTIMAGE',30,'Free_Sample_By_Wix_1593059100852.jpg','image/jpeg',2896,0,'01110340242','2020-06-25 12:25:00',NULL,NULL,1),(78,'UPLOAD_PRODUCTIMAGE',31,'b6985496-7a8d-4721-86e4-86b14d4a3ec0_1593059119926.jpg','image/jpeg',60130,0,'01110340242','2020-06-25 12:25:20',NULL,NULL,1),(84,'UPLOAD_PRODUCTIMAGE',25,'product_1593117823029.jpg','image/jpeg',12941,2,'01110340242','2020-06-26 04:43:43',NULL,NULL,1),(91,'UPLOAD_PRODUCTIMAGE',25,'1b5c00bc2389345e571a8a6fb27f21b6_1593119219581.jpg','image/jpeg',104808,3,'01110340242','2020-06-26 05:06:59',NULL,NULL,1),(103,'UPLOAD_PRODUCTIMAGE',2,'1b5c00bc2389345e571a8a6fb27f21b6_1593120629571.jpg','image/jpeg',104808,1,'01110340242','2020-06-26 05:30:29',NULL,NULL,1),(104,'UPLOAD_PRODUCTIMAGE',25,'1b5c00bc2389345e571a8a6fb27f21b6_1593120689806.jpg','image/jpeg',104808,4,'01110340242','2020-06-26 05:31:29',NULL,NULL,1),(106,'UPLOAD_PRODUCTIMAGE',21,'1b5c00bc2389345e571a8a6fb27f21b6_1593120777660.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 05:32:57',NULL,NULL,1),(107,'UPLOAD_PRODUCTIMAGE',12,'1b5c00bc2389345e571a8a6fb27f21b6_1593120897181.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 05:34:57',NULL,NULL,1),(108,'UPLOAD_PRODUCTIMAGE',12,'product_1593120905604.jpg','image/jpeg',65100,3,'01110340242','2020-06-26 05:35:05',NULL,NULL,1),(109,'UPLOAD_PRODUCTIMAGE',18,'1b5c00bc2389345e571a8a6fb27f21b6_1593122905517.jpg','image/jpeg',104808,1,'01110340242','2020-06-26 06:08:25',NULL,NULL,1),(110,'UPLOAD_PRODUCTIMAGE',6,'1b5c00bc2389345e571a8a6fb27f21b6_1593122935648.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:08:55',NULL,NULL,1),(111,'UPLOAD_PRODUCTIMAGE',17,'1b5c00bc2389345e571a8a6fb27f21b6_1593123023627.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:10:23',NULL,NULL,1),(112,'UPLOAD_PRODUCTIMAGE',2,'1b5c00bc2389345e571a8a6fb27f21b6_1593123154330.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:12:34',NULL,NULL,1),(113,'UPLOAD_PRODUCTIMAGE',4,'1b5c00bc2389345e571a8a6fb27f21b6_1593123172035.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:12:52',NULL,NULL,1),(116,'UPLOAD_PRODUCTIMAGE',15,'product_1593124372802.jpg','image/jpeg',65286,2,'01110340242','2020-06-26 06:32:52',NULL,NULL,1),(118,'UPLOAD_PRODUCTIMAGE',21,'product_1593161221649.jpg','image/jpeg',64004,3,'01110340242','2020-06-26 16:47:01',NULL,NULL,1),(125,'UPLOAD_PRODUCTIMAGE',33,'product_1593793122261.jpg','image/png',906614,0,'01110340242','2020-07-04 00:18:42',NULL,NULL,1),(127,'UPLOAD_TOUCHNGO',NULL,'product_1593889562795.jpg','image/png',738241,NULL,'01110340242','2020-07-05 03:06:02',NULL,NULL,NULL),(129,'UPLOAD_PRODUCTIMAGE',33,'product_1598545563033.jpg','image/png',304993,1,'admin','2020-08-28 00:26:03',NULL,NULL,1),(130,'UPLOAD_PRODUCTIMAGE',33,'80d7c9769aae5_1598545771847.jpg','image/jpeg',89695,2,'admin','2020-08-28 00:29:31',NULL,NULL,1),(131,'UPLOAD_PRODUCTIMAGE',33,'01_1598545792198.jpg','image/jpeg',2209863,3,'admin','2020-08-28 00:29:52',NULL,NULL,1),(133,'UPLOAD_PRODUCTIMAGE',34,'1_1598551788588.jpg','image/jpeg',687757,1,'admin','2020-08-28 02:09:48',NULL,NULL,1),(135,'UPLOAD_PRODUCTIMAGE',34,'2_1598551846286.jpg','image/jpeg',688682,1,'admin','2020-08-28 02:10:46',NULL,NULL,1),(136,'UPLOAD_PRODUCTIMAGE',34,'3_1598551851202.jpg','image/jpeg',734656,1,'admin','2020-08-28 02:10:51',NULL,NULL,1),(137,'UPLOAD_PRODUCTIMAGE',34,'4_1598551859521.jpg','image/jpeg',812156,1,'admin','2020-08-28 02:10:59',NULL,NULL,1),(138,'UPLOAD_PRODUCTIMAGE',34,'5_1598551869261.jpg','image/jpeg',746288,1,'admin','2020-08-28 02:11:09',NULL,NULL,1),(139,'UPLOAD_PRODUCTIMAGE',34,'6_1598551873427.jpg','image/jpeg',690382,1,'admin','2020-08-28 02:11:13',NULL,NULL,1),(140,'UPLOAD_PRODUCTIMAGE',34,'0_1598552186660.jpg','image/jpeg',163640,0,'admin','2020-08-28 02:16:26',NULL,NULL,1),(141,'UPLOAD_PRODUCTIMAGE',35,'a4d0a1aa0fed5_1598552445447.jpg','image/jpeg',98515,0,'admin','2020-08-28 02:20:45',NULL,NULL,1),(142,'UPLOAD_PRODUCTIMAGE',35,'01_1598552451065.jpg','image/jpeg',2120002,1,'admin','2020-08-28 02:20:51',NULL,NULL,1),(143,'UPLOAD_PRODUCTIMAGE',35,'02_1598552460526.jpg','image/jpeg',1277044,2,'admin','2020-08-28 02:21:00',NULL,NULL,1),(144,'UPLOAD_PRODUCTIMAGE',35,'03_1598552467338.jpg','image/jpeg',1277217,3,'admin','2020-08-28 02:21:07',NULL,NULL,1),(145,'UPLOAD_PRODUCTIMAGE',35,'04_1598552472779.jpg','image/jpeg',1123567,4,'admin','2020-08-28 02:21:12',NULL,NULL,1),(146,'UPLOAD_PRODUCTIMAGE',35,'05_1598552476942.jpg','image/jpeg',1299255,5,'admin','2020-08-28 02:21:16',NULL,NULL,1),(147,'UPLOAD_PRODUCTIMAGE',35,'06_1598552481064.jpg','image/jpeg',1099641,6,'admin','2020-08-28 02:21:21',NULL,NULL,1),(148,'UPLOAD_PRODUCTIMAGE',35,'07_1598552484395.jpg','image/jpeg',1186792,7,'admin','2020-08-28 02:21:24',NULL,NULL,1),(149,'UPLOAD_PRODUCTIMAGE',36,'LAE2_1598553179051.png','image/png',506436,0,'admin','2020-08-28 02:32:59',NULL,NULL,1),(150,'UPLOAD_PRODUCTIMAGE',36,'01_1598553185151.jpg','image/jpeg',2209863,1,'admin','2020-08-28 02:33:05',NULL,NULL,1),(151,'UPLOAD_PRODUCTIMAGE',36,'02_1598553188922.jpg','image/jpeg',12494280,2,'admin','2020-08-28 02:33:08',NULL,NULL,1),(152,'UPLOAD_PRODUCTIMAGE',36,'03_1598553192414.jpg','image/jpeg',13150355,3,'admin','2020-08-28 02:33:12',NULL,NULL,1),(153,'UPLOAD_PRODUCTIMAGE',36,'04_1598553197458.jpg','image/jpeg',12645023,4,'admin','2020-08-28 02:33:17',NULL,NULL,1),(154,'UPLOAD_PRODUCTIMAGE',36,'05_1598553201086.jpg','image/jpeg',13217028,5,'admin','2020-08-28 02:33:21',NULL,NULL,1),(155,'UPLOAD_PRODUCTIMAGE',36,'06_1598553204521.jpg','image/jpeg',12848088,6,'admin','2020-08-28 02:33:24',NULL,NULL,1),(156,'UPLOAD_PRODUCTIMAGE',36,'07_1598553208341.jpg','image/jpeg',12595955,7,'admin','2020-08-28 02:33:28',NULL,NULL,1),(157,'UPLOAD_PRODUCTIMAGE',36,'08_1598553212155.jpg','image/jpeg',13094966,8,'admin','2020-08-28 02:33:32',NULL,NULL,1),(158,'UPLOAD_PRODUCTIMAGE',37,'0ddb998159ae5_1598553433196.jpg','image/jpeg',86146,0,'admin','2020-08-28 02:37:13',NULL,NULL,1),(159,'UPLOAD_PRODUCTIMAGE',37,'1_1598553437764.jpg','image/jpeg',398957,1,'admin','2020-08-28 02:37:17',NULL,NULL,1),(160,'UPLOAD_PRODUCTIMAGE',37,'2_1598553441978.jpg','image/jpeg',422469,2,'admin','2020-08-28 02:37:21',NULL,NULL,1),(161,'UPLOAD_PRODUCTIMAGE',37,'3_1598553445787.jpg','image/jpeg',423305,3,'admin','2020-08-28 02:37:25',NULL,NULL,1),(162,'UPLOAD_PRODUCTIMAGE',37,'4_1598553450355.jpg','image/jpeg',406487,4,'admin','2020-08-28 02:37:30',NULL,NULL,1),(163,'UPLOAD_PRODUCTIMAGE',37,'5_1598553455212.jpg','image/jpeg',528101,5,'admin','2020-08-28 02:37:35',NULL,NULL,1),(165,'UPLOAD_PRODUCTIMAGE',37,'6_1598553465858.jpg','image/jpeg',441034,6,'admin','2020-08-28 02:37:45',NULL,NULL,1),(167,'UPLOAD_PRODUCTIMAGE',38,'01_1598815116313.jpg','image/jpeg',18691273,0,'01110340242','2020-08-31 03:18:36',NULL,NULL,1),(168,'UPLOAD_PRODUCTIMAGE',38,'02_1598815120433.jpg','image/jpeg',18225654,1,'01110340242','2020-08-31 03:18:40',NULL,NULL,1),(169,'UPLOAD_PRODUCTIMAGE',38,'03_1598815124245.jpg','image/jpeg',18553373,1,'01110340242','2020-08-31 03:18:44',NULL,NULL,1),(170,'UPLOAD_PRODUCTIMAGE',38,'04_1598815129487.jpg','image/jpeg',18592317,1,'01110340242','2020-08-31 03:18:49',NULL,NULL,1),(171,'UPLOAD_PRODUCTIMAGE',38,'05_1598815133758.jpg','image/jpeg',18289768,1,'01110340242','2020-08-31 03:18:53',NULL,NULL,1),(172,'UPLOAD_PRODUCTIMAGE',38,'06_1598815142096.jpg','image/jpeg',18559272,1,'01110340242','2020-08-31 03:19:02',NULL,NULL,1),(173,'UPLOAD_PRODUCTIMAGE',38,'07_1598815146245.jpg','image/jpeg',18538367,1,'01110340242','2020-08-31 03:19:06',NULL,NULL,1);
/*!40000 ALTER TABLE `ao_uploadlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_vendorinvoiceheader`
--

DROP TABLE IF EXISTS `ao_vendorinvoiceheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_vendorinvoiceheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CustomerID` int DEFAULT NULL,
  `MAWBID` varchar(100) DEFAULT NULL,
  `InvoiceNo` varchar(100) DEFAULT NULL,
  `InvoiceDate` date DEFAULT NULL,
  `InvoiceDueDate` date DEFAULT NULL,
  `InvoiceAmount` decimal(18,2) DEFAULT NULL,
  `InvoiceAmountSST` decimal(18,2) DEFAULT NULL,
  `InvoiceTotalAmount` decimal(18,2) DEFAULT NULL,
  `TPInvoiceNo` varchar(45) DEFAULT NULL,
  `InvoiceAmountSSTApplicable` decimal(18,2) DEFAULT NULL,
  `PaymentTerm` varchar(20) DEFAULT NULL,
  `CustomerCode` varchar(45) DEFAULT NULL,
  `Approved` smallint DEFAULT NULL,
  `Deleted` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`),
  KEY `MAWBID` (`MAWBID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_vendorinvoiceheader`
--

LOCK TABLES `ao_vendorinvoiceheader` WRITE;
/*!40000 ALTER TABLE `ao_vendorinvoiceheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_vendorinvoiceheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_zonelist`
--

DROP TABLE IF EXISTS `ao_zonelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_zonelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ZoneID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DriverID` smallint DEFAULT NULL,
  `ZoneTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZoneDesc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ZoneArea` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_zonelist`
--

LOCK TABLES `ao_zonelist` WRITE;
/*!40000 ALTER TABLE `ao_zonelist` DISABLE KEYS */;
INSERT INTO `ao_zonelist` VALUES (1,'Zone01',1,'Zone 1','BPW 4452 - 1 TON\r\nArmizan\r\n016-201 6367','Klang, Meru, Kapar, Port Klang, Parkland,\r\nWest Port, Bukit Tinggi, Sijangkang, Teluk Panglima Garang',NULL,NULL,'2019-06-21 13:00:33','Damien'),(2,'Zone02',2,'Zone 2','BPW 584 - 1 TON\r\nIzzat\r\n016-201 6394','Shah Alam, Kota Kemuning, Putra Height, Bukit Naga, Taman Sri Muda',NULL,NULL,'2019-06-21 13:01:02','Damien'),(3,'Zone03',3,'Zone 3','BPW 5672 - 1 TON \r\nHasni\r\n016-201 6327 ','Kelana Jaya, Sunway Damansara, Mont Kiara, Bandar Utama, Tmn Tun Dr.Ismail, Damansara Perdana, Kota Damansara',NULL,NULL,'2019-06-21 13:18:09','Damien'),(4,'Zone04',4,'Zone 4','VDC 4910 - 1 TON\r\nShaiful\r\n016-201 6349','Puchong SATU \r\n\r\nBandar Puchong',NULL,NULL,'2019-08-08 14:56:14','Damien'),(5,'Zone05',5,'Zone 5','BPW 4458 - 1 TON\r\nSakri\r\n016-201 6354','Subang Jaya,  Ara Damansara, USJ, Denai Alam, Bandar Sunway, ACER',NULL,NULL,'2019-08-05 14:44:50','PeiLing'),(6,'Zone06',6,'Zone 6','BPW 614 - 1 TON\r\nFaizal\r\n016-201 6302','Rawang, Sungai Buloh, Kepong, Sri Damansara, Selayang, Manjalara, Damansara Damai',NULL,NULL,'2019-06-21 13:03:45','Damien'),(7,'Zone07',7,'Zone 7','BPW 589 - 1 TON\r\nAdnan \r\n017 2401051','Kuchai Lama, Salak South, Bdr Tun Razak, Sg. Besi, Taman Connaught, BT9 Cheras, Old Klang Road',NULL,NULL,'2019-10-16 17:47:13','Damien'),(8,'Zone08',8,'Zone 8','BPW 5670 - 1 TON\r\nAmirul  019 9192885','Cheras, Ampang, Jln Pudu, Loke Yew, Jln San Peng, Chan Saw Lin, Pandan Perdana, Pandan Indah','2013-04-21 02:36:10','asiaone-admin','2019-10-16 17:46:57','Damien'),(9,'Zone09',9,'Zone 9','BPW 4449 - 1 TON\r\nFairus\r\n014 6600943','Seri Kembangan, Balakong, Bangi, Bukit Jalil, Kajang, Semenyih, Mahkota Cheras, Taman Teknologi Klang','2013-04-21 02:41:00','asiaone-admin','2019-10-16 17:42:14','Damien'),(14,'Zone10',10,'Zone 10','BPW 5880 - 1 TON\r\nHisyam\r\n017-677 0401','Sentul, Jalan Ipoh, Jalan Kuching, Segambut, Batu Caves, Jinjang','2014-04-01 12:07:36','CHELSEA','2019-08-08 15:08:10','Damien'),(15,'Zone11',11,'Zone  11','BPW 5911 - 1 TON\r\nErzzad \r\n018 6681702','KL Town, Imbi, Bukti Bintang, Petaling Street, Pudu, KLCC, Ampang, Wangsa Maju, Setapak, Chow Kit','2014-04-03 11:41:55','CHELSEA','2019-10-16 17:46:24','Damien'),(16,'Zone12',12,'Zone 12','VDD 3467 - 1 TON\r\n Rizal \r\n011-6992 0772','PJ Town, Bangsar, Brickfield, Bangsar South ','2014-04-09 16:27:21','CHELSEA','2019-10-16 17:42:42','Damien'),(19,'Zone14',13,'Zone 14','BPW 5890 - 1 TON  Firdaus 013 2927159','KLIA Airport ','2017-05-02 12:57:05','PeiLing','2019-10-16 17:43:37','Damien'),(18,'Zone13',14,'Zone 13','BPW 5882 1 -TON  Hairul \r\n011 16336956\r\n','Glenmarie U1-U8, CANON','2016-11-18 10:45:02','PeiLing','2019-10-16 17:45:35','Damien'),(20,'Zone 21',15,'Zone 21','W6115F - 1 Ton\r\nBustami 013 8853058','Acer Local, Techworld','2018-06-21 16:38:31','PeiLing','2019-10-16 17:47:59','Damien'),(21,'Zone 15',16,'Zone 15','VDD 3464 - 1 TON  Khairul \r\n013 6665533','Puchong DUA \r\n\r\n','2018-08-03 16:05:16','PeiLing','2019-10-16 17:44:54','Damien'),(22,'Zone 16',NULL,'Zone 16','BPW  5667 - 1 TON Jihad\r\n011 11466753','Klang DUA ','2019-04-17 09:29:00','PeiLing','2019-10-16 17:44:29','Damien'),(23,'Zone 17',NULL,'Zone 17','BPW 5901  - 1 TON  Yusliza \r\n018 2835844\r\n','KLIA  Airport ','2019-06-21 12:26:43','Damien','2019-10-16 17:44:08','Damien'),(24,'Zone 19 ',NULL,'Zone 19','WWP 8935 - 3 TON \r\n\r\nKLIA Airport','','2019-06-21 12:26:55','Damien','2019-06-21 12:34:49','Damien'),(25,'Zone 20 ',NULL,'Zone 20 ','WVH 783 - 3 TON\r\n\r\nKLIA Airport  ','','2019-06-21 12:27:07','Damien','2019-06-21 12:35:14','Damien'),(27,'Zone 22 ',NULL,'Zone 22','Other ','','2019-06-21 12:27:40','Damien','2019-06-21 12:29:41','Damien');
/*!40000 ALTER TABLE `ao_zonelist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-04 10:21:06
